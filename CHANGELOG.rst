Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_.


`Unreleased`_
-------------

Added
+++++
- expected look only version.


`20.05`_
--------

Added
+++++
- Proof of concept using `Leaflet`_.

-------

Laboratório de Automação de Sistemas Educacionais
-------------------------------------------------

**Copyright © Carlo Olivera**


.. ..

 [LABASE](http://labase.sefip.org) -
 [Instituto Tércio Pacitti](http://nce.ufrj.br) -
 [UFRJ](http://www.ufrj.br)

 ![LABASE Laboratório de Automação de sistemas Educacionais][logo]

 [logo]: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png "Laboratório de Automação de sistemas Educacionais"
 <!---

.. ..

LABASE_ - `Instituto Tércio Pacitti`_ - UFRJ_

.. image:: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png
   :alt: Laboratório de Automação de sistemas Educacionais


.. _LABASE: http://labase.nce.ufrj.br/
.. _Instituto Tércio Pacitti: http://www.nce.ufrj.br/
.. _ufrj: http://www.UFRJ.BR/
.. _Unreleased: https://gitlab.com/cetoli/spekuloom/-/commit/aee4f8d0ddb0bd02aa439ba5ac18f285cc7283e7
.. _20.05: https://gitlab.com/cetoli/spekuloom/-/commit/aee4f8d0ddb0bd02aa439ba5ac18f285cc7283e7
.. _20.04.1: https://gitlab.com/cetoli/spekuloom/-/commit/aee4f8d0ddb0bd02aa439ba5ac18f285cc7283e7
.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _Leaflet: https://leafletjs.com/

.. ..

 --->

.. ..