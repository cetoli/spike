#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program SuperPython
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Generate dynamic site from client side.

    This module controls the creation of the IDE user interface.

Classes in this module:

    :py:class:`Generate` Generate dynamic site from client side.

    :py:class:`CONST` Default file names for methods and other default constants.

Changelog
---------
    *Upcoming*
        * TODO: Improve documentation and test.
        * CHANGE: New Architecture.
    20.03
        * BETTER: Document classes and methods.
        * CHANGE: Move consts to a class.
        * NEW: Use Brython templates.
        * FIX: Working burger menus.
    19.10
        * NEW: Initial Implementation.

.. seealso::

   Page :ref:`core_introduction`

"""
import urllib.request


class Action(object):

    def __init__(self, responder):
        self.responder = responder

    def action(self):
        Main.UI.template("id_titulo").render(titulo="LABASE - Sistemas Educacionais")


class Responder(object):
    pass


class Main:
    UI = None

    def __init__(self, browser, url=None):
        def create_tag(src, tag='script', _type="text/javascript"):
            """ Create a tag added to head"""
            _fp = urllib.request.urlopen(src)
            _tag = self.dc.createElement(tag)
            _tag.type, _tag.text = _type, _fp.read()
            self.dc.get(tag='head')[0].appendChild(_tag)

        def create_style_tag(href, _type="text/css", tag='style'):
            create_tag(f"css/{href}", _type=_type, tag=tag)

        self.url = url
        self.by = by = Main.UI = browser
        # self.panel = by.document["_nav_panel_"]
        self.dc, self.ht, self.tp = by.document, by.html, by.template.Template
        self.action = Action(Responder())

    def main(self):
        """ Activate Brython templates and set events to burger menus.

        """

        def power_nav_burger():
            document = self.dc

            def toggle_burger(_element):
                _element = _element.target
                _target = document[_element.getAttribute('data-target')]
                # Toggle the class on both the "navbar-burger" and the "navbar-menu"
                _element.classList.toggle('is-active')
                _target.classList.toggle('is-active')

            nav_burger = document.select('.navbar-burger')
            for element in nav_burger:
                element.setAttribute("data-target", "navbar-menu")
                element.addEventListener('click', toggle_burger)

        # power_nav_burger()
        # self.dc["navbar_home"].bind("click", self.action.action)
        print(Main.UI, Main.UI.template)
        Main.UI.template.Template("id_titulo").render(titulo="LABASE - Sistemas Educacionais")
        # self.action.action()
