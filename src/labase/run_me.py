#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program Labase
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Serves static files.

    This module serve static files according to extension.


Changelog
---------
    *Upcoming*
        * FIX: . regulan expression
    20.03
        * NEW: Use Brython templates.

"""
import bottle
import os
import sys

from bottle import TEMPLATE_PATH, static_file, route, default_app, get
project_home = os.path.dirname(os.path.abspath(__file__))

# add your project directory to the sys.path
# project_home = u'/home/supygirls/dev/SuPyGirls/src'
if project_home not in sys.path:
    sys.path = [project_home] + sys.path

application = default_app()

# Create a new list with absolute paths
# TEMPLATE_PATH.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../view/tpl')))
# make sure the default templates directory is known to Bottle
tpl_dir = os.path.join(project_home, 'view/file/labase')
img_dir = os.path.join(project_home, 'view/file/labase')
css_dir = os.path.join(project_home, 'view/file/labase')
js_dir = os.path.join(project_home, 'view/file/labase')
py_dir = os.path.join(project_home, 'view/file/labase')
# py_dir = os.path.join(project_home, 'view/freq')

if tpl_dir not in TEMPLATE_PATH:
    TEMPLATE_PATH.insert(0, tpl_dir)


@route('/')
def index():
    bottle.redirect("/file/labase/homesite.html")
    # bottle.redirect("/file/labase/labase.html")


# Static Routes
@get("/file/labase/<filepath:re:.*[.](png|jpg|svg|gif|ico|html)>")
def img(filepath):
    print(filepath, img_dir)
    return static_file(filepath, root=img_dir)


# Static Routes
@get("/file/labase/<filepath:re:.*[.]ttf>")
def font(filepath):
    return static_file(filepath, root=css_dir)


# Static Routes
@get("/file/labase/<filepath:re:.*[.]css>")
def css(filepath):
    print("def css",  filepath, img_dir)
    return static_file(filepath, root=css_dir)


# Static Routes
@get("/file/labase/<filepath:re:.*[.]js>")
def ajs(filepath):
    return static_file(filepath, root=js_dir)


# Static Routes
@get("/file/labase/<filepath:re:.*[.]py>")
def apy(filepath):
    return static_file(filepath, root=py_dir)


if __name__ == "__main__":
    bottle.run(host='localhost', port=8080)
