"""
############################################################
Jogo das Carinhas - Principal
############################################################

Classe que define uma carinha e inicia o jogo com as carinhas embaralhadas.

"""
from random import random
from vitollino import JOGO

ACTIV = "https://activufrj.nce.ufrj.br/studio/GamesInteligentesII/{}.png"
GRUPOS = "camg"
CENA = "https://i.imgur.com/sGoKfvs.jpg"
DD = 90
MW, MH = 400, 300
CARA = {}
WIKI = "https://activufrj.nce.ufrj.br/rest/wiki/activlets/Caras_Tabuleiro"


class Cara(JOGO.a):

    def __init__(self, img, x, y, cena):
        CARA[f"_cara_{x}_{y}"] = self
        super().__init__(img, x=x, y=y, w=DD, h=DD, drag=True, cena=cena)
        """chama o construtor do Elemento Vitollino passandoa as informações necessárias"""
        self.elt.id = f"_cara_{x}_{y}"
        JOGO.i.bota(self)
        self.i = len

    def posiciona(self, x, y):
        self.x, self.y = x - DD / 2, y - DD / 2


class Mesa(JOGO.a):

    def __init__(self, img, x, y=0, cena=None):
        super().__init__(img, x=x, y=y, w=MW, h=MH, cena=cena)
        """chama o construtor do Elemento Vitollino passandoa as informações necessárias"""
        self.elt.ondrop = lambda ev: self.drop(ev)
        self.cena = cena

    def drop(self, ev):
        ev.preventDefault()
        ev.stopPropagation()
        src_id = ev.data['text']
        x, y = ev.clientX, ev.clientY
        CARA[src_id].entra(self.cena)
        CARA[src_id].posiciona(x, y)


class Carinhas:
    """Base do jogo com tabuleiro e carinhas."""

    def __init__(self, doc, j):
        """Constroi os objetos iniciais. """
        self.s = self.gui = self.tabuleiro = self.cena = None
        self.game = doc.query.getvalue("g", None)
        self.j = j
        doc["_version_"].html = f"21.05 - {self.game}"
        self.d = 'https://activufrj.nce.ufrj.br/studio/Jogo_Carinhas/tabuleiro.png?disp=inline&size=G'
        self.cena = self.j.c(CENA).vai()

    def cria(self):
        Mesa("https://i.imgur.com/84sRLl2.jpg", x=700, y=0, cena=self.cena)
        Mesa("https://i.imgur.com/WPashRd.jpg", x=700 + 450, cena=self.cena)
        Mesa("https://i.imgur.com/KwKyMLN.jpg", x=700, y=350, cena=self.cena)
        Mesa("https://i.imgur.com/ECVki8P.jpg", x=700 + 450, y=350, cena=self.cena)

    def cria_(self):
        cena = self.cena
        w = 100
        dw = 80
        dy = 0
        color = ["a", "v"]
        # [self.j.a(ACTIV.format(f"{grupo}_{color[oid // 8]}{oid % 8 + 1}"),
        #           w=w, h=w, x=dw * (oid % 16), y=dw * (gcount), cena=cena)
        #  for oid in range(16) for gcount, grupo in enumerate(GRUPOS)]
        [Cara(ACTIV.format(f"{grupo}_{color[colour]}{oid % 8 + 1}"),
              x=dw * (oid % 16), y=dw * (gcount * 2 + colour), cena=cena)
         for oid in range(8) for colour in (0, 1) for gcount, grupo in enumerate(GRUPOS)]

    def read_wiki(self, url):
        """Lê uma página da wiki com uma chamada REST
        :param url: Url REST da wiki a ser lida
        :return: COnteúdo da página wiki
        """
        _fakec = '<h1></h1>' + '<li>Lê uma página da wiki com uma chamada REST</li>' * 10
        _fake = dict(result=dict(wikidata=dict(conteudo=_fakec)))
        try:
            import urllib.request
            import json
            _fp = urllib.request.urlopen(url)
            print(_fp)
            if isinstance(_fp, tuple):
                _fp = _fp[0]
                _data = _fp.read()
            else:
                _data = _fp.read().decode('utf8')
            print(_data)
            _json = json.loads(str(_data))
        except Exception as ex:
            _json = _fake

        if "result" in _json and "wikidata" in _json["result"]\
                and "conteudo" in _json["result"]["wikidata"]:
            return _json["result"]["wikidata"]["conteudo"]
        else:
            return ""


def main(doc, svg, j, s=None):
    s.update(width=1600, height="650px")
    # print('Carinhas 0.1.0')
    carinhas = Carinhas(doc, j)
    # carinhas.cria()
    carinhas.cria()
    carinhas.cria_()
    # carinhas.build_base(doc, svg)


if __name__ == "__main__":
    from browser import document, svg
    from _spy.vitollino.main import JOGO, STYLE

    STYLE.update(width=1600, height='600px')
    main(document, svg, JOGO)
