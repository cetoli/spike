"""
############################################################
Jogo da Roda da Lingugem - Principal
############################################################

Classe que define Rodas om imagens para formar uma frase.

"""
from random import random
from vitollino import JOGO

ACTIV = "https://activufrj.nce.ufrj.br/studio/GamesInteligentesII/{}.png"
GRUPOS = "camg"
CENA = "https://i.imgur.com/sGoKfvs.jpg"
RODAS = "/file/carlo/rodas.png"
SETAS = "/file/carlo/setas.png"
DD = 30
MW, MH = 1800, 600
CARA = {}


class Roda(JOGO.a):

    def __init__(self, img=RODAS, x=DD, y=DD, i=0, s=MH, cena=CENA):
        super().__init__(img, x=x, y=y, w=s, h=s, drag=True, cena=cena)
        """chama o construtor do Elemento Vitollino passandoa as informações necessárias"""
        self.img, self.x, self.y, self.i, self.cena = img, x, y, i, cena
        self.elt.id = f"_cara_{x}_{y}"
        self.siz = (3*s, s)
        self.rodou = 0
        self.pos = (-s*i, 0)
        self.yoff, self.xoff = 3*DD, 0
        self.botao = None

    def cria_botao(self):
        img, x, y, i, cena = self.img, self.x, self.y, self.i, self.cena
        self.botao = Roda(img=img, x=2*DD+self.xoff, y=i*6*DD+self.yoff, i=i, s=5*DD, cena=cena)
        self.botao.vai = self.rotaciona

    def rotaciona(self, _):
        self.rodou = (self.rodou + 30) % 360
        self.elt.style.transform = "rotate({}deg)".format(self.rodou)


class Seta(Roda):

    def __init__(self, img=SETAS, x=DD, y=DD, i=0, s=MH, cena=CENA):
        super().__init__(img, x=x+30, y=y+30, i=i, s=int(s-60), cena=cena)
        self.rodou = 15
        self.elt.style.transform = "rotate({}deg)".format(self.rodou)
        # self.yoff = 6*DD
        self.xoff = 6*DD


class Mesa(JOGO.a):

    def __init__(self, img, x, y=0, cena=None):
        super().__init__(img, x=x, y=y, w=MW, h=MH, cena=cena)
        """chama o construtor do Elemento Vitollino passandoa as informações necessárias"""
        self.elt.ondrop = lambda ev: self.drop(ev)
        self.cena = cena

    def drop(self, ev):
        ev.preventDefault()
        ev.stopPropagation()
        src_id = ev.data['text']
        x, y = ev.clientX, ev.clientY
        CARA[src_id].entra(self.cena)
        CARA[src_id].posiciona(x, y)


class Rodas:
    """Base do jogo com tabuleiro e carinhas."""

    def __init__(self, doc, j):
        """Constroi os objetos iniciais. """
        self.s = self.gui = self.tabuleiro = self.cena = None
        self.game = doc.query.getvalue("g", None)
        self.j = j
        doc["_version_"].html = f"21.05 - {self.game}"
        self.cena = self.j.c(CENA).vai()

    def cria(self):
        Roda(i=0, cena=self.cena, x=500).cria_botao()
        Seta(i=0, cena=self.cena, x=500).cria_botao()
        Roda(i=1, cena=self.cena, x=500).cria_botao()
        Seta(i=1, cena=self.cena, x=500).cria_botao()
        Roda(i=2, cena=self.cena, x=500).cria_botao()
        Seta(i=2, cena=self.cena, x=500).cria_botao()


def main(doc, svg, j, s=None):
    s.update(width=1600, height="650px")
    # print('Carinhas 0.1.0')
    roda = Rodas(doc, j)
    # carinhas.cria()
    roda.cria()
    # carinhas.build_base(doc, svg)


if __name__ == "__main__":
    from browser import document, svg
    from _spy.vitollino.main import JOGO, STYLE

    STYLE.update(width=1600, height='600px')
    main(document, svg, JOGO)
