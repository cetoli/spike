
"""
Use este comando de linha para invocar o libreoffice:
libreoffice --draw --accept="socket,host=localhost,port=2002;urp;StarOffice.ServiceManager" <arquivo>
"""
import socket
import uno
from random import choice


localContext = uno.getComponentContext()
resolver = localContext.ServiceManager.createInstanceWithContext(
    "com.sun.star.bridge.UnoUrlResolver", localContext)
ctx = resolver.resolve("uno:socket,host=localhost,port=2002;urp;StarOffice.ComponentContext")
smgr = ctx.ServiceManager
desktop = smgr.createInstanceWithContext("com.sun.star.frame.Desktop", ctx)
model = desktop.getCurrentComponent()
dp0 = model.getDrawPages().getByIndex(0)


class Dia:
    def __init__(self, line, dia):
        horas = (0, 4, 5, 10)
        diasem = (9, 8, 9, 9)[dia]
        off = choice([0, 5, 10, 15])
        [self.texter((5500 + col * 2500, 7800 - 1024 + line * 512), "{}:{:=02}".format(diasem + hora, off))
         for col, hora in enumerate(horas)]

    @staticmethod
    def texter(pos, string, size=(2500, 1000)):
        text = model.createInstance("com.sun.star.drawing.TextShape")
        tpos = text.Position
        tpos.X, tpos.Y = pos
        text.Position = tpos
        tsize = text.Size
        tsize.Width, tsize.Height = size
        text.Size = tsize

        dp0.add(text)
        text.String = string
        text.Style.CharHeight = 12.0
        return text


calnov = (2, 3, 4, 5, 9, 10, 11, 12, 16, 17, 18, 19)
calout = (1, 2, 3, 4, 7, 8, 9, 10, 14, 15, 16, 17, 21, 22, 23, 24, 28, 29, 30, 31)
week = (0, 1, 2, 3) * 5
[Dia(dmes, dsem) for dmes, dsem in zip(calout, week)]
