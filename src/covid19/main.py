#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program PyCovid19
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Generate dynamic site from client side.

    This module controls the creation of the IDE user interface.

Classes in this module:

    :py:class:`Response` Generate dynamic site from use cases.

    :py:class:`CONST` Default file names for methods and other default constants.

Changelog
---------
    *Upcoming*
        * TODO: Improve documentation and test.
    20.05
        * NEW: Leaflet spike.

.. seealso::

   Page :ref:`epydemic_introduction`

"""


class Covid:
    def __init__(self, browser):
        self.item = None
        self.zero = [-22.902, -43.3075]
        self.by, self.dc, self.ht = browser, browser.document, browser.html
        self.svg, self.w, self.lf, self.map = browser.svg, browser.window, browser.leaf, browser.map
        self.map = self.lf.map('_map_').setView([-22.902, -43.3075], 12)

        self.root = self.dc["_main_"]
        self.person = self.dc["_person_"]
        self.people = []
        self.lf.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            "attribution": '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(self.map)
        # marker = self.lf.marker([-22.902, -43.2075]).addTo(self.map)

        # self.map = self.w.L.map('_map_').setView([51.505, -0.09], 13)
        self.dc['_map_'].style.display = "block"
        self.dc['_splash_'].style.display = "none"
        self.render()

    def view(self, x, y):
        self.map.setView([x, y], 14)

    def render(self):
        from random import random, choice
        colors = "red red red yellow yellow green".split()
        people = [(-random()/5+0.09, -random()/3+0.05) for _ in range(120)]

        self.people = [Person(self, choice(colors), self.zero[0]+x, self.zero[1]+y)
                       for x, y in people]

    def add(self, elm, cont=None):
        cont = cont or self.root
        cont <= elm

    def image(self, color):
        div = self.ht.DIV()  # style="float:left;")  # (style="display: inline; ")
        div.style.float = "left"
        img = self.ht.IMG(src=CONST.FACE.format(color), width=16)
        self.add(img, div)
        self.add(div, self.person)
        return div
        
    def pin(self, color, shadow="gray"):
        icon = self.lf.icon(dict(
            iconUrl=CONST.PIN.format(color),
            iconSize=[12, 20],
            iconAnchor=[6, 20],
            popupAnchor=[-3, -76],
            shadowUrl=CONST.PIN.format(shadow),
            shadowSize=[16, 20],
            shadowAnchor=[8, 20]
        ))
        return icon

    def marker(self, icon, x, y):
        return self.lf.marker([x, y], {"icon": icon}).addTo(self.map)


class Person:
    def __init__(self, app, color, x, y):
        def engage(*_):
            app.view(x, y)
        self.item = None
        self.item = app.image(color)
        self.item.bind("click", engage)
        self.pin = app.pin(color)
        self.x, self.y = x, y
        app.marker(self.pin, x, y)




class CONST:
    FACE = "covid19/images/person_{}.png"
    PIN = "covid19/images/pin_{}.png"


def main(brython):
    def _render(*_):
        action_object = None
        return action_object

    covid = Covid(brython)

    brython.timer.set_timeout(_render, 100)
    return covid, _render


if __name__ == "__main__":
    def offline_main():
        from unittest.mock import MagicMock

        class Browser:
            mock = MagicMock()
            document, alert, html, window, ajax, timer, svg = [mock] * 7
            leaf, map = [mock] * 2
            Covid.add = MagicMock()

        covid, render = main(Browser)
        return covid, render
    offline_main()
