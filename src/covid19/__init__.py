#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program PyCovid19
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""General setup for program.

    This module manage the version for the app.

Changelog
---------
    *Upcoming*
        * TODO: Improve documentation and test.
    20.05
        * NEW: Leaflet spike.

.. seealso::

   Page :ref:`epydemic_introduction`

"""
__version__ = "20.05"
