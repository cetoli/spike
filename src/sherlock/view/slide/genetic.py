from random import choice
GTYPE = "12346789"
EMPTY = range(0)


class Genetica:
    NUCLEOTIDEOS = "1234"
    BASE = "0"
    CODON = ("",)*4
    GEN = (CODON,)*4
    CROMO = (GEN,)*4
    DNA = (CROMO,)*3
    GENOMA = ("",)*20

    @property
    def code(self):
        return "".join(str(ek.code) for ek in self.eucariontes)

    def visit(self, visitor):
        visitor.visit_population(self.eucariontes)
        # [ek.visit(visitor) for ek in self.eucariontes]

    def __init__(self, genoma=GENOMA):

        class Base:
            def __init__(self, code="", default=None, parts=1):
                self._parts = parts
                self._default = default or choice(Genetica.NUCLEOTIDEOS)
                self._code = code or choice(Genetica.NUCLEOTIDEOS)

            @property
            def code(self):
                # return self.encoding
                return self._code

            def visit(self, visitor):
                visitor.visit_base(self._code)

            def create(self, factory, parts, **kwargs):
                _ = self
                return [factory(**kwargs) for _ in range(parts)]

            def __repr__(self):
                return str(self._code)

        class Codon(Base):
            def __init__(self, codon=None, default=Genetica.CODON, parts=4):
                self._default = default
                super().__init__(codon, None)
                self._code = codon or self.create(Base, parts)

            def visit(self, visitor):
                visitor.visit_codon(self._code)

            def __repr__(self):
                return "".join(repr(cd) for cd in self._code)

        class Gen(Codon):
            def __init__(self, gen=None, default=Genetica.GEN, parts=4):
                self._default = default
                super().__init__(gen)
                self._code = gen or self.create(Codon, parts)
                self._parts = range(parts)

            def visit(self, visitor):
                visitor.visit_gen(self._code)

        class Cromossomo(Gen):
            def __init__(self, genes=None, default=Genetica.CROMO, parts=4):
                super().__init__(genes, default)
                self._code = genes or self.create(Gen, parts)
                self._parts = range(parts)

            def visit(self, visitor):
                visitor.visit_cromossomo(self._code)

        class DNA(Cromossomo):
            def __init__(self, genes=None, default=Genetica.DNA, parts=4):
                super().__init__(genes, default)
                self._code = genes or self.create(Cromossomo, parts)
                self._parts = range(parts)

            def visit(self, visitor):
                visitor.visit_dna(self._code)

        class Alelo(DNA):
            def __init__(self, dna=None, default=Genetica.DNA):
                super().__init__(dna, default)
                self._code = dna or self.create(DNA, 1)

            def visit(self, visitor):
                visitor.visit_alelo(self._code)

        class Eucarionte:
            def __init__(self, genm=None, genf=None):
                # super().__init__(genm, genf)
                self._codem = genm or Alelo(genm)
                self._codef = genf or Alelo(genf)

            @property
            def code(self):

                def rep(code):
                    return "".join(repr(cd) for cd in code.code)
                return f"\n X:{rep(self._codem)},\n Y:{rep(self._codef)}"

            def visit(self, visitor):
                visitor.visit_eucarionte([self._codef, self._codem])

        self.eucariontes = [Eucarionte(genotipo) for genotipo in genoma]
        # parts = range(2)


class GeneticVisitor:
    def __init__(self):
        ...

    def visit_population(self, code):
        print("\nPopulation", end=':')
        [org.visit(self) for org in code]

    def visit_eucarionte(self, code):
        print("\n Eucarionte", end='=')
        [org.visit(self) for org in code]

    def visit_alelo(self, code):
        print("\n  Alelo", end='-')
        [org.visit(self) for org in code]

    def visit_dna(self, code):
        print("DNA", end=':')
        [org.visit(self) for org in code]

    def visit_cromossomo(self, code):
        print("\n    Cromossomo", end=':')
        [org.visit(self) for org in code]

    def visit_gen(self, code):
        print(" Gen", end='{')
        [org.visit(self) for org in code]
        print(":}", end=',')

    def visit_codon(self, code):
        print("", end=':')
        [org.visit(self) for org in code]
        # print("", end='#')

    def visit_base(self, code):
        _ = self
        print(code, end='')


def main():
    genetica = Genetica((None, None))
    [print("cd", ek.code) for ek in genetica.eucariontes]
    genetica.visit(GeneticVisitor())


if __name__ == '__main__':
    main()
