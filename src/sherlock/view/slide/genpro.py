from slide.pfive import Pfive
from slide.genetic import Genetica
ORG = "peadcgkbz"
SIZ = [int(2.56**(i*0.85))+2 for i, j in enumerate(ORG[:-1])]
# SIZ = [2**(len(ORG)-i) for i, j in enumerate(ORG)]
DBUG = False
LOWER = False


class ProGenDraw(Pfive):

    def __init__(self):
        class Dimensions:
            def __init__(self, c, x, y, dx, dy, s, h, w):
                self.c, self.x, self.y, self.dx, self.dy, self.s, self.h, self.w = c, x, y, dx, dy, s, h, w

            def all(self):
                return self.c, self.x, self.y, self.dx, self.dy, self.s, self.h, self.w
        self._genetica = Genetica((None, None))
        self._count = {k: 0 for k in ORG}
        self._bas_color = {"1": (200, 0, 0), "2": (0, 200, 0),
                           "3": (0, 0, 200), "4": (200, 200, 0),
                           "5": (250, 250, 200)}
        self.d = [Dimensions(0, sz//2, sz//2, sz, sz, sz, sz, 2) for sz in SIZ]
        sz = SIZ[-1]
        # self.d[-1] = Dimensions(0, sz//2, sz//2, sz, sz, sz, sz//2, 1)
        self.d[-2].w = 1
        self.d[-1].x = sz//4
        self.d[-1].s = sz//2
        self.d[-1].dx = sz//2
        super().__init__()

    def draw_org(self, bas, dim, go=False):
        c, x, y, dx, dy, width, height, w = dim.all()
        offx, offy = x + dx * (c % w), y + dy * (c // w)
        self.fill(self.color(*self._bas_color[bas]))
        self.ellipse(offx, offy, width, height) if go else None
        dim.c += 1
        self.d[-1].x, self.d[-1].y = offx-self.d[-1].dx//2, offy-self.d[-1].dy//2
        # self.d[-1].x, self.d[-1].y = offx-self.d[-1].dx/2, offy-self.d[-1].dy//2

    def draw_this_and_sublevels(self, code, color="5", draw=False):
        dimensions = self.d.pop()
        self.draw_org(color, dimensions, draw)
        [org.visit(self) for org in code]
        self.d[-1].c = 0
        self.d.append(dimensions)

    def _draw_org(self, bas, org, go=False):
        self._count[ORG[-org]] = 0
        self._count[ORG[-org-1]] += 1
        size, offx, offy = self.compute_offs(org)
        # self.fill(self.color(*self._bas_color[bas]))
        self.ellipse(offx, offy, size, size) if go else None

    def visit_population(self, code):
        print("\nPopulation", end=':') if DBUG else None
        [org.visit(self) for org in code]

    def visit_eucarionte(self, code):
        print("\n Eucarionte", end='=') if DBUG else None
        self.draw_this_and_sublevels(code, draw=True)

    def visit_alelo(self, code):
        print("\n  Alelo", end='-') if DBUG else None
        [org.visit(self) for org in code]
        # self.draw_this_and_sublevels(code, draw=True)

    def visit_dna(self, code):
        print("DNA", end=':') if DBUG else None
        self.draw_this_and_sublevels(code, draw=True)

    def visit_cromossomo(self, code):
        print("\n    Cromossomo", end=':') if DBUG else None
        self.draw_this_and_sublevels(code, draw=True)

    def visit_gen(self, code):
        print(" Gen", end='{') if DBUG else None
        self.draw_this_and_sublevels(code, draw=True)
        print(":}", end=',') if DBUG else None

    def visit_codon(self, code):
        print("", end=':') if DBUG else None
        self.draw_this_and_sublevels(code, draw=True)
        # print("", end='#')

    def visit_base(self, code):
        _ = self
        print(code, end='') if DBUG else None
        self.draw_this_and_sublevels([], code, draw=True)

    def preload(self):
        ...

    def draw(self):
        self._genetica.visit(self)
        ...

    def setup(self):
        self.createCanvas(2000, 900)
        self.noLoop()

    def compute_offs(self, org):
        x = sum(self._count[o]*SIZ[i] for i, o in enumerate(ORG[:-org]))
        y = SIZ[len(ORG)-org]

        return y, x*4-((self._count["b"]//2) % 2)*y*16, 200+((self._count["b"]//2) % 2)*y*8
        # return SIZ[org], x*4-((self._count["b"]//2) % 2)*y*16, 200+((self._count["b"]//2) % 2)*y*8


def main(*_):
    ProGenDraw()


if __name__ == '__main__':
    main()
