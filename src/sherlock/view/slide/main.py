from random import choice
GTYPE = "12346789"
EMPTY = range(0)


class Tile:
    def __init__(self, pos, tabuleiro, w=3, h=3):
        self.value = pos
        self.pos, self.w, self.h, self.tabuleiro, self.neighbours = pos, w, h, tabuleiro, []
        # self.tabuleiro.append(self)
        self.calc_neighbours()

    def calc_neighbours(self):
        pos, w, h = self.pos, self.w, self.h
        self.neighbours = [pos + dx + dy for dx in [-1, 0, 1] for dy in [-w, 0, w]
                           if (0 <= pos + dx + dy < w * h)
                           and ((pos // w) * w - 1 < pos + dx < (pos // w) * w + w)
                           and abs(dx) != abs(dy / w)]

    def click(self, *_):
        return 1 if any(self.tabuleiro[neigh].move(self) for neigh in self.neighbours) else 0

    def move(self, tile):
        return 0

    def enter(self, pos):
        self.pos = pos
        self.calc_neighbours()
        self.tabuleiro[self.pos] = self


class NoTile(Tile):
    def __init__(self, pos, tabuleiro, w=3, h=3):
        super().__init__(pos, tabuleiro, w=w, h=h)

    def go(self, wind):
        if not wind:
            return 80
        going = self.neighbours[(wind-1) % len(self.neighbours)]
        self.tabuleiro[going].click()
        return self.tabuleiro.fitness()

    def click(self):
        return 0

    def move(self, tile):
        tile_pos, notile_pos = tile.pos, self.pos
        self.pos, _ = tile_pos, tile.enter(notile_pos)
        self.calc_neighbours()
        self.tabuleiro[self.pos] = self
        return 1


class Tabuleiro:
    def __init__(self, pop=40, elder=0.1, good=0.6, xover=0.4, zero_nine=GTYPE, gen=5):
        self.xover = list(range(int(pop*xover)))
        self.tabuleiro = [Tile(pos, self) if pos != 0 else NoTile(pos, self)
                          for pos in range(9)]
        self.no_tile = self.tabuleiro[0]
        self.shuff = self.fit = self.fits = 10000000
        self.elder = int(pop*elder)
        self.good = int(pop*(elder+good))//2
        self.remains_size = pop-self.elder
        self.zero = "0"*gen
        self.shuffle()
        self.pop = [self.navigate("".join(choice(zero_nine)
                                          for _ in range(gen))) for _ in range(pop)]

    def fitness(self):
        return sum(abs(tile.value - pos) for pos, tile in enumerate(self.tabuleiro) if tile.value == 8)

    def append(self, tile):
        raise NotImplementedError  # self.tabuleiro.append(tile)

    def __getitem__(self, item):
        return self.tabuleiro[item]

    def __setitem__(self, key, value):
        self.tabuleiro[key] = value

    def next_gen(self):
        self.pop = sorted(self.pop, reverse=True)
        next_ = self.pop[:self.elder]
        cut = self.good-self.elder
        next_ += self.breed(self.pop[:cut], self.pop[cut:])
        next_ = sorted(next_, reverse=True)
        # [print("phenotype", r) for r in next_]
        self.pop = next_
        print("next_gen", next_[0])
        return next_

    def shuffle(self, turns=20):
        shuffles = sum(choice(self.tabuleiro).click() for _ in range(turns))
        fit = sum(abs(tile.value - pos) for pos, tile in enumerate(self.tabuleiro))
        return shuffles, fit

    def navigate(self, stream):
        self.fits = fit = [self.no_tile.go(int(wind)) for wind in stream]
        self.fit = sum(p*0.1*f for p, f in enumerate(fit))
        self.shuff = min(fit)
        stream = "".join(g if choice(range(30)) != 0 else choice(GTYPE) for g in stream)
        # fen = fit.index(self.shuff)
        # stream = stream[0:fen] + self.zero[fen:]

        # print("stream_fit", stream, shuffles / (fit + 1))
        return 1 / (self.fit + 1), stream

    def _stream_fit(self, stream):
        self.shuff = shuffles = sum(self.tabuleiro[int(tile)].click() for tile in stream)
        self.fit = fit = sum(abs(tile.value - pos) for pos, tile in enumerate(self.tabuleiro))
        # print("stream_fit", stream, shuffles / (fit + 1))
        return 1 / (fit + 1) + 0.1 / (shuffles + 1), stream

    def show(self):
        # print("fit: {}, shuffles: {}".format(*self.shuffle()))
        print(f"fit: {self.fit}, shuffles: {self.shuff}")
        for x in [0, 3, 6]:
            print([t.value for t in self.tabuleiro[x:x + 3]])

    def breed(self, best, remains):
        splits = list(range(1, len(self.pop)))
        remains = [choice(remains) for _ in range(self.remains_size)]
        remains = list(best + remains)
        # gen = range(len(remains))

        def mate(male, female, children=4):
            return ["".join(choice([m, f]) for m, f in split(male, female)) for _ in range(children)]

        def split(male, female):
            splits_ = list(splits)
            splits_ = [splits_.pop(splits_.index(choice(splits_))) for _ in self.xover]
            splits_ = sorted(splits_)
            splits_ = zip([0]+splits_, splits_[0:]+[len(self.pop)])
            splits_ = list(splits_)
            # print("split", splits_)
            couples = [[m, f] for m, f in zip([male[ini:end] for ini, end in splits_],
                       [female[ini:end] for ini, end in splits_]) if m and f]
            # print("split", male, female, list(couples))
            return couples

        def consort(_remains):
            return (_remains.pop(_remains.index(choice(_remains)))[1],
                    _remains.pop(_remains.index(choice(_remains)))[1])  # if len(_remains) > 1 else ('', '')
        breed_ = []
        while len(breed_) < self.remains_size:
            breed_.extend(mate(*consort(remains)))

        breed_ = [self.navigate(fen) for fen in breed_]
        # print("breed", breed_[0])
        return breed_


class Genetica:
    NUCLEOTIDEOS = "1234"
    BASE = "0"
    CODON = ("",)*4
    GEN = (CODON,)*4
    CROMO = (GEN,)*4
    DNA = (CROMO,)*3
    GENOMA = ("",)*20

    @property
    def code(self):
        return "".join(str(ek.code) for ek in self.eucariontes)

    def __init__(self, genoma=GENOMA):

        class Base:
            def __init__(self, code="", default=None, parts=0):
                self._parts = 1
                self._default = default or choice(Genetica.NUCLEOTIDEOS)
                self._code = code or choice(Genetica.NUCLEOTIDEOS)
                self.encoding = []
                Base.encode = lambda *_: self.__encode(range(parts))

            @property
            def code(self):
                # return self.encoding
                return self._code

            def create(self, factory, parts, **kwargs):
                _ = self
                return [factory(**kwargs) for _ in range(parts)]

            def encode(self, parts=EMPTY):
                _, _ = self, parts
                return []

            def __encode(self, parts=EMPTY):
                # print("base encode super", parts)
                _ = parts
                self._code = self.encoding = choice(Genetica.NUCLEOTIDEOS)
                return self.encoding

            def __repr__(self):
                return str(self._code)

        class Codon(Base):
            def __init__(self, codon=None, default=Genetica.CODON, parts=4):
                self._default = default
                super().__init__(codon, None)
                self._code = codon or self.create(Base, parts)
                Codon.encode = lambda *_: self.__encode(range(parts))

            def __repr__(self):
                return "".join(repr(cd) for cd in self._code)

            def __encode(self, parts=EMPTY):
                enc = super().encode
                self.encoding = [enc() for _ in parts]
                # self._code = "".join(code for code in self.encoding)
                print("coodn encode super", self.encoding, self.code)
                return self.encoding

        class Gen(Codon):
            def __init__(self, gen=None, default=Genetica.GEN, parts=4):
                self._default = default
                super().__init__(gen)
                self._code = gen or self.create(Codon, parts)
                self._parts = range(parts)
                Gen.encode = lambda *_: self.__encode(range(parts))

            def __encode(self, parts=EMPTY):
                print(f"Gen {self} encode super", super().encode)
                enc = super().encode
                self.encoding = [enc() for _ in parts]
                # self._code = "".join(code.code for code in self.encoding)
                return self.encoding

        class Cromossomo(Gen):
            def __init__(self, genes=None, default=Genetica.CROMO, parts=4):
                super().__init__(genes, default)
                self._code = genes or self.create(Gen, parts)
                self._parts = range(parts)
                Cromossomo.encode = lambda *_: self.__encode(range(parts))

            def __encode(self, parts=EMPTY):
                print(f"Cromossomo {self} encode super", super().encode)
                enc = super().encode
                self.encoding = [enc() for _ in parts]
                # self._code = "".join(code for code in self.encoding)
                return self.encoding

        class DNA(Cromossomo):
            def __init__(self, genes=None, default=Genetica.DNA, parts=4):
                super().__init__(genes, default)
                self._code = genes or self.create(Cromossomo, parts)
                self._parts = range(parts)
                DNA.encode = lambda *_: self.__encode(range(parts))

            def __encode(self, parts=EMPTY):
                print(f"Cromossomo {self} encode super", super().encode)
                enc = super().encode
                self.encoding = [enc() for _ in parts]
                return self.encoding

        class Alelo(DNA):
            def __init__(self, genm=None, genf=None, default=Genetica.DNA, parts=4):
                super().__init__(genm, default)
                self._codef = DNA(genf)
                Alelo.encode = lambda *_: self.__encode(range(parts))
                # self._code = (genm, genf) if genf and genm else (self._code, self.create(DNA, parts))

            def __encode(self, parts=EMPTY):
                print(f"Cromossomo {self} encode super", super().encode)
                enc = super().encode
                self.encoding = [enc() for _ in parts]
                return self.encoding

            @property
            def code(self):
                return f"\n X:{repr(self._code)},\n Y:{repr(self._codef)}"

        class Eucarionte(Alelo):
            def __init__(self, genm=None, genf=None, parts=2):
                super().__init__(genm, genf)
                Eucarionte.encode = lambda *_: self.__encode(range(parts))

            def __encode(self, parts=EMPTY):
                print(f"Cromossomo {self} encode super", super().encode)
                enc = super().encode
                self.encoding = [enc() for _ in parts]
                return self.encoding

        self.eucariontes = [Eucarionte(genotipo) for genotipo in genoma]
        # parts = range(2)
        self.encoding = [ek.encode() for ek in self.eucariontes]


def main():
    genetica = Genetica((None, None))
    [print(ek) for ek in genetica.encoding]
    [print("cd", ek.code) for ek in genetica.eucariontes]


def _main():
    t = Tabuleiro(pop=25, gen=30, elder=0.3, good=0.4, xover=0.4,)
    t.show()
    # pop = [choice(range(9)) for _ in range(30)]
    # print(f"fit: {t.stream_fit(pop)}")
    # pop = t.navigate("".join(choice(GTYPE) for _ in range(30)))
    # print("stream : {} fit: {}".format(*pop))
    [t.next_gen() for _ in range(20)]
    stream = t.pop[0][1]
    # stream = stream[:t.fits.index(t.shuff)]
    t.navigate(stream)
    t.show()


if __name__ == '__main__':
    main()
