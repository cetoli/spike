# from _spy.vitollino.main import Cena, Texto, Elemento, STYLE, Codigo, Sala
MAPA_ = {
    'A': {'N': 'aLEjWgB', 'L': 'sivjAnO', 'S': 'otHJhF0'},
    'B': {'N': '40K5493', 'L': 'R3bpFXD', 'O': 'dlxY8hi', 'S': 'eYM3Yp9'},
    'C': {'N': 'YJfnhy9', 'L': '94V79TA', 'O': 'Fzz2FNz', 'S': 'LFKXlB1'},
    'D': {'N': '1uWH7rU', 'L': 'b0FcjLq', 'O': '406g75C', 'S': 'HQBtUoQ'},
    'E': {'N': 'uNkTVGg', 'S': 'bculg4O', 'L': 'lUi1E1v', 'O': 'bPBT1d7'},
    'F': {'N': 'iHsggAa', 'S': 'euNeDGs', 'L': 'NqSCDQR', 'O': 'hG4mgby'},
    'G': {'N': 'XDIASJa', 'S': 'ARQZ8CX', 'L': 'pJOegNT', 'O': '9IhOYjO'},
    'H': {'N': 'WjTtZPn', 'L': 'AzvB8hs', 'O': 'SIhLGCP', 'S': 'UVnpzzE'},
    'I': {'N': 'RSdQSH1', 'S': 'UGCRJ0d', 'L': 'jSn4zsl', 'O': 'eG43vn5'},
    'J': {'N': 'MMO11Dv', 'S': 'RkWPb8Z', 'L': 'btv0qfO', 'O': 'lDezYKu'},
    'K': {'N': 'Tx9Q6vW', 'S': 'rrI94Xh', 'L': 'R6gON2E', 'O': 'Mn69uua'},
    'L': {'N': 'oAu9lkN', 'S': 'xTjd7UV', 'L': 'JMQAGvc', 'O': 'UJBMKY7'},
    'M': {'N': 'qoHwGLW', 'S': '5P3U1Ai', 'L': '1UXBodl', 'O': 'AC2KgZg'},
    'N': {'N': 'KVlUf94', 'L': 'f6vR0tY', 'O': 'GE8IsRM', 'S': 'RfUP0ez'},
    'O': {'N': 'lOT96Hr', 'S': 'HtRKv7X', 'L': 'uvPjc14', 'O': 'I7Gn0Xx'},
    'P': {'N': 'OutDPac', 'S': 'sAIhp4b', 'L': 'dc2Ol59', 'O': '9IBwxjI'},
    'Q': {'N': 'JRYlZeN', 'S': '4BCiuYZ', 'L': 'ek4cwBg', 'O': 'vmZHZmr'},
    'R': {'N': 'qnjq624', 'S': 'nZvwdhP', 'L': 'gS4rXYk', 'O': '2Z36mLI'}
}
MAPA = """
A_NORTE = "https://i.imgur.com/aLEjWgB.png"
A_LESTE = "https://i.imgur.com/sivjAnO.png"
A_SUL = "https://i.imgur.com/otHJhF0.png"
B_NORTE = "https://i.imgur.com/40K5493.png"
B_LESTE = "https://i.imgur.com/R3bpFXD.png"
B_OESTE = "https://i.imgur.com/dlxY8hi.png"
B_SUL = "https://i.imgur.com/eYM3Yp9.png"
C_NORTE = "https://i.imgur.com/YJfnhy9.png"
C_LESTE = "https://i.imgur.com/94V79TA.png"
C_OESTE = "https://i.imgur.com/Fzz2FNz.png"
C_SUL = "https://i.imgur.com/LFKXlB1.png"
D_NORTE = "http://i.imgur.com/1uWH7rU.png"
D_LESTE = "https://i.imgur.com/b0FcjLq.png"
D_OESTE = "https://i.imgur.com/406g75C.png"
D_SUL = "https://i.imgur.com/HQBtUoQ.png"
E_NORTE= "https://i.imgur.com/uNkTVGg.png"
E_SUL = "http://i.imgur.com/bculg4O.png"
E_LESTE = "https://i.imgur.com/lUi1E1v.png"
E_OESTE = "https://i.imgur.com/bPBT1d7.png"
F_NORTE= "https://i.imgur.com/iHsggAa.png"
F_SUL = "http://i.imgur.com/euNeDGs.png"
F_LESTE = "https://i.imgur.com/NqSCDQR.png"
F_OESTE = "https://i.imgur.com/hG4mgby.png"
G_NORTE = "https://i.imgur.com/XDIASJa.png"
G_SUL = "https://i.imgur.com/ARQZ8CX.png"
G_LESTE = "https://i.imgur.com/pJOegNT.png"
G_OESTE = "http://i.imgur.com/9IhOYjO.png"
H_NORTE = "https://i.imgur.com/WjTtZPn.png"
H_LESTE = "https://i.imgur.com/AzvB8hs.png"
H_OESTE = "https://i.imgur.com/SIhLGCP.png"
H_SUL = "https://i.imgur.com/UVnpzzE.png"
I_NORTE= "https://i.imgur.com/RSdQSH1.png"
I_SUL = "https://i.imgur.com/UGCRJ0d.png"
I_LESTE = "https://i.imgur.com/jSn4zsl.png"
I_OESTE= "https://i.imgur.com/eG43vn5.png"
J_NORTE= "https://i.imgur.com/MMO11Dv.png"
J_SUL = "https://i.imgur.com/RkWPb8Z.png"
J_LESTE = "https://i.imgur.com/btv0qfO.png"
J_OESTE= "https://i.imgur.com/lDezYKu.png"
K_NORTE= "https://i.imgur.com/Tx9Q6vW.png"
K_SUL = "https://i.imgur.com/rrI94Xh.png"
K_LESTE = "https://i.imgur.com/R6gON2E.png"
K_OESTE= "https://i.imgur.com/Mn69uua.png"
L_NORTE= "https://i.imgur.com/oAu9lkN.png"
L_SUL = "https://i.imgur.com/xTjd7UV.png"
L_LESTE = "https://i.imgur.com/JMQAGvc.png"
L_OESTE = "http://i.imgur.com/UJBMKY7.png"
M_NORTE = "https://i.imgur.com/qoHwGLW.png"
M_SUL = "https://i.imgur.com/5P3U1Ai.png"
M_LESTE = "http://i.imgur.com/1UXBodl.png"
M_OESTE = "https://i.imgur.com/AC2KgZg.png"
N_NORTE = "https://i.imgur.com/KVlUf94.png"
N_LESTE = "https://i.imgur.com/f6vR0tY.png"
N_OESTE = "https://i.imgur.com/GE8IsRM.png"
N_SUL = "https://i.imgur.com/RfUP0ez.png"
O_NORTE = "https://i.imgur.com/lOT96Hr.png"
O_SUL = "https://i.imgur.com/HtRKv7X.png"
O_LESTE = "https://i.imgur.com/uvPjc14.png"
O_OESTE = "https://i.imgur.com/I7Gn0Xx.png"
P_NORTE = "https://i.imgur.com/OutDPac.png"
P_SUL = "https://i.imgur.com/sAIhp4b.png"
P_LESTE = "https://i.imgur.com/dc2Ol59.png"
P_OESTE = "https://i.imgur.com/9IBwxjI.png"
Q_NORTE= "https://i.imgur.com/JRYlZeN.png"
Q_SUL = "http://i.imgur.com/4BCiuYZ.png"
Q_LESTE = "https://i.imgur.com/ek4cwBg.png"
Q_OESTE= "https://i.imgur.com/vmZHZmr.png"
R_NORTE= "https://i.imgur.com/qnjq624.png"
R_SUL = "https://i.imgur.com/nZvwdhP.png"
R_LESTE = "https://i.imgur.com/gS4rXYk.png"
R_OESTE = "http://i.imgur.com/2Z36mLI.png"
"""
MAPA = MAPA.split("\n")


def main():
    alpha = {
        chr(alf): {
            # img.split("\n")[0].split("_")[1]: img.split("/")[-1] for img in MAPA if f"{alf}_" in img
            img[2:3]: img.split("/")[-1].split(".")[0] for img in MAPA if f"{chr(alf)}_" in img
        } for alf in range(ord("A"), ord("S"))}
    for k, v in alpha.items():
        print(f"'{k}': {v},")


if __name__ == '__main__':
    main()
