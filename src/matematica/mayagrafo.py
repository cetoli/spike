#! /usr/bin/env python
# -*- coding: UTF8 -*-
"""
############################################################
Avaliando a educação matemática em grafo
############################################################

:Author: *Carlo E. T. Oliveira*
:Contact: carlo@nce.ufrj.br
:Date: $Date: 2012/02/08  $
:Status: This is a "work in progress"
:Revision: $Revision: 0.1 $
:Home: `Labase <http://labase.nce.ufrj.br/>`__
:Copyright: ©2011, `GPL <http://is.gd/3Udt>`__. 
"""
__author__ = "Carlo E. T. Oliveira (cetoli@yahoo.com.br) $Author: cetoli $"
__version__ = "0.1 $Revision$"[10:-1]
__date__ = "2012/02/08 $Date$"

import numpy

try:
    from mayavi.mlab import *
except ImportError:
    from enthought.mayavi.mlab import *

from random import choice as cz

MM = (-2, -1, 1, 2)
MUM = (-1, 1)
PHI = (1 + 5 ** 0.5) / 2
IHP = 1 / PHI
PHV = [0, IHP, PHI]


def rotate(x, y=1):
    if len(x) == 0:
        return x
    y = y % len(x)  # Normalize y, using modulo - even works for negative y
    return x[y:] + x[:y]


DDV = [[1, 1, 1]] + [rotate(PHV, n) for n in range(3)]


def center(i): return i - (2, -2)[i < 0]


# def center(i): return 0
def dodecphi(a=1):
    PP = set([(a * i * x, a * j * y, a * k * z) for i, j, k in DDV for x in MUM for y in MUM for z in MUM])
    return [[c[n] for c in PP] for n in range(3)]


def phidodec():
    x, y, z = dodecphi(18)
    u, v, w = dodecphi()
    obj = quiver3d(x, y, z, u, v, w, line_width=3, scale_factor=2, mode='cone')


def dodec():
    pentang = 2 * numpy.pi / 10
    decang = 0  # pentang/2
    ph = (1 + 5 ** 0.5) / 2
    a = 18
    b = a - (a / ph)

    x = [(a - b * j) * numpy.sin(pentang * i + j * decang) for j in (0, 1) for i in range(10)]
    y = [(a - b * j) * numpy.cos(pentang * i + j * decang) for j in (0, 1) for i in range(10)]
    z = [k * j for k in (b, a) for i in range(5) for j in (-1, 1)]
    u = [-2 * numpy.sin(pentang * i + j * decang) for j in (0, 1) for i in range(10)]
    v = [-2 * numpy.cos(pentang * i + j * decang) for j in (0, 1) for i in range(10)]
    w = [-1 * j - j * k * 1 for k in (0, 1) for i in range(5) for j in (-1, 1)]
    # w = [0 for i in range(10)]
    obj = quiver3d(x, y, z, u, v, w, line_width=3, scale_factor=2, mode='cone')


def vector(x, y, z):
    # print '\nx',x
    # x =numpy.array([[[a+ cz(MM) for a in b]for b in c] for c in x])
    # y =numpy.array([[[a+ cz(MM) for a in b]for b in c] for c in y])
    # z =numpy.array([[[a+ cz(MM) for a in b]for b in c] for c in z])
    x = numpy.array([[[center(a) for a in b] for b in c] for c in x])
    y = numpy.array([[[center(a) for a in b] for b in c] for c in y])
    z = numpy.array([[[center(a) for a in b] for b in c] for c in z])
    # if x >0 :
    #    x -= 1
    # else:
    #    x +=1
    # if y >0 :
    #    y -= 1
    # else:
    #    y +=1
    # if z >0 :
    #    z -= 1
    # else:
    #    z +=1
    # vt = y+ cz(MM),z+ cz(MM),x- cz(MM)
    return x, y, z
    return vt


def test_quiver3d():
    x, y, z = numpy.mgrid[-1:1, -1:1, -2:3]
    x = numpy.array([[[a + cz(MM) for a in b] for b in c] for c in x])
    y = numpy.array([[[a + cz(MM) for a in b] for b in c] for c in y])
    z = numpy.array([[[a + cz(MM) for a in b] for b in c] for c in z])
    r = numpy.sqrt(x ** 2 + y ** 2 + z ** 4)
    u = y * numpy.sin(r) / (r + 0.001)
    v = -x * numpy.sin(r) / (r + 0.001)
    w = numpy.zeros_like(z)
    # print 'x',x
    # print '\ny\n', y
    # print '\nz\n', z
    # obj = quiver3d(x, y, z, u, v, w, line_width=3, scale_factor=1, mode = 'cone')
    obj = quiver3d(x, y, z, vector, line_width=3, scale_factor=1, mode='cone')
    # return obj


def main():
    # test_quiver3d()
    phidodec()


if __name__ == "__main__":
    main()
