#! /usr/bin/env python
# -*- coding: UTF8 -*-
"""
############################################################
Menu Strategy: strategies to build a menu
############################################################

:Author: *Carlo E. T. Oliveira*
:Contact: carlo@nce.ufrj.br
:Date: $Date: 2011/10/02  $
:Status: This is a "work in progress"
:Revision: $Revision: 0.1 $
:Home: `Labase <http://labase.nce.ufrj.br/>`__
:Copyright: ©2011, `GPL <http://is.gd/3Udt>`__. 
"""
__author__  = "Carlo E. T. Oliveira (carlo@ufrj.br) $Author: cetoli $"
__version__ = "0.1 $Revision$"[10:-1]
__date__    = "2011/10/02 $Date$"

from pygame_factory import ICONS
class ActDecorator:
    def __init__(self, object, action = None):
        self.action = action
        self.object = object
        self._action()
    def _action(self):
        self.object.gui.act(self)
    def collide(self,x,y):
        return self.object.avatar.collidepoint(x,y)

class Wall3DMenu:
    def __init__(self,gui):
        self.gui = gui
        #app = self.gui.slate('Jeppeto',50, 50, 700, 500)
        self.act = ActDecorator(self,self.activate)
        self.dx= 700
        self.cxi = self.dxi = -100
        self._create()
        self.gui.roll(self)
        
    def start(self, name):
        self.gui.create_game(self, name)
        
    def action(self,x,y):
        self.cxi = x
    def activate(self,ev):
        self.dxi += (4*self.cxi -self.dxi)/5
        x = self.cxi
        a,b,c = 0,0,0
        for ind, flyer in enumerate(self.flyers[:40]):
            #scale =128 -96*abs(x-nx-96)/96
            scale = 64
            x, y = 10+(ind % 10) * 70, 10+(ind // 10) * 70
            flyer.scale(scale,scale)
            flyer.move(x,y)
        
    def _create(self):#, source, x, y ,w , h, l = None, f= None, buff= None):
        self.names = [x for x in ICONS.namelist() if ('128' in x) and ('.png' in x)]
        #print self.names[:10]
        dx =7000
        px = lambda i:self.dxi+ i*70
        self.flyers = [self.gui.flyer(name,px(ind),100,128,128)
                       for ind,name in enumerate(self.names)]
        self.activate(None)

class DockMenu:
    def __init__(self,gui):
        self.gui = gui
        #app = self.gui.slate('Jeppeto',50, 50, 700, 500)
        self.act = ActDecorator(self,self.activate)
        self.dx= 700
        self.cxi = self.dxi = -100
        self._create()
        self.gui.roll(self)
        
    def start(self, name):
        self.gui.create_game(self, name)
        
    def action(self,x,y):
        self.cxi = x
    def activate(self,ev):
        self.dxi += (4*self.cxi -self.dxi)/5
        x = self.cxi
        a,b,c = 0,0,0
        for ind, flyer in enumerate(self.flyers):
            nx = -self.dxi+ a + ind*34
            if x-96 <nx+40 <= x +96:
                #scale =128 -96*abs(x-nx-96)/96
                scale =128 -96*abs(x-nx-40)/96
                flyer.scale(scale,scale)
                flyer.move(nx,100)
                a +=scale-32
            else:
                flyer.move(nx,100)
                flyer.scale(32,32)
        
    def _create(self):#, source, x, y ,w , h, l = None, f= None, buff= None):
        self.names = [x for x in ICONS.namelist() if ('128' in x) and ('.png' in x)]
        #print self.names[:10]
        dx =7000
        px = lambda i:self.dxi+ i*70
        self.flyers = [self.gui.flyer(name,px(ind),100,128,128)
                       for ind,name in enumerate(self.names)]
        self.activate(None)

from math import sin, cos, pi
class CarrousselMenu:
    def __init__(self,gui):
        self.gui = gui
        #app = self.gui.slate('Jeppeto',50, 50, 700, 500)
        self.act = ActDecorator(self,self.activate)
        self.dx= 700
        self.dxi = -100
        self._create()
        self.gui.roll(self)
        
    def start(self, name):
        self.gui.create_game(self, name)
        
    def action(self,x,y):
        self.dxi = -(x-400)/4000.0
    def activate(self,ev):
        delta =  2*pi / len(self.flyers)
        self.alpha += self.dxi
        for ind, flyer in enumerate(self.flyers):
            beta = ind*delta+self.alpha
            #delta = 400*sin(beta)
            if -pi < (beta+pi/2) %(2*pi)< pi:
                sc = 96 - 80*abs(sin(beta))
                posx = 400+ 318*sin(beta)*(1+sc/128)
                
            else:
                sc = 16
                posx = 400+ 350*sin(beta)
            flyer.move(posx,150+150*cos(beta))
            flyer.scale(sc,sc)
            #
    def _create(self):#, source, x, y ,w , h, l = None, f= None, buff= None):
        self.names = [x for x in ICONS.namelist() if ('128' in x) and ('.png' in x)]
        self.classes = {}
        for name in self.names:
            clazz = name.split('_')[0]
            if clazz in self.classes:
                self.classes[clazz].append(name)
            else:
                self.classes[clazz] = [name]
        self.carroussel = self.classes.values()
        self.alpha = 0
        
        #print self.names[:10]
        dx =7000
        px = lambda i:self.dxi+ i*70
        self.flyers = [self.gui.flyer(name[0],px(ind),100,128,128)
                       for ind,name in enumerate(self.classes.values()) if len(name) >2]
        self.activate(None)

def main():
    from pygame_factory import GUI
    #main = DockMenu(GUI())
    #main = Wall3DMenu(GUI())
    main = CarrousselMenu(GUI())
    main.start('Jeppeto')
 
if __name__ == "__main__":
    main()

