#! /usr/bin/env python
# -*- coding: UTF8 -*-
"""
############################################################
Avaliando a educação matemática em grafo
############################################################

:Author: *Carlo E. T. Oliveira*
:Contact: carlo@nce.ufrj.br
:Date: $Date: 2011/09/09  $
:Status: This is a "work in progress"
:Revision: $Revision: 0.1 $
:Home: `Labase <http://labase.nce.ufrj.br/>`__
:Copyright: ©2011, `GPL <http://is.gd/3Udt>`__. 
"""
__author__ = "Carlo E. T. Oliveira (cetoli@yahoo.com.br) $Author: cetoli $"
__version__ = "0.1 $Revision$"[10:-1]
__date__ = "2011/09/09 $Date$"

# from networkx import Graph, draw,draw_circular,draw_random,draw_spectral
# import matplotlib.pyplot as plt
# from visual import sphere
from ubigraph import Ubigraph
from random import randint
from matplotlib.pyplot import boxplot, show, figure, plot, polar
from csv import writer
import orange
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.projections.polar import PolarAxes
from matplotlib.projections import register_projection


def radar_factory(num_vars, frame='circle'):
    """Create a radar chart with `num_vars` axes."""
    # calculate evenly-spaced axis angles
    theta = 2 * np.pi * np.linspace(0, 1 - 1. / num_vars, num_vars)

    # rotate theta such that the first axis is at the top
    # theta += np.pi/2

    def draw_poly_frame(self, x0, y0, r):
        # TODO: use transforms to convert (x, y) to (r, theta)
        verts = [(r * np.cos(t) + x0, r * np.sin(t) + y0) for t in theta]
        return plt.Polygon(verts, closed=True, edgecolor='k')

    def draw_circle_frame(self, x0, y0, r):
        return plt.Circle((x0, y0), r)

    frame_dict = {'polygon': draw_poly_frame, 'circle': draw_circle_frame}
    if frame not in frame_dict:
        raise ValueError('unknown value for `frame`: %s' % frame)

    class RadarAxes(PolarAxes):
        """Class for creating a radar chart (a.k.a. a spider or star chart)

        http://en.wikipedia.org/wiki/Radar_chart
        """
        name = 'radar'
        # use 1 line segment to connect specified points
        RESOLUTION = 1
        # define draw_frame method
        draw_frame = frame_dict[frame]

        def fill(self, *args, **kwargs):
            """Override fill so that line is closed by default"""
            closed = kwargs.pop('closed', True)
            return super(RadarAxes, self).fill(closed=closed, *args, **kwargs)

        def plot(self, *args, **kwargs):
            """Override plot so that line is closed by default"""
            lines = super(RadarAxes, self).plot(*args, **kwargs)
            for line in lines:
                self._close_line(line)

        def _close_line(self, line):
            x, y = line.get_data()
            # FIXME: markers at x[0], y[0] get doubled-up
            if x[0] != x[-1]:
                x = np.concatenate((x, [x[0]]))
                y = np.concatenate((y, [y[0]]))
                line.set_data(x, y)

        def set_varlabels(self, labels):
            self.set_thetagrids(theta * 180 / np.pi, labels)

        def _gen_axes_patch(self):
            x0, y0 = (0.5, 0.5)
            r = 0.5
            return self.draw_frame(x0, y0, r)

    register_projection(RadarAxes)
    return theta


LEGEND = 'NNO,SND,NRO,SBI,NPR,UMC,PDC,CME,FBT,FPL,OEF,RDP,MFC,PEA,NDS,TDI,NAT,FND,GEO,TAB'.split(',')

PDB = dict([
    ('Margareth',
     [['m1', "[u'e1', u'e2', u'e3', u'e5']"], ['m2', "[u'e1', u'e2', u'e5', u'e3']"], ['m3', "[u'e1', u'e3', u'e5']"],
      ['m4', "[u'e5', u'e1', u'e2', u'e3']"], ['m5', "[u'e1', u'e3']"], ['m6', "[u'e7', u'e1', u'e2', u'e3', u'e6']"],
      ['m7', "[u'e1', u'e2', u'e5', u'e3']"], ['m8', "[u'e1', u'e2', u'e3']"], ['m9', "[u'e3', u'e4', u'e6', u'e7']"],
      ['m10', "[u'e3', u'e4', u'e6', u'e7']"], ['m11', "[u'e3', u'e4', u'e6']"],
      ['m12', "[u'e1', u'e2', u'e5', u'e3']"], ['m13', "[u'e1', u'e2', u'e3']"],
      ['m14', "[u'e1', u'e7', u'e3', u'e4', u'e6']"], ['m15', "[u'e3', u'e4', u'e6', u'e7']"],
      ['m16', "[u'e1', u'e3', u'e4', u'e6']"], ['m17', "[u'e1', u'e2', u'e3']"], ['m18', "[u'e1', u'e3', u'e2']"],
      ['m19', "[u'e3', u'e4', u'e6']"], ['m20', "[u'e1', u'e2', u'e3']"]]),
    ('Fabiana Castro Franco',
     [['m1', "[u'e3', u'e1', u'e2']"], ['m2', "[u'e3', u'e1']"], ['m3', "[u'e3']"], ['m4', "[u'e5', u'e3']"],
      ['m5', "[u'e3']"], ['m6', "[u'e7', u'e3']"], ['m7', "[u'e1', u'e2', u'e3']"], ['m8', "[u'e3', u'e1']"],
      ['m9', "[u'e4', u'e3']"], ['m10', "[u'e3', u'e4']"], ['m11', "[u'e4', u'e3']"], ['m12', "[u'e3', u'e2', u'e1']"],
      ['m13', "[u'e3', u'e1']"], ['m14', "[u'e3', u'e7']"], ['m15', "[u'e3']"], ['m16', "[u'e3', u'e4']"],
      ['m17', "[u'e1', u'e3']"], ['m18', "[u'e3', u'e1', u'e2']"], ['m19', "[u'e3']"],
      ['m20', "[u'e1', u'e3', u'e2']"]]),
    ("Thiago Dal'Bello",
     [['m1', "[u'e1', u'e3']"], ['m2', "[u'e3', u'e1']"], ['m3', "[u'e1', u'e3']"], ['m4', "[u'e1', u'e3']"],
      ['m5', "[u'e3', u'e1']"], ['m6', "[u'e2', u'e4', u'e7']"], ['m7', "[u'e1']"], ['m8', "[u'e1']"],
      ['m9', "[u'e3']"], ['m10', "[u'e1', u'e3']"], ['m11', "[u'e3', u'e2', u'e4', u'e7']"], ['m12', "[u'e5', u'e1']"],
      ['m13', "[u'e1']"], ['m14', "[u'e7', u'e2', u'e4']"], ['m15', "[u'e1', u'e7']"], ['m16', "[u'e1', u'e2', u'e4']"],
      ['m17', "[u'e3', u'e1']"], ['m18', "[u'e3']"], ['m19', "[u'e4', u'e7', u'e2']"], ['m20', "[u'e1', u'e2']"]]),
    ('Jailson',
     [['m1', "[u'e1', u'e2', u'e5', u'e7']"], ['m2', "[u'e3', u'e4']"], ['m3', "[u'e1', u'e3']"], ['m4', "[u'e6']"],
      ['m5', "[u'e1']"], ['m6', "[u'e4', u'e7']"], ['m7', "[u'e3']"], ['m8', "[u'e1', u'e3']"], ['m9', "[u'e4']"],
      ['m10', "[u'e3', u'e4']"], ['m11', "[u'e4', u'e3']"], ['m12', "[u'e2', u'e5']"], ['m13', "[u'e2']"],
      ['m14', "[u'e7', u'e4']"], ['m15', "[u'e1']"], ['m16', "[u'e3']"], ['m17', "[u'e1', u'e2']"],
      ['m18', "[u'e3', u'e4']"], ['m19', "[u'e7', u'e4']"], ['m20', "[u'e1']"]]),
    ('Fabio quintanilha',
     [['m1', "[u'e1', u'e2', u'e3']"], ['m2', "[u'e1', u'e2', u'e3', u'e5']"], ['m3', "[u'e1', u'e2', u'e3', u'e5']"],
      ['m4', "[u'e1', u'e5', u'e3']"], ['m5', "[u'e1', u'e3', u'e2']"], ['m6', "[u'e7', u'e5', u'e3']"],
      ['m7', "[u'e1', u'e2', u'e3']"], ['m8', "[u'e1', u'e6', u'e2']"], ['m9', "[u'e4', u'e7', u'e6']"],
      ['m10', "[u'e7', u'e4', u'e6', u'e3']"], ['m11', "[u'e4', u'e7', u'e6']"],
      ['m12', "[u'e3', u'e5', u'e2', u'e1']"], ['m13', "[u'e1', u'e2', u'e3']"],
      ['m14', "[u'e4', u'e7', u'e1', u'e2']"], ['m15', "[u'e4', u'e7']"], ['m16', "[u'e1', u'e2', u'e3', u'e6']"],
      ['m17', "[u'e1', u'e2', u'e5']"], ['m18', "[u'e3', u'e2', u'e1', u'e6']"],
      ['m19', "[u'e5', u'e4', u'e7', u'e3']"], ['m20', "[u'e1', u'e2']"]]),
    ('Jacqueline Silva', [['m1', "[u'e1', u'e2', u'e5']"], ['m2', "[u'e3', u'e2', u'e1', u'e5']"], ['m3', "[u'e3']"],
                          ['m4', "[u'e3', u'e5', u'e1', u'e2']"], ['m5', "[u'e1', u'e3']"],
                          ['m6', "[u'e6', u'e3', u'e7']"], ['m7', "[u'e3', u'e2', u'e1']"],
                          ['m8', "[u'e1', u'e2', u'e3']"], ['m9', "[u'e3', u'e4']"], ['m10', "[u'e3', u'e4']"],
                          ['m11', "[u'e4', u'e6', u'e3']"],
                          ['m12', "[u'e1', u'e2', u'e3', u'e4', u'e5', u'e6', u'e7']"],
                          ['m13', "[u'e1', u'e2', u'e3']"], ['m14', "[u'e1', u'e3', u'e7']"], ['m15', "[u'e3', u'e4']"],
                          ['m16', "[u'e3', u'e4', u'e2']"], ['m17', "[u'e5', u'e1', u'e2', u'e3']"],
                          ['m18', "[u'e3', u'e2', u'e5']"], ['m19', "[u'e3', u'e4']"],
                          ['m20', "[u'e2', u'e1', u'e5']"]]),
    ('Elizabeth', [['m1', "[u'e1', u'e2', u'e3']"], ['m2', "[u'e1', u'e2', u'e3']"], ['m3', "[u'e1', u'e2', u'e3']"],
                   ['m4', "[u'e1', u'e2', u'e3']"], ['m5', "[u'e1', u'e2', u'e3']"],
                   ['m6', "[u'e4', u'e1', u'e2', u'e3', u'e5', u'e7']"], ['m7', "[u'e1', u'e2', u'e3']"],
                   ['m8', "[u'e4', u'e3', u'e1', u'e2', u'e6']"], ['m9', "[u'e1', u'e5', u'e3', u'e4']"],
                   ['m10', "[u'e4', u'e7']"], ['m11', "[u'e1', u'e3', u'e6', u'e7', u'e4']"],
                   ['m12', "[u'e5', u'e6', u'e3']"], ['m13', "[u'e1', u'e3']"], ['m14', "[u'e7', u'e2', u'e3', u'e4']"],
                   ['m15', "[u'e4', u'e6', u'e3']"], ['m16', "[u'e5', u'e3']"], ['m17', "[u'e1', u'e2', u'e3']"],
                   ['m18', "[u'e1', u'e2', u'e3']"], ['m19', "[u'e4', u'e7', u'e1', u'e3']"]]),
    ('Fabio', [['m1', "[u'e1', u'e3', u'e2', u'e5', u'e7']"], ['m2', "[u'e1', u'e3', u'e2', u'e5']"],
               ['m3', "[u'e3', u'e1', u'e2', u'e5']"], ['m4', "[u'e1', u'e3', u'e2', u'e5']"],
               ['m5', "[u'e1', u'e3', u'e5']"], ['m6', "[u'e7', u'e2', u'e1', u'e6', u'e4']"],
               ['m7', "[u'e1', u'e2', u'e3']"], ['m8', "[u'e3', u'e2', u'e1']"], ['m9', "[u'e3', u'e6', u'e4', u'e7']"],
               ['m10', "[u'e3', u'e4', u'e7']"], ['m11', "[u'e3', u'e6', u'e4']"],
               ['m12', "[u'e4', u'e5', u'e3', u'e2', u'e1', u'e7']"], ['m13', "[u'e1', u'e3', u'e5']"],
               ['m14', "[u'e7', u'e6', u'e4', u'e1', u'e2']"], ['m15', "[u'e4', u'e7', u'e3']"],
               ['m16', "[u'e2', u'e3', u'e1', u'e6']"], ['m17', "[u'e1', u'e5', u'e7', u'e3', u'e2']"],
               ['m18', "[u'e3', u'e1', u'e2', u'e7', u'e5', u'e4']"], ['m19', "[u'e7', u'e4', u'e3', u'e6']"],
               ['m20', "[u'e7', u'e5', u'e3', u'e1', u'e2']"]]),
    ('Adriano',
     [['m1', "[u'e1']"], ['m2', "[u'e1']"], ['m3', "[u'e1']"], ['m4', "[u'e1']"], ['m5', "[u'e3']"], ['m6', "[u'e7']"],
      ['m7', "[u'e1']"], ['m8', "[u'e1']"], ['m9', "[u'e4']"], ['m10', "[u'e4']"], ['m11', "[u'e4']"],
      ['m12', "[u'e3']"], ['m13', "[u'e1']"], ['m14', "[u'e4']"], ['m15', "[u'e5']"], ['m16', "[u'e3']"],
      ['m17', "[u'e1', u'e3']"], ['m18', "[u'e3', u'e2']"], ['m19', "[u'e4']"], ['m20', "[u'e1']"]]),
    ('Rodrigo Guedes',
     [['m1', "[u'e5']"], ['m2', "[u'e3', u'e5']"], ['m3', "[u'e3']"], ['m4', "[u'e1']"], ['m5', "[u'e1']"],
      ['m6', "[u'e7']"], ['m7', "[u'e1']"], ['m8', "[u'e1']"], ['m9', "[u'e4']"], ['m10', "[u'e4']"],
      ['m11', "[u'e3']"], ['m12', "[u'e5']"], ['m13', "[u'e2']"], ['m14', "[u'e4']"], ['m15', "[u'e4']"],
      ['m16', "[u'e5']"], ['m17', "[u'e1']"], ['m18', "[u'e3']"], ['m19', "[u'e6']"], ['m20', "[u'e1']"]]),
    ('Rosiney', [['m1', "[u'e1', u'e2', u'e3', u'e6']"], ['m2', "[u'e1', u'e3', u'e6', u'e5', u'e7']"],
                 ['m3', "[u'e1', u'e2', u'e3', u'e5']"], ['m4', "[u'e1', u'e3', u'e5']"],
                 ['m5', "[u'e1', u'e3', u'e2']"], ['m6', "[u'e5', u'e7', u'e2']"], ['m7', "[u'e1', u'e2', u'e7']"],
                 ['m8', "[u'e1', u'e2']"], ['m9', "[u'e7', u'e4']"], ['m10', "[u'e7', u'e4']"], ['m11', "[u'e7']"],
                 ['m12', "[u'e5', u'e3']"], ['m13', "[u'e7', u'e2']"], ['m14', "[u'e4', u'e7', u'e2']"],
                 ['m15', "[u'e5', u'e7']"], ['m16', "[u'e3', u'e5']"], ['m17', "[u'e1', u'e3']"],
                 ['m18', "[u'e1', u'e2', u'e5']"], ['m19', "[u'e4', u'e7']"], ['m20', "[u'e1', u'e2']"]])
])

DB = dict([

    ('Larissa', [['m1', "[u'e1']"], ['m2', "[u'e1']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e5']"],
                 ['m6', "[u'e7', u'e6']"], ['m7', "[u'e1']"], ['m8', "[u'e1']"], ['m9', "[u'e3', u'e4']"],
                 ['m10', "[u'e3']"], ['m11', "[u'e6', u'e7']"], ['m12', "[u'e1']"], ['m13', "[u'e5']"],
                 ['m14', "[u'e7', u'e6']"], ['m15', "[u'e7', u'e6']"], ['m16', "[u'e3']"], ['m17', "[u'e5']"],
                 ['m18', "[u'e2']"], ['m19', "[u'e4', u'e5']"], ['m20', "[u'e5']"]]),
    ('Iago', [['m1', "[u'e3', u'e1', u'e2']"], ['m2', "[u'e1', u'e2', u'e3']"], ['m3', "[u'e1', u'e2', u'e3']"],
              ['m4', "[u'e3', u'e2', u'e1']"], ['m5', "[u'e3', u'e2']"], ['m6', "[u'e6', u'e7']"],
              ['m7', "[u'e5', u'e2', u'e1']"], ['m8', "[u'e1', u'e2']"], ['m9', "[u'e3', u'e4', u'e7']"],
              ['m10', "[u'e7', u'e6', u'e2', u'e3']"], ['m11', "[u'e4', u'e7']"], ['m12', "[u'e5']"],
              ['m13', "[u'e1', u'e2']"], ['m14', "[u'e6', u'e7', u'e2', u'e3']"], ['m15', "[u'e2', u'e4']"],
              ['m16', "[u'e1', u'e2', u'e3']"], ['m17', "[u'e2', u'e1']"], ['m18', "[u'e3', u'e2', u'e1']"],
              ['m19', "[u'e7', u'e6', u'e4', u'e1', u'e2', u'e3']"], ['m20', "[u'e1', u'e2']"]]),
    ('Petinate', [['m1', "[u'e2', u'e3', u'e1', u'e5']"], ['m2', "[u'e5', u'e7']"], ['m3', "[u'e5', u'e2', u'e3']"],
                  ['m4', "[u'e5', u'e3', u'e2']"], ['m5', "[u'e3', u'e5', u'e2']"], ['m6', "[u'e7', u'e6']"],
                  ['m7', "[u'e1', u'e2', u'e3']"], ['m8', "[u'e2', u'e1', u'e5', u'e3']"],
                  ['m9', "[u'e3', u'e4', u'e6']"], ['m10', "[u'e3', u'e4']"], ['m11', "[u'e1', u'e3']"],
                  ['m12', "[u'e5', u'e2']"], ['m13', "[u'e5', u'e3']"], ['m14', "[u'e6', u'e4', u'e7']"],
                  ['m15', "[u'e2', u'e5', u'e7', u'e3']"], ['m16', "[u'e7', u'e1']"], ['m17', "[u'e1', u'e7', u'e2']"],
                  ['m18', "[u'e5']"], ['m19', "[u'e4', u'e6', u'e2', u'e7', u'e3']"], ['m20', "[u'e1', u'e2']"]]),
    ('Andre Mendez',
     [['m1', "[u'e1']"], ['m2', "[u'e1']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e1']"], ['m6', "[u'e7']"],
      ['m7', "[u'e3']"], ['m8', "[u'e2']"], ['m9', "[u'e4']"], ['m10', "[u'e6']"], ['m11', "[u'e6']"],
      ['m12', "[u'e1']"], ['m13', "[u'e3']"], ['m14', "[u'e6', u'e7']"], ['m15', "[u'e7', u'e6']"], ['m16', "[u'e3']"],
      ['m17', "[u'e5']"], ['m18', "[u'e3']"], ['m19', "[u'e6']"], ['m20', "[u'e1']"]]),
    ('Igor', [['m1', "[u'e1', u'e2', u'e3', u'e5', u'e4']"], ['m2', "[u'e7', u'e5']"], ['m3', "[u'e5', u'e2', u'e3']"],
              ['m4', "[u'e5', u'e3', u'e2']"], ['m5', "[u'e5', u'e3', u'e2']"], ['m6', "[u'e6', u'e7']"],
              ['m7', "[u'e5', u'e7', u'e1', u'e2', u'e3']"], ['m8', "[u'e5', u'e3', u'e1', u'e2']"],
              ['m9', "[u'e6', u'e4', u'e3']"], ['m10', "[u'e3', u'e4']"], ['m11', "[u'e1', u'e3', u'e6']"],
              ['m12', "[u'e4', u'e5', u'e3']"], ['m13', "[u'e1', u'e7', u'e5']"],
              ['m14', "[u'e6', u'e4', u'e2', u'e7']"], ['m15', "[u'e2', u'e7', u'e5', u'e3']"],
              ['m16', "[u'e5', u'e7']"], ['m17', "[u'e5', u'e1']"], ['m18', "[u'e1', u'e5']"],
              ['m19', "[u'e4', u'e7', u'e2', u'e6', u'e3']"], ['m20', "[u'e1', u'e5', u'e2']"]]),
    ('Rafael dias',
     [['m1', "[u'e1']"], ['m2', "[u'e5']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e1']"], ['m6', "[u'e7']"],
      ['m7', '[]'], ['m8', "[u'e1']"], ['m9', "[u'e4']"], ['m10', "[u'e1', u'e5']"], ['m11', "[u'e4', u'e3']"],
      ['m12', "[u'e5', u'e3']"], ['m13', "[u'e5']"], ['m14', "[u'e7', u'e6', u'e4']"], ['m15', "[u'e5', u'e3']"],
      ['m16', "[u'e5']"], ['m17', "[u'e2']"], ['m18', "[u'e1', u'e2']"], ['m19', "[u'e4']"],
      ['m20', "[u'e1', u'e5']"]]),
    ('M Clara Nevoa',
     [['m1', "[u'e1', u'e5']"], ['m2', "[u'e5']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e1']"],
      ['m6', "[u'e7']"], ['m7', "[u'e5', u'e1']"], ['m8', "[u'e1']"], ['m9', "[u'e4']"], ['m10', "[u'e7', u'e4']"],
      ['m11', "[u'e3', u'e4']"], ['m12', "[u'e5', u'e3']"], ['m13', "[u'e5']"], ['m14', "[u'e4', u'e7', u'e6']"],
      ['m15', "[u'e3', u'e5']"], ['m16', "[u'e3', u'e5']"], ['m17', "[u'e2']"], ['m18', "[u'e1', u'e2']"],
      ['m19', "[u'e4', u'e7']"], ['m20', "[u'e1', u'e5', u'e2']"]]),
    ('Filipe', [['m1', "[u'e5']"], ['m2', "[u'e5']"], ['m3', "[u'e5', u'e1']"], ['m4', "[u'e5']"], ['m5', "[u'e3']"],
                ['m6', "[u'e7']"], ['m7', "[u'e1']"], ['m8', "[u'e1', u'e3']"], ['m9', "[u'e4', u'e6']"],
                ['m10', "[u'e6']"], ['m11', "[u'e6', u'e4']"], ['m12', "[u'e1', u'e5']"], ['m13', "[u'e5']"],
                ['m14', "[u'e4', u'e7', u'e6']"], ['m15', "[u'e7', u'e5']"], ['m16', "[u'e3', u'e1']"],
                ['m17', "[u'e3']"], ['m18', "[u'e5', u'e3']"], ['m19', "[u'e7', u'e6']"], ['m20', "[u'e1']"]]),
    ('Isabela Helena',
     [['m1', "[u'e1']"], ['m2', "[u'e1']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e5']"], ['m6', "[u'e6']"],
      ['m7', "[u'e1']"], ['m8', "[u'e1']"], ['m9', "[u'e3', u'e4']"], ['m10', "[u'e3']"], ['m11', "[u'e6']"],
      ['m12', "[u'e1']"], ['m13', "[u'e5']"], ['m14', "[u'e7', u'e6']"], ['m15', "[u'e7', u'e6']"], ['m16', "[u'e3']"],
      ['m17', "[u'e5']"], ['m18', "[u'e2']"], ['m19', "[u'e5', u'e4']"], ['m20', "[u'e5', u'e1']"]]),
    ('Yan Romano',
     [['m1', "[u'e1', u'e2']"], ['m2', "[u'e2']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e3']"],
      ['m6', "[u'e7']"], ['m7', "[u'e1']"], ['m8', "[u'e1']"], ['m9', "[u'e4', u'e6', u'e3']"],
      ['m10', "[u'e3', u'e4']"], ['m11', "[u'e6', u'e3']"], ['m12', "[u'e1', u'e2']"], ['m13', "[u'e3']"],
      ['m14', "[u'e3', u'e4', u'e7']"], ['m15', "[u'e3']"], ['m16', '[]'], ['m17', "[u'e3']"], ['m18', "[u'e3']"],
      ['m19', "[u'e6', u'e4', u'e7']"], ['m20', "[u'e1', u'e2']"]]),
    ('Filippo', [['m1', "[u'e1', u'e2']"], ['m2', "[u'e1', u'e2']"], ['m3', "[u'e3', u'e2', u'e1']"],
                 ['m4', "[u'e3', u'e1', u'e2']"], ['m5', "[u'e2', u'e3']"],
                 ['m6', "[u'e4', u'e2', u'e7', u'e6', u'e1']"], ['m7', "[u'e1']"], ['m8', "[u'e1', u'e2']"],
                 ['m9', "[u'e6', u'e7']"], ['m10', "[u'e7', u'e4']"], ['m11', "[u'e4', u'e2', u'e7', u'e6']"],
                 ['m12', "[u'e5']"], ['m13', "[u'e5', u'e3']"], ['m14', "[u'e7', u'e4']"], ['m15', "[u'e4', u'e7']"],
                 ['m16', "[u'e5']"], ['m17', "[u'e2']"], ['m18', "[u'e2']"], ['m19', "[u'e4', u'e6', u'e7', u'e3']"],
                 ['m20', "[u'e1']"]]),
    ('Luan Campos',
     [['m1', "[u'e1', u'e5']"], ['m2', "[u'e5']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e3']"],
      ['m6', "[u'e6', u'e7']"], ['m7', "[u'e1']"], ['m8', "[u'e1']"], ['m9', "[u'e4']"], ['m10', "[u'e7']"],
      ['m11', "[u'e4']"], ['m12', "[u'e1']"], ['m13', "[u'e1']"], ['m14', "[u'e4']"], ['m15', "[u'e7']"],
      ['m16', "[u'e3']"], ['m17', "[u'e5']"], ['m18', "[u'e5', u'e3']"], ['m19', "[u'e7', u'e4']"],
      ['m20', "[u'e1']"]]),
    ('Bruna', [['m1', "[u'e1', u'e5']"], ['m2', "[u'e5']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e1']"],
               ['m6', "[u'e7', u'e5']"], ['m7', "[u'e1', u'e5']"], ['m8', "[u'e1']"], ['m9', "[u'e4']"],
               ['m10', "[u'e4', u'e7']"], ['m11', "[u'e3', u'e4']"], ['m12', "[u'e3']"], ['m13', "[u'e5']"],
               ['m14', "[u'e4', u'e6', u'e7']"], ['m15', "[u'e5', u'e3']"], ['m16', "[u'e5', u'e3']"],
               ['m17', "[u'e2']"], ['m18', "[u'e1', u'e2']"], ['m19', "[u'e4', u'e7', u'e5']"],
               ['m20', "[u'e1', u'e2']"]]),
    ('testando', [['m1', "[u'e1', u'e5']"], ['m2', "[u'e3']"], ['m3', "[u'e5', u'e6']"], ['m4', "[u'e7', u'e4']"],
                  ['m5', "[u'e4']"], ['m6', "[u'e3']"], ['m7', "[u'e5']"], ['m8', "[u'e6', u'e4']"], ['m9', "[u'e1']"],
                  ['m10', "[u'e3']"], ['m11', "[u'e5']"], ['m12', "[u'e6']"], ['m13', "[u'e1']"],
                  ['m14', "[u'e2', u'e3', u'e4']"], ['m15', "[u'e5', u'e4', u'e2']"], ['m16', "[u'e7']"],
                  ['m17', "[u'e1']"], ['m18', "[u'e2']"], ['m19', "[u'e4']"], ['m20', "[u'e6']"]]),
    ('Luiz Felipe', [['m1', "[u'e1']"], ['m2', "[u'e1']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e1']"],
                     ['m6', "[u'e3', u'e7']"], ['m7', "[u'e1', u'e3']"], ['m8', "[u'e1']"], ['m9', "[u'e4']"],
                     ['m10', "[u'e4']"], ['m11', "[u'e4']"], ['m12', "[u'e1']"], ['m13', "[u'e1']"], ['m14', "[u'e4']"],
                     ['m15', "[u'e4']"], ['m16', "[u'e5']"], ['m17', "[u'e1']"], ['m18', "[u'e1']"], ['m19', "[u'e3']"],
                     ['m20', "[u'e1']"]]),
    ('Ana Luisa Genn', [['m1', "[u'e1']"], ['m2', "[u'e1']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e5']"],
                        ['m6', "[u'e6', u'e7']"], ['m7', "[u'e1']"], ['m8', "[u'e1']"], ['m9', "[u'e3', u'e4']"],
                        ['m10', "[u'e3']"], ['m11', "[u'e6', u'e7']"], ['m12', "[u'e1']"], ['m13', "[u'e5']"],
                        ['m14', "[u'e6', u'e7']"], ['m15', "[u'e4', u'e7']"], ['m16', "[u'e3']"], ['m17', "[u'e5']"],
                        ['m18', "[u'e2']"], ['m19', "[u'e4', u'e5']"], ['m20', "[u'e1', u'e5']"]]),
    ('Henrique', [['m1', "[u'e5', u'e4', u'e3']"], ['m2', "[u'e5']"], ['m3', "[u'e2', u'e3']"], ['m4', "[u'e5']"],
                  ['m5', "[u'e2', u'e3']"], ['m6', "[u'e3', u'e4']"], ['m7', "[u'e1']"], ['m8', "[u'e1', u'e2']"],
                  ['m9', "[u'e4']"], ['m10', "[u'e4', u'e3']"], ['m11', "[u'e3']"], ['m12', "[u'e1', u'e3']"],
                  ['m13', "[u'e5']"], ['m14', "[u'e2', u'e7', u'e6', u'e4']"], ['m15', "[u'e6']"], ['m16', "[u'e3']"],
                  ['m17', "[u'e2', u'e3']"], ['m18', "[u'e6', u'e7']"], ['m19', "[u'e4']"], ['m20', "[u'e1', u'e2']"]]),
    ('Raphaella',
     [['m1', "[u'e1']"], ['m2', "[u'e5']"], ['m3', "[u'e3']"], ['m4', "[u'e1']"], ['m5', "[u'e1']"], ['m6', "[u'e3']"],
      ['m7', "[u'e1']"], ['m8', "[u'e1']"], ['m9', "[u'e6', u'e7']"], ['m10', "[u'e6', u'e7']"],
      ['m11', "[u'e4', u'e3']"], ['m12', "[u'e4', u'e3']"], ['m13', "[u'e2', u'e5']"], ['m14', "[u'e7', u'e6']"],
      ['m15', "[u'e1', u'e7']"], ['m16', "[u'e3']"], ['m17', "[u'e1', u'e5', u'e6']"], ['m18', "[u'e1', u'e3']"],
      ['m19', "[u'e7', u'e6', u'e4', u'e1']"], ['m20', "[u'e1', u'e5']"]]),
    ('Carolina', [['m1', "[u'e1', u'e5']"], ['m2', "[u'e5']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e1']"],
                  ['m6', "[u'e7']"], ['m7', "[u'e1']"], ['m8', "[u'e6']"], ['m9', "[u'e4']"], ['m10', "[u'e7', u'e3']"],
                  ['m11', "[u'e4']"], ['m12', "[u'e3']"], ['m13', "[u'e5', u'e2']"], ['m14', "[u'e4', u'e7', u'e6']"],
                  ['m15', "[u'e4', u'e2']"], ['m16', "[u'e3']"], ['m17', "[u'e5']"], ['m18', "[u'e3']"],
                  ['m19', "[u'e3']"], ['m20', "[u'e1', u'e2']"]]),
    ('Arthur Bueno', [['m1', "[u'e1']"], ['m2', "[u'e5']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e1']"],
                      ['m6', "[u'e5', u'e7']"], ['m7', "[u'e1']"], ['m8', "[u'e1']"], ['m9', "[u'e4']"],
                      ['m10', "[u'e4']"], ['m11', "[u'e4']"], ['m12', "[u'e3']"], ['m13', "[u'e1']"],
                      ['m14', "[u'e4', u'e7']"], ['m15', "[u'e7']"],
                      ['m16', "[u'e1', u'e4', u'e7', u'e6', u'e3', u'e5', u'e2']"], ['m17', "[u'e5']"],
                      ['m18', "[u'e3']"], ['m19', "[u'e6', u'e7']"], ['m20', "[u'e1', u'e5']"]]),
    ('Yan', [['m1', "[u'e5']"], ['m2', "[u'e5']"], ['m3', "[u'e1']"], ['m4', "[u'e5']"], ['m5', "[u'e3']"],
             ['m6', "[u'e4', u'e7']"], ['m7', "[u'e1']"], ['m8', "[u'e1', u'e3']"], ['m9', "[u'e4', u'e3', u'e7']"],
             ['m10', "[u'e7']"], ['m11', "[u'e7', u'e6', u'e4']"], ['m12', "[u'e1', u'e5']"], ['m13', "[u'e5']"],
             ['m14', "[u'e7', u'e6', u'e4']"], ['m15', '[]'], ['m16', "[u'e1']"], ['m17', "[u'e5', u'e3']"],
             ['m18', "[u'e5', u'e3']"], ['m19', "[u'e5', u'e7']"], ['m20', "[u'e1']"]]),
    ('Carlos Eduardo', [['m1', "[u'e1', u'e2', u'e3', u'e5']"], ['m2', "[u'e1', u'e2', u'e3', u'e5', u'e6']"],
                        ['m3', "[u'e1', u'e2', u'e3', u'e5', u'e4']"], ['m4', "[u'e1', u'e2', u'e3', u'e5']"],
                        ['m5', "[u'e3', u'e4']"], ['m6', "[u'e1', u'e2', u'e3', u'e5', u'e6', u'e7']"],
                        ['m7', "[u'e1', u'e2', u'e5']"], ['m8', "[u'e1', u'e4', u'e5', u'e6']"],
                        ['m9', "[u'e3', u'e4', u'e7']"], ['m10', "[u'e3', u'e4', u'e7']"],
                        ['m11', "[u'e3', u'e4', u'e6']"], ['m12', "[u'e1', u'e3', u'e5']"],
                        ['m13', "[u'e1', u'e2', u'e3']"], ['m14', "[u'e1', u'e3', u'e7']"],
                        ['m15', "[u'e3', u'e4', u'e7']"], ['m16', "[u'e3', u'e4', u'e5', u'e6', u'e7']"],
                        ['m17', "[u'e4', u'e2', u'e3', u'e5', u'e1', u'e7']"], ['m18', "[u'e1', u'e3', u'e4', u'e5']"],
                        ['m19', "[u'e3', u'e4', u'e7']"], ['m20', "[u'e1', u'e2', u'e3', u'e5']"]]),
    ('Maria Clara', [['m1', "[u'e1', u'e3']"], ['m2', "[u'e5', u'e2']"], ['m3', "[u'e3']"], ['m4', "[u'e5', u'e3']"],
                     ['m5', "[u'e1']"], ['m6', "[u'e4', u'e7']"], ['m7', "[u'e1']"], ['m8', "[u'e1', u'e6']"],
                     ['m9', "[u'e7', u'e6']"], ['m10', "[u'e6', u'e7']"], ['m11', "[u'e4', u'e3']"],
                     ['m12', "[u'e4', u'e3']"], ['m13', "[u'e2', u'e5']"], ['m14', "[u'e7', u'e2']"],
                     ['m15', "[u'e1', u'e7', u'e4', u'e6']"], ['m16', "[u'e5', u'e3']"], ['m17', "[u'e1', u'e6']"],
                     ['m18', "[u'e1']"], ['m19', "[u'e4', u'e7', u'e6', u'e1']"], ['m20', "[u'e1', u'e2']"]]),
    ('Victor Bazin',
     [['m1', "[u'e1']"], ['m2', "[u'e1']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e5']"], ['m6', "[u'e7']"],
      ['m7', "[u'e1']"], ['m8', "[u'e5']"], ['m9', "[u'e3']"], ['m10', "[u'e3']"], ['m11', "[u'e6']"],
      ['m12', "[u'e6']"], ['m13', "[u'e2']"], ['m14', "[u'e4']"], ['m15', "[u'e7']"], ['m16', "[u'e3']"],
      ['m17', "[u'e5']"], ['m18', "[u'e3']"], ['m19', "[u'e6']"], ['m20', "[u'e5']"]]),
    ('Rhyelle',
     [['m1', "[u'e1']"], ['m2', "[u'e1']"], ['m3', "[u'e3']"], ['m4', "[u'e5']"], ['m5', "[u'e5']"], ['m6', "[u'e6']"],
      ['m7', "[u'e1']"], ['m8', "[u'e1']"], ['m9', "[u'e4']"], ['m10', "[u'e3']"], ['m11', "[u'e7', u'e6']"],
      ['m12', "[u'e1']"], ['m13', "[u'e5']"], ['m14', "[u'e4']"], ['m15', "[u'e3']"], ['m16', "[u'e3']"],
      ['m17', "[u'e5']"], ['m18', "[u'e2', u'e3']"], ['m19', "[u'e4', u'e5']"], ['m20', "[u'e1', u'e2', u'e3']"]])
])


class Grapher(Ubigraph):  # (Graph):
    def _put_edge(self, col, edge, idea):
        if edge in col:
            col[edge].append(idea)
        else:
            col[edge] = [idea]

    def contains(self, node):
        return node in self.__nodes

    def _hidden_add_nodes_from(self, spikes):
        for ind, node_ends in enumerate(spikes):
            node, ends = node_ends
            if not self.contains(node):
                print('.', end=' ')
                self.__nodes[node] = dict(
                    index=ind, vertex=node, size=len(ends), edges=set())

    def legend(self, node):
        return LEGEND[int(node[1:]) - 1]

    def _add_nodes_from(self, spikes):
        NFACTOR = 20.0
        # NFACTOR = 70.0
        for ind, node_ends in enumerate(spikes):
            node, ends = node_ends
            if node in self.__nodes:
                size = self.__nodes[node]['size']
                vertex = self.__nodes[node]['vertex']
                size = self.__nodes[node]['size'] = size + len(ends) / NFACTOR
                vertex.set(size=size)

            else:
                v = self.newVertex(shape="sphere", fontsize="24",
                                   fontcolor="#00ffff", color="#ffff00",
                                   label=self.legend(node),
                                   size=len(ends) / NFACTOR)
                self.__nodes[node] = dict(index=ind, vertex=v,
                                          size=len(ends) / NFACTOR,
                                          pos=(randint(-100, 100), randint(-100, 100)))

    def _add_edge(self, edge, weight):
        FUW = 14600.0;
        FW = 11
        FUW = 8000.0;
        FW = 4
        # FUW = 8600.0; FW =12
        fro, to = edge
        fro, to = self.__nodes[fro]['vertex'], self.__nodes[to]['vertex']
        self.newEdge(fro, to, spline=False, strength=weight / FUW,
                     width=weight / 1.5,
                     showstrain=True, visible=weight > FW)

    def _hidden_add_edge(self, edge, weight):
        fro, to = edge
        fro, to = self.__nodes[fro]['vertex'], self.__nodes[to]['vertex']
        # print (fro, to),
        edge_label = min((fro, to), (to, fro))
        self.__edges[edge_label] = dict(fro=edge_label[0], to=edge_label[1], weight=weight)

    def _assemble_edges(self, spikes):
        edges = []
        themearrays = {}
        for themepair in spikes:
            theme, ideas = themepair
            exec('ideas = %s' % ideas)  # <<<<<<<<<<<<<<<<remove
            assert len(ideas) == 0 or (len(ideas[0]) == 2), 'ideas th, %s %s' % (theme, ideas)
            for idea in ideas:
                if idea in themearrays:
                    themearrays[idea].add(theme)
                else:
                    themearrays[idea] = set([theme])
        theme_edges = {}
        for idea, themes in list(themearrays.items()):
            [self._put_edge(theme_edges, (theme, othertheme), idea)
             for theme in themes
             for othertheme in themes
             if (othertheme != theme) and
             not ((othertheme, theme) in theme_edges)]
        # print themearrays
        return theme_edges  # themearrays

    def main_shown(self, db):
        # draw_random(self)
        # plt.show()
        self._nodes = self.__nodes = {}
        self.__graph = {}
        self.clear()
        # self.single_graph()
        self.multiple_graph(db)

    def statistics(self, learnerset):
        alunomprox = min(learnerset)
        mediaaluno = sum(learnerset) / len(learnerset)
        alunomdist = max(learnerset)
        var = sum(
            (aluno - mediaaluno) ** 2 for aluno in learnerset
        ) ** 0.5 / len(learnerset)
        print("alunomprox: %s, mediaaluno: %s, alunomdist: %s, var: %s" % (
            alunomprox, mediaaluno, alunomdist, var
        ))

    def multiple_graph(self, db):
        self.__DB = db
        for graph in list(db.values()):
            theme_edges = self._assemble_edges(graph)
            self._add_nodes_from(graph)
            [self._add_edge(edge, weight=len(idea))
             for edge, idea in list(theme_edges.items())]
        # print theme_edges
        # self.add_nodes_from('m%d'%i for i in range(1,21))

    def single_graph(self, user, base):
        theme_edges = self._assemble_edges(base[user])
        # print theme_edges
        self._add_nodes_from(DB[user])
        [self._add_edge(edge, weight=len(idea))
         for edge, idea in list(theme_edges.items())]

    def hidden(self):
        self.__nodes = {}
        self.__edges = {}
        self.__nodes = {}

    def _hidden_graph(self, base):
        for graph in list(base.values()):
            theme_edges = self._assemble_edges(graph)
            self._hidden_add_nodes_from(graph)
            [self._hidden_add_edge(edge, weight=len(idea))
             for edge, idea in list(theme_edges.items())]
        self._attach_edges_to_nodes()
        print(theme_edges)
        return

    def single_hidden_graph(self, user, base):
        self.hidden()
        theme_edges = self._assemble_edges(base[user])
        # print theme_edges
        self._hidden_add_nodes_from(base[user])
        [self._hidden_add_edge(edge, weight=len(idea))
         for edge, idea in list(theme_edges.items())]
        self._attach_edges_to_nodes()
        return self

    def _attach_edges_to_nodes(self):
        for node in self.__nodes: print('ANODE', node)  # node['edges'] = set()
        attach_edges = lambda node, edge, s=self: s.__nodes[node]['edges'].add(edge)
        for edge in self.__edges:
            attach_edges(edge[0], edge)
            attach_edges(edge[1], edge)

    def _hidden_graph_diff(self, learner):
        x = sum(1 for edge in list(self.__edges.keys())
                if not (edge in list(learner.__edges.keys())))
        y = sum(1 for edge in list(learner.__edges.keys())
                if not (edge in list(self.__edges.keys())))
        print(len(list(learner.__edges.keys())), len(list(self.__edges.keys())), x, y, x + y)
        print('\n')
        return x + y

    def _hidden_graph_distance(self, learner):
        distance = lambda edge: abs(
            self.__edges[edge]['weight'] - learner.__edges[edge]['weight'])
        x = [distance(edge) for edge in list(self.__edges.keys())
             if (edge in list(learner.__edges.keys()))]
        return x

    def _hidden_nodes_distance(self, learner):
        assert isinstance(learner.__nodes, dict)
        assert isinstance(list(learner.__nodes.values())[0]['edges'], set)
        distance = lambda edge: abs(
            self.__edges[edge]['weight'] - learner.__edges[edge]['weight'])
        # nodes = [sum(distance(edge) for edge in node['edges'])
        #         for node in learner.__nodes.values()]
        nodes = [sum(distance(edge) for edge in learner.__nodes[node]['edges'])
                 for node in self.clustered]
        return nodes

    def map_learner_spread(self, spread):
        def prune(cluster, togo):
            if cluster.branches:
                if togo < 0:
                    cluster.branches = None
                else:
                    for branch in cluster.branches:
                        prune(branch, togo - cluster.height)

        def listOfClusters0(cluster, alist):
            if not cluster.branches:
                alist.append(list(cluster))
            else:
                for branch in cluster.branches:
                    listOfClusters0(branch, alist)

        def listOfClusters(root):
            l = []
            listOfClusters0(root, l)
            return l

        tree = [[0, 3, 11, 22, 1, 12], [2, 24, 16, 4, 6, 17, 20, 9],
                [7, 23, 13, 18, 10, 8, 14, 21], [5, 19, 15]]
        learner_strength = lambda o: sum(index
                                         for index, nodes in enumerate(tree) if o in nodes)
        # learner_strength = lambda o: sum(node
        #                    for node in o)
        # map = [[learner,learner_strength(nodes)//40]+[node
        #        for node in nodes] for learner, nodes in enumerate(spread)]
        map = [[learner, learner_strength(learner)] + [20 - node
                                                       for node in nodes] for learner, nodes in enumerate(spread)]
        self.learner_spread_map = [['nam', 'siz'] + [node for node in self.__nodes],
                                   ['s', 'd'] + ['c'] * (0 + len(self.__nodes)),
                                   ['i', 'c'] + [' '] * (0 + len(self.__nodes))] + map
        f = file('learnermatrix.tab', 'w')
        w = writer(f, delimiter='\t')
        for line in self.learner_spread_map: w.writerow(line)
        f.close()
        data = orange.ExampleTable("learnermatrix")
        import orngClustering
        root = orngClustering.hierarchicalClustering(data, order=False)
        classifier = orange.BayesLearner(data)
        prune(root, 1.9)
        # for n, cluster in enumerate(listOfClusters(root)):
        #    print "\n\n Cluster %i \n" % n
        #    for instance in cluster:
        #        print instance
        for i in range(25):
            c = classifier(data[i])
            print("original", data[i].getclass(), "classified as", c)

    def map_node_gravity(self):
        edge_strength = lambda n, o: sum(self.__edges[e]['weight']
                                         for e in self.__nodes[o]['edges']
                                         if n != o and n in e)
        node_strength = lambda o: sum(self.__edges[e]['weight']
                                      for e in self.__nodes[o]['edges'])
        f = file('nodematrix.tab', 'w')
        w = writer(f, delimiter='\t')
        map = [[node, node_strength(node) // 10] + [edge_strength(node, othernode)
                                                    for othernode in self.__nodes] for node in self.__nodes]
        map = [['nam', 'siz'] + [node for node in self.__nodes],
               ['s', 'd'] + ['c'] * (0 + len(self.__nodes)),
               [' ', 'c'] + [' '] * (0 + len(self.__nodes))] + map
        for line in map: w.writerow(line)
        f.close()
        clustered = "11,6,15,10,19,9,18,5,16,7,1,3,2,12,17,8,13,20,4,14"
        self.CLEGEND = [LEGEND[int(theme) - 1] for theme in clustered.split(',')]
        self.clustered = ['m' + i for i in clustered.split(',')]

    def main_hidden(self, db):
        self.hidden()
        # self.single_graph()
        # self.single_graph()
        self._hidden_graph(db)
        print('NODES:', self.__nodes)

    def stats_hidden(self, db=DB):
        self._learner_graphs = [Grapher().single_hidden_graph(learner, db)
                                for learner in db]
        assert isinstance(self._learner_graphs[0].__nodes, dict)
        self._learner_diff = [self._hidden_graph_diff(learner)
                              for learner in self._learner_graphs]
        self.statistics(self._learner_diff)
        self._learner_spread = [self._hidden_graph_distance(learner)
                                for learner in self._learner_graphs]
        self._learner_distance = [sum(self._hidden_graph_distance(learner))
                                  for learner in self._learner_graphs]
        self.statistics(self._learner_distance)
        self._learner_node_spread = [self._hidden_nodes_distance(learner)
                                     for learner in self._learner_graphs]
        self.map_learner_spread(self._learner_node_spread)
        self._learner_node_distance = [sum(self._hidden_nodes_distance(learner))
                                       for learner in self._learner_graphs]
        print(self._learner_node_distance)
        self.statistics(self._learner_node_distance)

    def plot_hidden(self, db=DB):
        figure(1)
        boxplot(self._learner_spread)
        figure(2)
        for learner in self._learner_spread:
            plot(learner)
        figure(3)
        boxplot(self._learner_node_spread)
        fg = figure(4)
        theta = radar_factory(20)
        for learner_class in range(4):
            for learner in self.learner_spread_map:
                if learner[1] == learner_class:
                    thetas = len(learner) - 2
                    ax = fg.add_subplot(2, 2, learner_class + 1, projection='radar')
                    ax.set_varlabels(self.CLEGEND)
                    r = np.arange(0, 2 * np.pi, 2 * np.pi / thetas)
                    # print 'THETAS ',thetas, len(r)

                    ax.plot(r, learner[2:])
        show()

    def main(self, db=PDB):
        db = dict((user, [(theme, eval(spike)) for theme, spike in content if len(spike) > 4])
                  for user, content in list(db.items()))
        for id in db['Jailson'][1][1]: print(id)
        # return
        # self.main_shown(db)
        self.main_hidden(db)
        self.map_node_gravity()
        self.stats_hidden()
        # self.plot_hidden()
        # print 'EDGES', self.__edges


if __name__ == "__main__":
    Grapher().main()
