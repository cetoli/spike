#! /usr/bin/env python
# -*- coding: UTF8 -*-
"""
############################################################
Avaliando a educação matemática em grafo
############################################################

:Author: *Carlo E. T. Oliveira*
:Contact: carlo@nce.ufrj.br
:Date: $Date: 2011/09/09  $
:Status: This is a "work in progress"
:Revision: $Revision: 0.1 $
:Home: `Labase <http://labase.nce.ufrj.br/>`__
:Copyright: ©2011, `GPL <http://is.gd/3Udt>`__.
REVISED FOR PYTHON3.8: WORKING!
"""
__author__ = "Carlo E. T. Oliveira (cetoli@yahoo.com.br) $Author: cetoli $"
__version__ = "0.1 $Revision$"[10:-1]
__date__ = "2011/09/09 $Date$"

# import matplotlib.pyplot as plt
# from visual import sphere
# from ubigraph import Ubigraph
# from typing import List

from networkx import Graph  # , draw,draw_circular,draw_random,draw_spectral
from random import randint
from matplotlib.pyplot import boxplot, show, figure, plot  # , polar
from csv import writer
# import orange
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.projections.polar import PolarAxes
from matplotlib.projections import register_projection


def radar_factory(num_vars, frame='circle'):
    """Create a radar chart with `num_vars` axes."""
    # calculate evenly-spaced axis angles
    theta = 2 * np.pi * np.linspace(0, 1 - 1. / num_vars, num_vars)

    # rotate theta such that the first axis is at the top
    # theta += np.pi/2

    def draw_poly_frame(_, x0, y0, r):
        # TODO: use transforms to convert (x, y) to (r, theta)
        verts = [(r * np.cos(t) + x0, r * np.sin(t) + y0) for t in theta]
        return plt.Polygon(verts, closed=True, edgecolor='k')

    def draw_circle_frame(_, x0, y0, r):
        return plt.Circle((x0, y0), r)

    frame_dict = {'polygon': draw_poly_frame, 'circle': draw_circle_frame}
    if frame not in frame_dict:
        raise ValueError('unknown value for `frame`: %s' % frame)

    class RadarAxes(PolarAxes):
        """Class for creating a radar chart (a.k.a. a spider or star chart)

        http://en.wikipedia.org/wiki/Radar_chart
        """
        name = 'radar'
        # use 1 line segment to connect specified points
        RESOLUTION = 1
        # define draw_frame method
        draw_frame = frame_dict[frame]

        def fill(self, *args, **kwargs):
            """Override fill so that line is closed by default"""
            closed = kwargs.pop('closed', True)
            return super(RadarAxes, self).fill(closed=closed, *args, **kwargs)

        def plot(self, *args, **kwargs):
            """Override plot so that line is closed by default"""
            lines = super(RadarAxes, self).plot(*args, **kwargs)
            for line in lines:
                self._close_line(line)

        def _close_line(self, line):
            _ = self
            x, y = line.get_data()
            # FIXME: markers at x[0], y[0] get doubled-up
            if x[0] != x[-1]:
                x = np.concatenate((x, [x[0]]))
                y = np.concatenate((y, [y[0]]))
                line.set_data(x, y)

        def set_varlabels(self, labels):
            self.set_thetagrids(theta * 180 / np.pi, labels)

        def _gen_axes_patch(self):
            x0, y0 = (0.5, 0.5)
            r = 0.5
            return self.draw_frame(x0, y0, r)

    register_projection(RadarAxes)
    return theta


LEGEND = 'NNO,SND,NRO,SBI,NPR,UMC,PDC,CME,FBT,FPL,OEF,RDP,MFC,PEA,NDS,TDI,NAT,FND,GEO,TAB'.split(',')

PDB = {
    "Margareth":
        [['m1', ['e1', 'e2', 'e3', 'e5']], ['m2', ['e1', 'e2', 'e5', 'e3']], ['m3', ['e1', 'e3', 'e5']],
         ['m4', ['e5', 'e1', 'e2', 'e3']], ['m5', ['e1', 'e3']], ['m6', ['e7', 'e1', 'e2', 'e3', 'e6']],
         ['m7', ['e1', 'e2', 'e5', 'e3']], ['m8', ['e1', 'e2', 'e3']], ['m9', ['e3', 'e4', 'e6', 'e7']],
         ['m10', ['e3', 'e4', 'e6', 'e7']], ['m11', ['e3', 'e4', 'e6']], ['m12', ['e1', 'e2', 'e5', 'e3']],
         ['m13', ['e1', 'e2', 'e3']], ['m14', ['e1', 'e7', 'e3', 'e4', 'e6']], ['m15', ['e3', 'e4', 'e6', 'e7']],
         ['m16', ['e1', 'e3', 'e4', 'e6']], ['m17', ['e1', 'e2', 'e3']], ['m18', ['e1', 'e3', 'e2']],
         ['m19', ['e3', 'e4', 'e6']], ['m20', ['e1', 'e2', 'e3']]],
    "Fabiana Castro Franco":
        [['m1', ['e3', 'e1', 'e2']], ['m2', ['e3', 'e1']], ['m3', ['e3']], ['m4', ['e5', 'e3']], ['m5', ['e3']],
         ['m6', ['e7', 'e3']], ['m7', ['e1', 'e2', 'e3']], ['m8', ['e3', 'e1']], ['m9', ['e4', 'e3']],
         ['m10', ['e3', 'e4']], ['m11', ['e4', 'e3']], ['m12', ['e3', 'e2', 'e1']], ['m13', ['e3', 'e1']],
         ['m14', ['e3', 'e7']], ['m15', ['e3']], ['m16', ['e3', 'e4']], ['m17', ['e1', 'e3']],
         ['m18', ['e3', 'e1', 'e2']], ['m19', ['e3']], ['m20', ['e1', 'e3', 'e2']]],
    "Thiago Dal'Bello":
        [['m1', ['e1', 'e3']], ['m2', ['e3', 'e1']], ['m3', ['e1', 'e3']], ['m4', ['e1', 'e3']],
         ['m5', ['e3', 'e1']], ['m6', ['e2', 'e4', 'e7']], ['m7', ['e1']], ['m8', ['e1']], ['m9', ['e3']],
         ['m10', ['e1', 'e3']], ['m11', ['e3', 'e2', 'e4', 'e7']], ['m12', ['e5', 'e1']], ['m13', ['e1']],
         ['m14', ['e7', 'e2', 'e4']], ['m15', ['e1', 'e7']], ['m16', ['e1', 'e2', 'e4']], ['m17', ['e3', 'e1']],
         ['m18', ['e3']], ['m19', ['e4', 'e7', 'e2']], ['m20', ['e1', 'e2']]],
    "Jailson":
        [['m1', ['e1', 'e2', 'e5', 'e7']], ['m2', ['e3', 'e4']], ['m3', ['e1', 'e3']], ['m4', ['e6']],
         ['m5', ['e1']], ['m6', ['e4', 'e7']], ['m7', ['e3']], ['m8', ['e1', 'e3']], ['m9', ['e4']],
         ['m10', ['e3', 'e4']], ['m11', ['e4', 'e3']], ['m12', ['e2', 'e5']], ['m13', ['e2']], ['m14', ['e7', 'e4']],
         ['m15', ['e1']], ['m16', ['e3']], ['m17', ['e1', 'e2']], ['m18', ['e3', 'e4']], ['m19', ['e7', 'e4']],
         ['m20', ['e1']]],
    "Fabio quintanilha":
        [['m1', ['e1', 'e2', 'e3']], ['m2', ['e1', 'e2', 'e3', 'e5']], ['m3', ['e1', 'e2', 'e3', 'e5']],
         ['m4', ['e1', 'e5', 'e3']], ['m5', ['e1', 'e3', 'e2']], ['m6', ['e7', 'e5', 'e3']],
         ['m7', ['e1', 'e2', 'e3']], ['m8', ['e1', 'e6', 'e2']], ['m9', ['e4', 'e7', 'e6']],
         ['m10', ['e7', 'e4', 'e6', 'e3']], ['m11', ['e4', 'e7', 'e6']], ['m12', ['e3', 'e5', 'e2', 'e1']],
         ['m13', ['e1', 'e2', 'e3']], ['m14', ['e4', 'e7', 'e1', 'e2']], ['m15', ['e4', 'e7']],
         ['m16', ['e1', 'e2', 'e3', 'e6']], ['m17', ['e1', 'e2', 'e5']], ['m18', ['e3', 'e2', 'e1', 'e6']],
         ['m19', ['e5', 'e4', 'e7', 'e3']], ['m20', ['e1', 'e2']]],
    "Jacqueline Silva":
        [['m1', ['e1', 'e2', 'e5']], ['m2', ['e3', 'e2', 'e1', 'e5']], ['m3', ['e3']],
         ['m4', ['e3', 'e5', 'e1', 'e2']], ['m5', ['e1', 'e3']], ['m6', ['e6', 'e3', 'e7']],
         ['m7', ['e3', 'e2', 'e1']], ['m8', ['e1', 'e2', 'e3']], ['m9', ['e3', 'e4']], ['m10', ['e3', 'e4']],
         ['m11', ['e4', 'e6', 'e3']], ['m12', ['e1', 'e2', 'e3', 'e4', 'e5', 'e6', 'e7']],
         ['m13', ['e1', 'e2', 'e3']], ['m14', ['e1', 'e3', 'e7']], ['m15', ['e3', 'e4']],
         ['m16', ['e3', 'e4', 'e2']], ['m17', ['e5', 'e1', 'e2', 'e3']], ['m18', ['e3', 'e2', 'e5']],
         ['m19', ['e3', 'e4']], ['m20', ['e2', 'e1', 'e5']]],
    "Elizabeth":
        [['m1', ['e1', 'e2', 'e3']], ['m2', ['e1', 'e2', 'e3']], ['m3', ['e1', 'e2', 'e3']],
         ['m4', ['e1', 'e2', 'e3']], ['m5', ['e1', 'e2', 'e3']], ['m6', ['e4', 'e1', 'e2', 'e3', 'e5', 'e7']],
         ['m7', ['e1', 'e2', 'e3']], ['m8', ['e4', 'e3', 'e1', 'e2', 'e6']], ['m9', ['e1', 'e5', 'e3', 'e4']],
         ['m10', ['e4', 'e7']], ['m11', ['e1', 'e3', 'e6', 'e7', 'e4']], ['m12', ['e5', 'e6', 'e3']],
         ['m13', ['e1', 'e3']], ['m14', ['e7', 'e2', 'e3', 'e4']], ['m15', ['e4', 'e6', 'e3']],
         ['m16', ['e5', 'e3']], ['m17', ['e1', 'e2', 'e3']], ['m18', ['e1', 'e2', 'e3']],
         ['m19', ['e4', 'e7', 'e1', 'e3']]],
    "Fabio":
        [['m1', ['e1', 'e3', 'e2', 'e5', 'e7']], ['m2', ['e1', 'e3', 'e2', 'e5']], ['m3', ['e3', 'e1', 'e2', 'e5']],
         ['m4', ['e1', 'e3', 'e2', 'e5']], ['m5', ['e1', 'e3', 'e5']], ['m6', ['e7', 'e2', 'e1', 'e6', 'e4']],
         ['m7', ['e1', 'e2', 'e3']], ['m8', ['e3', 'e2', 'e1']], ['m9', ['e3', 'e6', 'e4', 'e7']],
         ['m10', ['e3', 'e4', 'e7']], ['m11', ['e3', 'e6', 'e4']], ['m12', ['e4', 'e5', 'e3', 'e2', 'e1', 'e7']],
         ['m13', ['e1', 'e3', 'e5']], ['m14', ['e7', 'e6', 'e4', 'e1', 'e2']], ['m15', ['e4', 'e7', 'e3']],
         ['m16', ['e2', 'e3', 'e1', 'e6']], ['m17', ['e1', 'e5', 'e7', 'e3', 'e2']],
         ['m18', ['e3', 'e1', 'e2', 'e7', 'e5', 'e4']], ['m19', ['e7', 'e4', 'e3', 'e6']],
         ['m20', ['e7', 'e5', 'e3', 'e1', 'e2']]],
    "Adriano":
        [['m1', ['e1']], ['m2', ['e1']], ['m3', ['e1']], ['m4', ['e1']], ['m5', ['e3']], ['m6', ['e7']],
         ['m7', ['e1']], ['m8', ['e1']], ['m9', ['e4']], ['m10', ['e4']], ['m11', ['e4']], ['m12', ['e3']],
         ['m13', ['e1']], ['m14', ['e4']], ['m15', ['e5']], ['m16', ['e3']], ['m17', ['e1', 'e3']],
         ['m18', ['e3', 'e2']], ['m19', ['e4']], ['m20', ['e1']]],
    "Rodrigo Guedes":
        [['m1', ['e5']], ['m2', ['e3', 'e5']], ['m3', ['e3']], ['m4', ['e1']], ['m5', ['e1']], ['m6', ['e7']],
         ['m7', ['e1']], ['m8', ['e1']], ['m9', ['e4']], ['m10', ['e4']], ['m11', ['e3']], ['m12', ['e5']],
         ['m13', ['e2']], ['m14', ['e4']], ['m15', ['e4']], ['m16', ['e5']], ['m17', ['e1']], ['m18', ['e3']],
         ['m19', ['e6']], ['m20', ['e1']]],
    "Rosiney":
        [['m1', ['e1', 'e2', 'e3', 'e6']], ['m2', ['e1', 'e3', 'e6', 'e5', 'e7']], ['m3', ['e1', 'e2', 'e3', 'e5']],
         ['m4', ['e1', 'e3', 'e5']], ['m5', ['e1', 'e3', 'e2']], ['m6', ['e5', 'e7', 'e2']],
         ['m7', ['e1', 'e2', 'e7']], ['m8', ['e1', 'e2']], ['m9', ['e7', 'e4']], ['m10', ['e7', 'e4']],
         ['m11', ['e7']], ['m12', ['e5', 'e3']], ['m13', ['e7', 'e2']], ['m14', ['e4', 'e7', 'e2']],
         ['m15', ['e5', 'e7']], ['m16', ['e3', 'e5']], ['m17', ['e1', 'e3']], ['m18', ['e1', 'e2', 'e5']],
         ['m19', ['e4', 'e7']], ['m20', ['e1', 'e2']]],
}

DB = {
    "Larissa":
        [['m1', ['e1']], ['m2', ['e1']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e5']], ['m6', ['e7', 'e6']],
         ['m7', ['e1']], ['m8', ['e1']], ['m9', ['e3', 'e4']], ['m10', ['e3']], ['m11', ['e6', 'e7']],
         ['m12', ['e1']], ['m13', ['e5']], ['m14', ['e7', 'e6']], ['m15', ['e7', 'e6']], ['m16', ['e3']],
         ['m17', ['e5']], ['m18', ['e2']], ['m19', ['e4', 'e5']], ['m20', ['e5']]],
    "Iago":
        [['m1', ['e3', 'e1', 'e2']], ['m2', ['e1', 'e2', 'e3']], ['m3', ['e1', 'e2', 'e3']],
         ['m4', ['e3', 'e2', 'e1']], ['m5', ['e3', 'e2']], ['m6', ['e6', 'e7']], ['m7', ['e5', 'e2', 'e1']],
         ['m8', ['e1', 'e2']], ['m9', ['e3', 'e4', 'e7']], ['m10', ['e7', 'e6', 'e2', 'e3']], ['m11', ['e4', 'e7']],
         ['m12', ['e5']], ['m13', ['e1', 'e2']], ['m14', ['e6', 'e7', 'e2', 'e3']], ['m15', ['e2', 'e4']],
         ['m16', ['e1', 'e2', 'e3']], ['m17', ['e2', 'e1']], ['m18', ['e3', 'e2', 'e1']],
         ['m19', ['e7', 'e6', 'e4', 'e1', 'e2', 'e3']], ['m20', ['e1', 'e2']]],
    "Petinate":
        [['m1', ['e2', 'e3', 'e1', 'e5']], ['m2', ['e5', 'e7']], ['m3', ['e5', 'e2', 'e3']],
         ['m4', ['e5', 'e3', 'e2']], ['m5', ['e3', 'e5', 'e2']], ['m6', ['e7', 'e6']], ['m7', ['e1', 'e2', 'e3']],
         ['m8', ['e2', 'e1', 'e5', 'e3']], ['m9', ['e3', 'e4', 'e6']], ['m10', ['e3', 'e4']], ['m11', ['e1', 'e3']],
         ['m12', ['e5', 'e2']], ['m13', ['e5', 'e3']], ['m14', ['e6', 'e4', 'e7']], ['m15', ['e2', 'e5', 'e7', 'e3']],
         ['m16', ['e7', 'e1']], ['m17', ['e1', 'e7', 'e2']], ['m18', ['e5']], ['m19', ['e4', 'e6', 'e2', 'e7', 'e3']],
         ['m20', ['e1', 'e2']]],
    "Andre Mendez":
        [['m1', ['e1']], ['m2', ['e1']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e1']], ['m6', ['e7']],
         ['m7', ['e3']], ['m8', ['e2']], ['m9', ['e4']], ['m10', ['e6']], ['m11', ['e6']], ['m12', ['e1']],
         ['m13', ['e3']], ['m14', ['e6', 'e7']], ['m15', ['e7', 'e6']], ['m16', ['e3']], ['m17', ['e5']],
         ['m18', ['e3']], ['m19', ['e6']], ['m20', ['e1']]],
    "Igor":
        [['m1', ['e1', 'e2', 'e3', 'e5', 'e4']], ['m2', ['e7', 'e5']], ['m3', ['e5', 'e2', 'e3']],
         ['m4', ['e5', 'e3', 'e2']], ['m5', ['e5', 'e3', 'e2']], ['m6', ['e6', 'e7']],
         ['m7', ['e5', 'e7', 'e1', 'e2', 'e3']], ['m8', ['e5', 'e3', 'e1', 'e2']], ['m9', ['e6', 'e4', 'e3']],
         ['m10', ['e3', 'e4']], ['m11', ['e1', 'e3', 'e6']], ['m12', ['e4', 'e5', 'e3']], ['m13', ['e1', 'e7', 'e5']],
         ['m14', ['e6', 'e4', 'e2', 'e7']], ['m15', ['e2', 'e7', 'e5', 'e3']], ['m16', ['e5', 'e7']],
         ['m17', ['e5', 'e1']], ['m18', ['e1', 'e5']], ['m19', ['e4', 'e7', 'e2', 'e6', 'e3']],
         ['m20', ['e1', 'e5', 'e2']]],
    "Rafael dias":
        [['m1', ['e1']], ['m2', ['e5']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e1']], ['m6', ['e7']], ['m7', []],
         ['m8', ['e1']], ['m9', ['e4']], ['m10', ['e1', 'e5']], ['m11', ['e4', 'e3']], ['m12', ['e5', 'e3']],
         ['m13', ['e5']], ['m14', ['e7', 'e6', 'e4']], ['m15', ['e5', 'e3']], ['m16', ['e5']], ['m17', ['e2']],
         ['m18', ['e1', 'e2']], ['m19', ['e4']], ['m20', ['e1', 'e5']]],
    "M Clara Nevoa":
        [['m1', ['e1', 'e5']], ['m2', ['e5']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e1']], ['m6', ['e7']],
         ['m7', ['e5', 'e1']], ['m8', ['e1']], ['m9', ['e4']], ['m10', ['e7', 'e4']], ['m11', ['e3', 'e4']],
         ['m12', ['e5', 'e3']], ['m13', ['e5']], ['m14', ['e4', 'e7', 'e6']], ['m15', ['e3', 'e5']],
         ['m16', ['e3', 'e5']], ['m17', ['e2']], ['m18', ['e1', 'e2']], ['m19', ['e4', 'e7']],
         ['m20', ['e1', 'e5', 'e2']]],
    "Filipe":
        [['m1', ['e5']], ['m2', ['e5']], ['m3', ['e5', 'e1']], ['m4', ['e5']], ['m5', ['e3']], ['m6', ['e7']],
         ['m7', ['e1']], ['m8', ['e1', 'e3']], ['m9', ['e4', 'e6']], ['m10', ['e6']], ['m11', ['e6', 'e4']],
         ['m12', ['e1', 'e5']], ['m13', ['e5']], ['m14', ['e4', 'e7', 'e6']], ['m15', ['e7', 'e5']],
         ['m16', ['e3', 'e1']], ['m17', ['e3']], ['m18', ['e5', 'e3']], ['m19', ['e7', 'e6']], ['m20', ['e1']]],
    "Isabela Helena":
        [['m1', ['e1']], ['m2', ['e1']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e5']], ['m6', ['e6']],
         ['m7', ['e1']], ['m8', ['e1']], ['m9', ['e3', 'e4']], ['m10', ['e3']], ['m11', ['e6']], ['m12', ['e1']],
         ['m13', ['e5']], ['m14', ['e7', 'e6']], ['m15', ['e7', 'e6']], ['m16', ['e3']], ['m17', ['e5']],
         ['m18', ['e2']], ['m19', ['e5', 'e4']], ['m20', ['e5', 'e1']]],
    "Yan Romano":
        [['m1', ['e1', 'e2']], ['m2', ['e2']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e3']], ['m6', ['e7']],
         ['m7', ['e1']], ['m8', ['e1']], ['m9', ['e4', 'e6', 'e3']], ['m10', ['e3', 'e4']], ['m11', ['e6', 'e3']],
         ['m12', ['e1', 'e2']], ['m13', ['e3']], ['m14', ['e3', 'e4', 'e7']], ['m15', ['e3']], ['m16', []],
         ['m17', ['e3']], ['m18', ['e3']], ['m19', ['e6', 'e4', 'e7']], ['m20', ['e1', 'e2']]],
    "Filippo":
        [['m1', ['e1', 'e2']], ['m2', ['e1', 'e2']], ['m3', ['e3', 'e2', 'e1']], ['m4', ['e3', 'e1', 'e2']],
         ['m5', ['e2', 'e3']], ['m6', ['e4', 'e2', 'e7', 'e6', 'e1']], ['m7', ['e1']], ['m8', ['e1', 'e2']],
         ['m9', ['e6', 'e7']], ['m10', ['e7', 'e4']], ['m11', ['e4', 'e2', 'e7', 'e6']], ['m12', ['e5']],
         ['m13', ['e5', 'e3']], ['m14', ['e7', 'e4']], ['m15', ['e4', 'e7']], ['m16', ['e5']], ['m17', ['e2']],
         ['m18', ['e2']], ['m19', ['e4', 'e6', 'e7', 'e3']], ['m20', ['e1']]],
    "Luan Campos":
        [['m1', ['e1', 'e5']], ['m2', ['e5']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e3']], ['m6', ['e6', 'e7']],
         ['m7', ['e1']], ['m8', ['e1']], ['m9', ['e4']], ['m10', ['e7']], ['m11', ['e4']], ['m12', ['e1']],
         ['m13', ['e1']], ['m14', ['e4']], ['m15', ['e7']], ['m16', ['e3']], ['m17', ['e5']], ['m18', ['e5', 'e3']],
         ['m19', ['e7', 'e4']], ['m20', ['e1']]],
    "Bruna":
        [['m1', ['e1', 'e5']], ['m2', ['e5']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e1']], ['m6', ['e7', 'e5']],
         ['m7', ['e1', 'e5']], ['m8', ['e1']], ['m9', ['e4']], ['m10', ['e4', 'e7']], ['m11', ['e3', 'e4']],
         ['m12', ['e3']], ['m13', ['e5']], ['m14', ['e4', 'e6', 'e7']], ['m15', ['e5', 'e3']], ['m16', ['e5', 'e3']],
         ['m17', ['e2']], ['m18', ['e1', 'e2']], ['m19', ['e4', 'e7', 'e5']], ['m20', ['e1', 'e2']]],
    "testando":
        [['m1', ['e1', 'e5']], ['m2', ['e3']], ['m3', ['e5', 'e6']], ['m4', ['e7', 'e4']], ['m5', ['e4']],
         ['m6', ['e3']], ['m7', ['e5']], ['m8', ['e6', 'e4']], ['m9', ['e1']], ['m10', ['e3']], ['m11', ['e5']],
         ['m12', ['e6']], ['m13', ['e1']], ['m14', ['e2', 'e3', 'e4']], ['m15', ['e5', 'e4', 'e2']], ['m16', ['e7']],
         ['m17', ['e1']], ['m18', ['e2']], ['m19', ['e4']], ['m20', ['e6']]],
    "Luiz Felipe":
        [['m1', ['e1']], ['m2', ['e1']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e1']], ['m6', ['e3', 'e7']],
         ['m7', ['e1', 'e3']], ['m8', ['e1']], ['m9', ['e4']], ['m10', ['e4']], ['m11', ['e4']], ['m12', ['e1']],
         ['m13', ['e1']], ['m14', ['e4']], ['m15', ['e4']], ['m16', ['e5']], ['m17', ['e1']], ['m18', ['e1']],
         ['m19', ['e3']], ['m20', ['e1']]],
    "Ana Luisa Genn":
        [['m1', ['e1']], ['m2', ['e1']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e5']], ['m6', ['e6', 'e7']],
         ['m7', ['e1']], ['m8', ['e1']], ['m9', ['e3', 'e4']], ['m10', ['e3']], ['m11', ['e6', 'e7']],
         ['m12', ['e1']], ['m13', ['e5']], ['m14', ['e6', 'e7']], ['m15', ['e4', 'e7']], ['m16', ['e3']],
         ['m17', ['e5']], ['m18', ['e2']], ['m19', ['e4', 'e5']], ['m20', ['e1', 'e5']]],
    "Henrique":
        [['m1', ['e5', 'e4', 'e3']], ['m2', ['e5']], ['m3', ['e2', 'e3']], ['m4', ['e5']], ['m5', ['e2', 'e3']],
         ['m6', ['e3', 'e4']], ['m7', ['e1']], ['m8', ['e1', 'e2']], ['m9', ['e4']], ['m10', ['e4', 'e3']],
         ['m11', ['e3']], ['m12', ['e1', 'e3']], ['m13', ['e5']], ['m14', ['e2', 'e7', 'e6', 'e4']], ['m15', ['e6']],
         ['m16', ['e3']], ['m17', ['e2', 'e3']], ['m18', ['e6', 'e7']], ['m19', ['e4']], ['m20', ['e1', 'e2']]],
    "Raphaella":
        [['m1', ['e1']], ['m2', ['e5']], ['m3', ['e3']], ['m4', ['e1']], ['m5', ['e1']], ['m6', ['e3']],
         ['m7', ['e1']], ['m8', ['e1']], ['m9', ['e6', 'e7']], ['m10', ['e6', 'e7']], ['m11', ['e4', 'e3']],
         ['m12', ['e4', 'e3']], ['m13', ['e2', 'e5']], ['m14', ['e7', 'e6']], ['m15', ['e1', 'e7']], ['m16', ['e3']],
         ['m17', ['e1', 'e5', 'e6']], ['m18', ['e1', 'e3']], ['m19', ['e7', 'e6', 'e4', 'e1']],
         ['m20', ['e1', 'e5']]],
    "Carolina":
        [['m1', ['e1', 'e5']], ['m2', ['e5']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e1']], ['m6', ['e7']],
         ['m7', ['e1']], ['m8', ['e6']], ['m9', ['e4']], ['m10', ['e7', 'e3']], ['m11', ['e4']], ['m12', ['e3']],
         ['m13', ['e5', 'e2']], ['m14', ['e4', 'e7', 'e6']], ['m15', ['e4', 'e2']], ['m16', ['e3']], ['m17', ['e5']],
         ['m18', ['e3']], ['m19', ['e3']], ['m20', ['e1', 'e2']]],
    "Arthur Bueno":
        [['m1', ['e1']], ['m2', ['e5']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e1']], ['m6', ['e5', 'e7']],
         ['m7', ['e1']], ['m8', ['e1']], ['m9', ['e4']], ['m10', ['e4']], ['m11', ['e4']], ['m12', ['e3']],
         ['m13', ['e1']], ['m14', ['e4', 'e7']], ['m15', ['e7']], ['m16', ['e1', 'e4', 'e7', 'e6', 'e3', 'e5', 'e2']],
         ['m17', ['e5']], ['m18', ['e3']], ['m19', ['e6', 'e7']], ['m20', ['e1', 'e5']]],
    "Yan":
        [['m1', ['e5']], ['m2', ['e5']], ['m3', ['e1']], ['m4', ['e5']], ['m5', ['e3']], ['m6', ['e4', 'e7']],
         ['m7', ['e1']], ['m8', ['e1', 'e3']], ['m9', ['e4', 'e3', 'e7']], ['m10', ['e7']],
         ['m11', ['e7', 'e6', 'e4']], ['m12', ['e1', 'e5']], ['m13', ['e5']], ['m14', ['e7', 'e6', 'e4']],
         ['m15', []], ['m16', ['e1']], ['m17', ['e5', 'e3']], ['m18', ['e5', 'e3']], ['m19', ['e5', 'e7']],
         ['m20', ['e1']]],
    "Carlos Eduardo":
        [['m1', ['e1', 'e2', 'e3', 'e5']], ['m2', ['e1', 'e2', 'e3', 'e5', 'e6']],
         ['m3', ['e1', 'e2', 'e3', 'e5', 'e4']], ['m4', ['e1', 'e2', 'e3', 'e5']], ['m5', ['e3', 'e4']],
         ['m6', ['e1', 'e2', 'e3', 'e5', 'e6', 'e7']], ['m7', ['e1', 'e2', 'e5']], ['m8', ['e1', 'e4', 'e5', 'e6']],
         ['m9', ['e3', 'e4', 'e7']], ['m10', ['e3', 'e4', 'e7']], ['m11', ['e3', 'e4', 'e6']],
         ['m12', ['e1', 'e3', 'e5']], ['m13', ['e1', 'e2', 'e3']], ['m14', ['e1', 'e3', 'e7']],
         ['m15', ['e3', 'e4', 'e7']], ['m16', ['e3', 'e4', 'e5', 'e6', 'e7']],
         ['m17', ['e4', 'e2', 'e3', 'e5', 'e1', 'e7']], ['m18', ['e1', 'e3', 'e4', 'e5']],
         ['m19', ['e3', 'e4', 'e7']], ['m20', ['e1', 'e2', 'e3', 'e5']]],
    "Maria Clara":
        [['m1', ['e1', 'e3']], ['m2', ['e5', 'e2']], ['m3', ['e3']], ['m4', ['e5', 'e3']], ['m5', ['e1']],
         ['m6', ['e4', 'e7']], ['m7', ['e1']], ['m8', ['e1', 'e6']], ['m9', ['e7', 'e6']], ['m10', ['e6', 'e7']],
         ['m11', ['e4', 'e3']], ['m12', ['e4', 'e3']], ['m13', ['e2', 'e5']], ['m14', ['e7', 'e2']],
         ['m15', ['e1', 'e7', 'e4', 'e6']], ['m16', ['e5', 'e3']], ['m17', ['e1', 'e6']], ['m18', ['e1']],
         ['m19', ['e4', 'e7', 'e6', 'e1']], ['m20', ['e1', 'e2']]],
    "Victor Bazin":
        [['m1', ['e1']], ['m2', ['e1']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e5']], ['m6', ['e7']],
         ['m7', ['e1']], ['m8', ['e5']], ['m9', ['e3']], ['m10', ['e3']], ['m11', ['e6']], ['m12', ['e6']],
         ['m13', ['e2']], ['m14', ['e4']], ['m15', ['e7']], ['m16', ['e3']], ['m17', ['e5']], ['m18', ['e3']],
         ['m19', ['e6']], ['m20', ['e5']]],
    "Rhyelle":
        [['m1', ['e1']], ['m2', ['e1']], ['m3', ['e3']], ['m4', ['e5']], ['m5', ['e5']], ['m6', ['e6']],
         ['m7', ['e1']], ['m8', ['e1']], ['m9', ['e4']], ['m10', ['e3']], ['m11', ['e7', 'e6']], ['m12', ['e1']],
         ['m13', ['e5']], ['m14', ['e4']], ['m15', ['e3']], ['m16', ['e3']], ['m17', ['e5']], ['m18', ['e2', 'e3']],
         ['m19', ['e4', 'e5']], ['m20', ['e1', 'e2', 'e3']]]}


class Grapher(Graph):

    def __init__(self, **attr):
        super().__init__(**attr)
        self.__DB = self.__edges = self.__nodes = self._nodes = self.__graph = None
        self.learner_spread_map = None
        self._learner_graphs = None
        self._learner_diff = None
        self._learner_spread = None
        self._learner_distance = None
        self._learner_node_spread = None
        self._learner_node_distance = None
        self.CLEGEND = None
        self.clustered = None

    def _hidden_add_nodes_from(self, spikes):
        [self.__nodes.setdefault(node, dict(index=ind, vertex=node, size=len(ends), edges=set()))
            for ind, (node, ends) in enumerate(spikes)]

    def legend(self, node):
        _ = self
        return LEGEND[int(node[1:]) - 1]

    def _add_nodes_from(self, spikes):
        nfactor = 20.0
        # NFACTOR = 70.0
        for ind, node_ends in enumerate(spikes):
            node, ends = node_ends
            if node in self.__nodes:
                size = self.__nodes[node]['size']
                vertex = self.__nodes[node]['vertex']
                size = self.__nodes[node]['size'] = size + len(ends) / nfactor
                vertex.set(size=size)

            else:
                '''v = self.newVertex(shape="sphere", fontsize="24",
                                   fontcolor="#00ffff", color="#ffff00",
                                   label=self.legend(node),
                                   size=len(ends) / nfactor)'''
                v = self.legend(node)
                self.__nodes[node] = dict(index=ind, vertex=v,
                                          size=len(ends) / nfactor,
                                          pos=(randint(-100, 100), randint(-100, 100)))

    def _add_edge(self, edge, weight):
        # FUW = 14600.0
        # FW = 11
        # FW = 4
        fuw = 8000.0
        # FUW = 8600.0; FW =12
        fro, to = edge
        fro, to = self.__nodes[fro]['vertex'], self.__nodes[to]['vertex']
        self.add_edge(fro, to, weight=weight / fuw, length=weight / 1.5)
        '''self.newEdge(fro, to, spline=False, strength=weight / FUW,
                     width=weight / 1.5,
                     showstrain=True, visible=weight > FW)'''

    def _hidden_add_edge(self, edge, weight):
        fro, to = edge
        fro, to = self.__nodes[fro]['vertex'], self.__nodes[to]['vertex']
        # print (fro, to),
        fro, to = edge_label = min((fro, to), (to, fro))
        self.__edges[edge_label] = dict(fro=fro, to=to, weight=weight)

    @staticmethod
    def _assemble_edges(spikes):
        def mx(t, o):
            return max((t, o), (o, t))
        ts = themearrays = {}
        [ts.update({idea: ts.setdefault(idea, {theme}) | {theme}})
         for theme, ideas in spikes for idea in ideas]
        te = theme_edges = {}
        [te.update({mx(th, oth): te.setdefault(mx(th, oth), []) + [idea]})
         for idea, themes in themearrays.items() for th in themes for oth in themes if th != oth]
        # print themearrays
        return theme_edges  # themearrays

    def main_shown(self, db):
        # draw_random(self)
        # plt.show()
        self._nodes = self.__nodes = {}
        self.__graph = {}
        self.clear()
        # self.single_graph()
        self.multiple_graph(db)

    @staticmethod
    def statistics(learnerset):
        alunomprox = min(learnerset)
        mediaaluno = sum(learnerset) / len(learnerset)
        alunomdist = max(learnerset)
        var = sum(
            (aluno - mediaaluno) ** 2 for aluno in learnerset
        ) ** 0.5 / len(learnerset)
        print(
            "alunomprox: %s, mediaaluno: %s, alunomdist: %s, var: %s" % (
                alunomprox, mediaaluno, alunomdist, var
            ))

    def multiple_graph(self, db):
        self.__DB = db
        for graph in db.values():
            theme_edges = self._assemble_edges(graph)
            self._add_nodes_from(graph)
            [self._add_edge(edge, weight=len(idea))
             for edge, idea in theme_edges.items()]
        # print theme_edges
        # self.add_nodes_from('m%d'%i for i in range(1,21))

    def single_graph(self, user, base):
        theme_edges = self._assemble_edges(base[user])
        # print theme_edges
        self._add_nodes_from(DB[user])
        [self._add_edge(edge, weight=len(idea))
         for edge, idea in theme_edges.items()]

    def hidden(self):
        self.__nodes = {}
        self.__edges = {}
        self.__nodes = {}

    def _hidden_graph(self, base):
        theme_edges = []
        for graph in base.values():
            theme_edges = self._assemble_edges(graph)
            self._hidden_add_nodes_from(graph)
            [self._hidden_add_edge(edge, weight=len(idea))
             for edge, idea in theme_edges.items()]
        self._attach_edges_to_nodes()
        print(
            theme_edges)
        return

    def single_hidden_graph(self, user, base):
        self.hidden()
        theme_edges = self._assemble_edges(base[user])
        # print theme_edges
        self._hidden_add_nodes_from(base[user])
        [self._hidden_add_edge(edge, weight=len(idea))
         for edge, idea in theme_edges.items()]
        self._attach_edges_to_nodes()
        return self

    def _attach_edges_to_nodes(self):
        [self.__nodes[vertex]["edges"].add(edge_) for edge_ in self.__edges for vertex in edge_]

    def _hidden_graph_diff(self, learner):
        x = sum(1 for edge in self.__edges.keys()
                if not (edge in learner.__edges.keys()))
        y = sum(1 for edge in learner.__edges.keys()
                if not (edge in self.__edges.keys()))
        print(
            len(learner.__edges.keys()), len(self.__edges.keys()), x, y, x + y)
        print('\n')
        return x + y

    def distance(self, edge, learner):
        return abs(self.__edges[edge]['weight'] - learner.__edges[edge]['weight'])

    def _hidden_graph_distance(self, learner):
        x = [self.distance(edge, learner) for edge in self.__edges.keys()
             if (edge in learner.__edges.keys())]
        return x

    def _hidden_nodes_distance(self, learner):
        assert isinstance(learner.__nodes, dict)
        assert isinstance(list(learner.__nodes.values())[0]['edges'], set)
        nodes = [sum(self.distance(edge, learner) for edge in learner.__nodes[node]['edges'])
                 for node in self.clustered]
        return nodes

    def map_learner_spread(self, spread):
        def prune(cluster, togo):
            if cluster.branches:
                if togo < 0:
                    cluster.branches = None
                else:
                    for branch in cluster.branches:
                        prune(branch, togo - cluster.height)

        def list_of_clusters0(cluster, alist):
            if not cluster.branches:
                alist.append(list(cluster))
            else:
                for branch in cluster.branches:
                    list_of_clusters0(branch, alist)

        # def listOfClusters(root):
        #     l = []
        #     listOfClusters0(root, l)
        #     return l

        tree = [[0, 3, 11, 22, 1, 12], [2, 24, 16, 4, 6, 17, 20, 9],
                [7, 23, 13, 18, 10, 8, 14, 21], [5, 19, 15]]
        learner_strength = lambda o: sum(index
                                         for index, nodes in enumerate(tree) if o in nodes)
        # learner_strength = lambda o: sum(node
        #                    for node in o)
        # map = [[learner,learner_strength(nodes)//40]+[node
        #        for node in nodes] for learner, nodes in enumerate(spread)]
        mapper = [[learner, learner_strength(learner)] + [
            20 - node for node in nodes] for learner, nodes in enumerate(spread)]
        self.learner_spread_map = [['nam', 'siz'] + [node for node in self.__nodes],
                                   ['s', 'd'] + ['c'] * (0 + len(self.__nodes)),
                                   ['i', 'c'] + [' '] * (0 + len(self.__nodes))]
        self.learner_spread_map += mapper
        '''TODO: rethink orange here!
        f = file('learnermatrix.tab', 'w')
        w = writer(f, delimiter='\t')
        for line in self.learner_spread_map: w.writerow(line)
        f.close()
        return
        data = orange.ExampleTable("learnermatrix")
        import orngClustering
        root = orngClustering.hierarchicalClustering(data, order=False)
        classifier = orange.BayesLearner(data)
        prune(root, 1.9)
        # for n, cluster in enumerate(listOfClusters(root)):
        #    print "\n\n Cluster %i \n" % n
        #    for instance in cluster:
        #        print instance
        for i in range(25):
            c = classifier(data[i])
            print
            "original", data[i].getclass(), "classified as", c'''

    def map_node_gravity(self):
        edge_strength = lambda n, o: sum(self.__edges[e]['weight']
                                         for e in self.__nodes[o]['edges']
                                         if n != o and n in e)
        node_strength = lambda o: sum(self.__edges[e]['weight']
                                      for e in self.__nodes[o]['edges'])
        with open('nodematrix.tab', 'w') as f:
            w = writer(f, delimiter='\t')
            mapper = [[node, node_strength(node) // 10] + [
                edge_strength(node, othernode) for othernode in self.__nodes] for node in self.__nodes]
            mapper += [['nam', 'siz'] + [node for node in self.__nodes],
                       ['s', 'd'] + ['c'] * (0 + len(self.__nodes)),
                       [' ', 'c'] + [' '] * (0 + len(self.__nodes))]
            for line in mapper:
                w.writerow(line)
            f.close()
        clustered = "11,6,15,10,19,9,18,5,16,7,1,3,2,12,17,8,13,20,4,14"
        self.CLEGEND = [LEGEND[int(theme) - 1] for theme in clustered.split(',')]
        self.clustered = ['m' + i for i in clustered.split(',')]

    def main_hidden(self, db):
        self.hidden()
        self._hidden_graph(db)
        print('NODES:', self.__nodes)

    def stats_hidden(self, db=None):
        db = db or DB
        self._learner_graphs = [Grapher().single_hidden_graph(learner, db)
                                for learner in db]
        assert isinstance(self._learner_graphs[0].__nodes, dict)
        self._learner_diff = [self._hidden_graph_diff(learner)
                              for learner in self._learner_graphs]
        self.statistics(self._learner_diff)
        self._learner_spread = [self._hidden_graph_distance(learner)
                                for learner in self._learner_graphs]
        self._learner_distance = [sum(self._hidden_graph_distance(learner))
                                  for learner in self._learner_graphs]
        self.statistics(self._learner_distance)
        self._learner_node_spread = [self._hidden_nodes_distance(learner)
                                     for learner in self._learner_graphs]
        self.map_learner_spread(self._learner_node_spread)
        self._learner_node_distance = [sum(self._hidden_nodes_distance(learner))
                                       for learner in self._learner_graphs]
        print(
            self._learner_node_distance)
        self.statistics(self._learner_node_distance)

    def plot_hidden(self):
        figure(1)
        boxplot(self._learner_spread)
        figure(2)
        for learner in self._learner_spread:
            plot(learner)
        figure(3)
        boxplot(self._learner_node_spread)
        fg = figure(4)
        _ = radar_factory(20)
        for learner_class in range(4):
            for learner in self.learner_spread_map:
                if learner[1] == learner_class:
                    thetas = len(learner) - 2
                    ax = fg.add_subplot(2, 2, learner_class + 1, projection='radar')
                    ax.set_varlabels(self.CLEGEND)
                    r = np.arange(0, 2 * np.pi, 2 * np.pi / thetas)
                    # print 'THETAS ',thetas, len(r)

                    ax.plot(r, learner[2:])
        show()

    def main(self, db=None):
        db = db or PDB
        db = dict((user, [(theme, spike) for theme, spike in content if len(spike) > 1])
                  for user, content in db.items())
        for id_ in db['Jailson'][1][1]:
            print(f"id {id_}")
        # self.main_shown(db)
        self.main_hidden(db)
        self.map_node_gravity()
        self.stats_hidden()
        self.plot_hidden()
        return
        # print 'EDGES', self.__edges


if __name__ == "__main__":
    Grapher().main()
