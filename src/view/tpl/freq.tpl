<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Frequencia</title>
    <!-- stylesheets -->
    <meta name="viewport" container="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/file/BibliotecaIES/freq.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link rel="shortcut icon" href="image/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="/site/css/cards.css">
    <script type="text/javascript" src="https://cdn.rawgit.com/brython-dev/brython/3.5.0/www/src/brython.js"></script>
    <script type="text/python">
        from browser import document, html
        from main import main


        class Browser:
            DOC, HTML = document, html
            front = document["front"]


        main(Browser)


    </script>

</head>

    <body onLoad="brython({debug:1, cache:'browser', static_stdlib_import:true,
     pythonpath :['/file/BibliotecaIES/']})">
    <!-- table class="open" id="headings" border="0px" cellpadding="1" cellspacing="1" style="width:100%;">
        <tr>
            <td border="0px" class="open container header bolding center">
                <div class="head">
                    <img src="/file/BibliotecaIES/minerva.png">
                </div>
            </td>
            <td border="0px" colspan = "9" class="open">
                <div class = "heading">
                  <span> UNIVERSIDADE FEDERAL DO RIO DE JANEIRO</span>
                </div>
            </td>
        </tr>
    </table -->
    <div class="header">
                <div class="head">
                    <img src="/file/BibliotecaIES/minerva.png">
                </div>
                <div class = "heading">
                  <span class="vcenter"> UNIVERSIDADE FEDERAL DO RIO DE JANEIRO</span>
                </div>
    </div>
    <br>
    <div>
        <table id="front" border="1mm" cellpadding="1" cellspacing="1"> <!-- style="height:9.12cm;width:7.18cm;"-->
            <tr>
                <td colspan = "6" class="container header bolding center">
                    <div>Folha de ponto de Instituto Tércio Pacitti de Aplicações e Pesquisas Computacionais</div>
                </td>
            </tr>

            <tr>
                <td class="container">
                    <div>Nome:</div>
                    <!-- Column 1 -->
                </td>

                <td colspan = "3" class="container left_bold">
                    <div class="container">Carlo Emmanoel Tolla de Oliveira</div>
                    <!-- Column 2 -->
                </td>
                <td class="container">
                    <div>Mês/Ano:</div>
                    <!-- Column 1 -->
                </td>

                <td class="container left_bold">
                    <div class="container">Outubro/2018</div>
                    <!-- Column 2 -->
                </td>
            </tr>

            <tr>
                <td class="container">
                    <div>SIAPE:</div>
                    <!-- Column 1 -->
                </td>

                <td colspan = "3" class="container left_bold">
                    <div class="container">0359933</div>
                    <!-- Column 2 -->
                </td>
                <td class="container">
                    <div>Carga Horária:</div>
                    <!-- Column 1 -->
                </td>

                <td class="container left_bold">
                    <div class="container">40h</div>
                    <!-- Column 2 -->
                </td>
            </tr>

            <tr>
                <td class="container">
                    <div>Lotação:</div>
                    <!-- Column 1 -->
                </td>

                <td colspan = "5" class="container left_bold">
                    <div class="container">Área de Ensino e Pesquisa</div>
                    <!-- Column 2 -->
                </td>

            </tr>

            <tr>
                <td class="container">
                    <div>Cargo:</div>
                    <!-- Column 1 -->
                </td>

                <td colspan = "5" class="container left_bold">
                    <div class="container">Analista em Tecnologia da Informação</div>
                    <!-- Column 2 -->
                </td>

            </tr>

            <tr rowspan = "2">
                <td class="container dcontainer">
                    <div>DIA</div>
                    <!-- Column 1 -->
                </td>

                <td class="container dcontainer">
                    <div class="container">ENTRADA</div>
                    <!-- Column 2 -->
                </td>
                <td colspan = "2" class="container dcontainer">
                    <div class="container">ALMOÇO</div>
                    <!-- Column 1 -->
                </td>

                <td class="container dcontainer">
                    <div class="container">SAÍDA</div>
                    <!-- Column 2 -->
                </td>

                <td class="container dcontainer">
                    <div class="container">RUBRICA</div>
                    <!-- Column 2 -->
                </td>
            </tr>
        </table>
    </div>
<p style="page-break-after: always;">&nbsp;</p>
    <div>
        <div class="header">
                    <div class="head">
                        <img src="/file/BibliotecaIES/minerva.png">
                    </div>
                    <div class = "heading">
                      <span class="vcenter"> UNIVERSIDADE FEDERAL DO RIO DE JANEIRO</span><br>
                      <span class="vcenter"> UNIVERSIDADE FEDERAL DO RIO DE JANEIRO</span><br>
                      <span class="vcenter"> UNIVERSIDADE FEDERAL DO RIO DE JANEIRO</span>
                    </div>
        </div>
        <br>
        <table id="back" border="1mm" cellpadding="1" cellspacing="1" style="width:95%;">
            <tr>
                <table>
                    <tr class="open">
                        <td class="open" colspan="2">
        01 - Os códigos de frequência somente serão aceitos se estiverem de acordo com as relações de ocorrências no ponto
                        </td>
                    </tr>
                    <tr class="open">
                        <td class="open" colspan="2">
                            02 - O Código LT somente poderá ser lançado se houver o respectivo comprovante médico fornecido pela
                        </td>
                    </tr>

                </table>
            </tr>
            <tr class="open">
                <td class="open">
                    Códigos a serem lançados no ponto:
                </td>
                <td class="open">
                    Atrasos ou saídas antecipadas:
                </td>
            </tr>
            <tr class="open">
                <td class="open">
                    &nbsp;
                </td>
                <td class="open">
                    &nbsp;
                </td>
            </tr>
            <tr class="open">
                <td class="open">
                    FN - Faltas
                </td>
                <td class="open">
                    A1 - igual ou menor que um hora
                </td>
            </tr>
        </table>
    </div>
</body>

</html>