BRY = None


class Session(object):

    def paint(self, front):
        """
            <span class="navbar-item">
                <a class="button is-white is-outlined" href="#">
                    <span class="icon is-large">
                        <i class="fa fa-home"></i>
                    </span>
                    <span>Biblioteca</span>
                </a>
            </span>

        :param front: Menu to be painted
        :param data: Data to be painted in
        :return: DOM item generated
        """
        html = BRY.HTML

        def do_item(day, month, year):
            the_row = html.TR()
            row = f"{day:02}/{month}/{year},0:0,0:0,0:0,0:0,&nbsp; "
            row = row.split(",")
            for column in row:
                dv = html.DIV(column)
                cell = html.TD(Class="container center")
                cell <= dv
                the_row <= cell
            return the_row

        def do_box(classes, content, rowspan=1, colspan=1, row=None):
            the_row = row if row else html.TR()
            dv = html.DIV(content)
            cell = html.TD(Class=classes, rowspan=rowspan, colspan=colspan)
            cell.setAttribute("colspan", colspan)
            if rowspan > 1:
                cell.setAttribute("rowspan", rowspan)
            cell <= dv
            the_row <= cell
            return the_row

        def do_card(name):
            """

            :param name: Nome Completo
            :param role: Papel profissional (Pesquisador, Mestrado, Egresso)
            :param home: Link do Activ
            :param links: Dicionário com outros links {LATTES: "", FACE: ""}
            :return:
            """
            icon = '<a href="{link}" title={title}><i class="fa fa-{icon} fa-2x" aria-hidden="true"></i></a>'
            # icons = "\n".join(icon.format(title=ref["icon"], **ref) for ref in links)
        mont, year = 10, 2018
        for day in range(1, 31):
            front <= do_item(day, mont, year)

        front <= do_box("high", "OBS:", 1, 6)
        row = do_box("double-high", "", 2, 3)
        front <= row
        row = do_box("high center", "Atesto as informações acima", 1, 3, row=row)
        front <= row
        row = do_box("double-high", "oi", 1, 3)
        front <= row
        row = do_box("container center", "Assinatura do Servidor", 1, 3)
        front <= row
        row = do_box("container center", "Direção da Área", 1, 3, row=row)
        front <= row
        return front


def main(brython):
    global BRY
    BRY = brython
    Session().paint(BRY.front)


if __name__ == '__main__':
    from browser import document, html


    class Browser:
        DOC, HTML = document, html
        front = document["front"]


    main(Browser)
