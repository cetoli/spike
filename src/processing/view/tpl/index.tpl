<!DOCTYPE html>
<html lang="en">
<head>
    <title>SuperPython</title>
    <meta http-equiv="content-type" content="application/xml;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.8.0/p5.js"></script>
    <!--script type="text/javascript" src="https://j.mp/brython_371"></script -->
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/brython/3.4.0/brython.min.js">
    </script>
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/brython/3.4.0/brython_stdlib.js">
    </script>
    <link rel="stylesheet"
          href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/highlight.min.js"></script>
    <script type="text/python">
            from pfive.main import main
            from browser import window
            #from livro.naomi import main
            '''
            from _spy.vitollino.main import Jogo, STYLE
            window.ey =500

            STYLE["width"], STYLE["height"] = 900, "650px"
            jogo = Jogo()
            jogo.clica_elemento = lambda *_: None
            jogo._clica_elemento = lambda _= None, j=jogo: j.clica_elemento()
            window.clica_elemento = jogo._clica_elemento
            '''
            jogo = window.p5
            main(jogo)

    </script>
    <style>
        /* #9E7681,#E4E2D7,#FA089B,#EC7C9C,#2F3F53,#9E7681,#E4E2D7,#FA089B */
        body
        {
          background-color: #052233;
        }
        .center-div
        {
          position: absolute;
          margin: auto;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          width: 900px;
          height: 650px;
          background-color: #9E7681;
          border-radius: 3px;
          text-align: center;
}    </style>

    <!--
    #! /usr/bin/env python
    # -*- coding: UTF8 -*-
    # Este arquivo é parte do programa Sherlock
    # Copyright 2010-2019 Carlo Oliveira <carlo@nce.ufrj.br>,
    # `Labase <http://labase.selfip.org/>`__; `GPL <http://j.mp/GNU_GPL3>`__.
    #
    # Sherlock é um software livre; você pode redistribuí-lo e/ou
    # modificá-lo dentro dos termos da Licença Pública Geral GNU como
    # publicada pela Fundação do Software Livre (FSF); na versão 2 da
    # Licença.
    #
    # Este programa é distribuído na esperança de que possa ser útil,
    # mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
    # a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
    # Licença Pública Geral GNU para maiores detalhes.
    #
    # Você deve ter recebido uma cópia da Licença Pública Geral GNU
    # junto com este programa, se não, veja em `<http://www.gnu.org/licenses/>`_

    """Brython front end client.

    .. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

    """

    -->
</head>
<body onLoad="brython({debug:1, cache:'browser', static_stdlib_import:true,
 pythonpath :['__code']})"><!-- background="https://i.imgur.com/8axRcxg.jpg"-->
    <!--
<div id="pydiv" class="center-div">
    <H1>&nbsp</H1>
    <H1>&nbsp</H1>
    <H1>A G U A R D E</H1>
    <H1>&nbsp</H1>
    <H1>&nbsp</H1>
    <H1>-- Página Carregando ---</H1>
    <H1>&nbsp</H1>
    <H1>&nbsp</H1>
    <H1>⚫ ⚫ ⚫</H1>
</div>    -->

</body>
</html>
