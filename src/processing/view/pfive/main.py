from pfive.pfive import Pfive


class MyDrawing(Pfive):
    def __init__(self):
        self._bas_color = {"1": (200, 0, 0), "2": (0, 200, 0), "3": (0, 0, 200), "4": (200, 200, 0)}
        super().__init__()

    def draw(self):
        self.fill(self.color(*self._bas_color["2"]))
        self.ellipse(250, 350, 380, 380)

    def setup(self):
        self.createCanvas(700, 410)


def main(*_):
    MyDrawing()
