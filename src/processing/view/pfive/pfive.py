from browser import window as w

_p5c = """alpha blue brightness color green hue lerpColor lightness red saturation
 background clear colorMode fill noFill noStroke stroke arc ellipse circle
 line point quad rect square triangle ellipseMode noSmooth rectMode smooth
 strokeCap strokeJoin strokeWeight bezier bezierDetail bezierPoint bezierTangent
 curve curveDetail curveTightness curvePoint curveTangent beginContour beginShape
 bezierVertex curveVertex endContour endShape quadraticVertex vertex plane box
 sphere cylinder cone ellipsoid torus loadModel model cursor frameRate
 noCursor windowResized fullscreen pixelDensity displayDensity getURL getURLPath
 getURLParams remove noLoop loop push pop redraw createCanvas
 resizeCanvas noCanvas createGraphics blendMode setAttributes applyMatrix resetMatrix
 rotate rotateX rotateY rotateZ scale shearX shearY translate createStringDict
 createNumberDict append arrayCopy concat reverse shorten shuffle sort splice subset
 boolean byte char unchar unhex join match matchAll nf nfc nfp nfs split splitTokens
 trim setMoveThreshold setShakeThreshold
 createImage saveCanvas saveFrames
 loadImage image tint noTint imageMode blend copy get loadPixels updatePixels
 loadJSON loadStrings loadTable loadXML loadBytes httpGet httpPost httpDo createWriter save
 saveJSON saveStrings saveTable day hour minute millis month second year createVector
 ceil constrain dist exp floor lerp log mag norm sq sqrt noise noiseDetail noiseSeed
 randomSeed random randomGaussian acos asin atan atan2 cos sin tan degrees radians angleMode
 textAlign textLeading textSize textStyle textWidth textAscent textDescent loadFont text textFont
 orbitControl debugMode noDebugMode ambientLight directionalLight pointLight lights loadShader
 createShader shader resetShader normalMaterial texture textureMode textureWrap ambientMaterial
 specularMaterial shininess camera perspective ortho createCamera setCamera"""

_ex = "windowResized######"


class Pfive:
    def __init__(self):
        self.ellipse = self.createCanvas = self.preload = lambda *_: None
        self.fill = self.noLoop = self.color = lambda *_: None
        self.p = self
        self.__p5 = [p for p in _p5c.split() if p not in _ex]
        # self.__p5 = [p for p v in globals().items() if v == 0xff and p not in _ex]

        def sketch(p):
            self.p = p
            for processing_command in self.__p5:
                exec(f'self.{processing_command} = p.{processing_command}')
            p.preload = self.preload
            p.setup = self.setup
            p.draw = self.draw

        # j.window.setup = self.setup
        w.p5.new(sketch)

    def preload(self):
        ...

    def draw(self):
        ...

    def setup(self):
        ...
