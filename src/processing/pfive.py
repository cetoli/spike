import bottle
import os
import sys

from bottle import TEMPLATE_PATH, static_file, route, default_app, get
project_home = os.path.dirname(os.path.abspath(__file__))

# add your project directory to the sys.path
# project_home = u'/home/supygirls/dev/SuPyGirls/src'
if project_home not in sys.path:
    sys.path = [project_home] + sys.path

application = default_app()

# Create a new list with absolute paths
# TEMPLATE_PATH.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../view/tpl')))
# make sure the default templates directory is known to Bottle
tpl_dir = os.path.join(project_home, 'view/tpl')
img_dir = os.path.join(project_home, 'view/image')
css_dir = os.path.join(project_home, 'view/css')
js_dir = os.path.join(project_home, 'view/js')
# py_dir = os.path.join(project_home, 'view/bies')
py_dir = os.path.join(project_home, 'view')
vt_dir = os.path.join(project_home, '../../../_spy')
# prefix = "/file/BibliotecaIES/<filepath:re:.*.."
prefix = "<filepath:re:.*.."
vtpfix = "__code/_spy/<filepath:re:.*.."
pypfix = "__code/<filepath:re:.*.."
if tpl_dir not in TEMPLATE_PATH:
    TEMPLATE_PATH.insert(0, tpl_dir)


@route('/')
@bottle.view('index')  # ('cards')
def index():
    return {}


# Static Routes
@get(f"{prefix}(png|jpg|svg|gif|ico)>")
def img(filepath):
    return static_file(filepath, root=img_dir)


# Static Routes
@get(f"{prefix}ttf>")
def font(filepath):
    return static_file(filepath, root=css_dir)


# Static Routes
@get(f"{prefix}css>")
def css(filepath):
    return static_file(filepath, root=css_dir)


# Static Routes
@get(f"{prefix}js>")
def js(filepath):
    return static_file(filepath, root=js_dir)


# Static Routes
# @get(f"{pypfix}py>")
@get("/__code/<filepath:re:.*/__init__>.py")
def pyinip(_):
    # print("pyiini", filepath)
    return ""


# Static Routes
# @get(f"{pypfix}py>")
@get("/__code/<filepath:re:[^_].*..py>")
def py(filepath):
    # print("py", filepath, py_dir)
    return static_file(filepath, root=py_dir)


# Static Routes
@get("/__code/_spy/<module_name>/<filepath:re:.*..py>")
def spy(module_name, filepath):
    # print("spy", module_name, filepath)
    # print("spy", module_name, filepath, vt_dir)
    return static_file(filepath, root=os.path.join(vt_dir, module_name))


# Static Routes
# @get(f"{pypfix}py>")
@get("/__code/<filepath:re:.*/__init__>.py")
def pyini(filepath):
    _ = filepath
    # print("pyioni", filepath)
    return ""


if __name__ == "__main__":
    bottle.run(host='localhost', port=8080)
