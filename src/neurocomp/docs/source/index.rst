.. Neurocomp documentation master file, created by
   sphinx-quickstart on Sat Feb 29 21:57:55 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Neurocomp's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro.rst
   modules.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
