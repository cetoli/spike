.. _neurocomp_modules:

################################
Neurocomp - Core Modules
################################

.. _core_indexer:

Indexer Module
==============

.. automodule:: indexer
    :members:
    :undoc-members:
    :show-inheritance:
    :platform: Desktop
    :synopsis: Indexes a given text into a graph.

.. seealso::

   Page :ref:`core_introduction`

.. _core_downloader:

PDF Cleaner Module
==================

.. automodule:: pdfcleaner
    :members:
    :undoc-members:
    :show-inheritance:
    :platform: Desktop
    :synopsis: Load and clean pdfs from the web.

.. seealso::

   Page :ref:`core_introduction`

.. _core_relator:

Context Word Relator
====================

.. automodule:: relator
    :members:
    :undoc-members:
    :show-inheritance:
    :platform: Desktop
    :synopsis: Relates words into context.

.. seealso::

   Page :ref:`core_introduction`

.. _core_utilities:

General Utilities Module
========================

.. automodule:: utilities
    :members:
    :undoc-members:
    :show-inheritance:
    :platform: Desktop
    :synopsis: Scripts used by core modules.

.. seealso::

   Page :ref:`core_introduction`

.. note::
   Main core modules.

