.. _core_introduction:

Neurocomp - Manual
==================

    Neruocomp investigation searchs to extract meaning from text using computational intelligence.

Downloading Files
-----------------
    This module downloads a collection of pdf files from SBIE conference. Pdfs are converted to plain text by calling system utilities. Text are then cleaned from publishing artifacts like page headings and numbers.

    .. seealso::

       Page :ref:`core_downloader`

Indexing a Text
---------------
    The GraphText class loads a text file and clean stop words for semantic inferences.
    The words are parsed through a word similarity calculator to generate a tag list of
    semantic relevance for the text contents.

    .. seealso::

       Page :ref:`core_indexer`

Relating Words in a Context
---------------------------
    Key words are scanned througout a text within a contextual range. They are related acconding to the matching count of their intersections.

    .. seealso::

       Page :ref:`core_relator`

Neurocomp - Modules
===================

Investigate content semantics studying graphs extracted from texts

Documented functionalities:

* Neurocomp Core Modules : Basic Entities

    :ref:`neurocomp_modules`

