#! /usr/bin/env python3
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa Neurocomp
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Calculate similar words, dynamic format string, strip stop words, process cli args.


This module contains a series of utilities for most modules.

Example CLI:
    Test the cli parser::

        $ python3.8 utilities.py -i <inputfile> -o <outputfile>

Example Module Call:
    call from another module::

        from utilities import similars  # calculate similarities of two given words.
        from utilities import make_format  # generate a dynamic format string.
        from utilities import no_stops  # strips a text from stop words.
        from utilities import main  # programmable cli arguments parser.


Ther are no modules attributes, they are packed inside a class not to clutter namespace.

Attributes:
        class CONST: Contains the constants used in de module.

        * CONST.options dict: example config dictionary for CLI parse.

        * CONST.SIMILARITY_THRESHOLD (float):


        There are no other classes in this module.

Todo:
    * TODO: Generate a class for CLI parsing.

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""


def helper(help_format_string: str) -> dict:
    """Format a comma separated string into a dictionary with three entries.

        :param help_format_string: str: a string with three parts, comma separated.
        :return: dict: a dictionary whith *min*, *arg* and *hlp* keys mapped to the string parts.

        "a,b,c" -> {'min': a, 'arg': b, 'hlp': c}

        * a: option entry is double minus preceeded <--arg_name>
        * b: argument placeholder appearing in the help line after option name
        * c: option explanation appearing in the line for options help


        Example:
             The return dict will be passed to the *main* service function::

                from utilities import helper
                dict_argument = helper("ofile,out,file to be overwritten")
                '''dict_argument =  {'min': 'ofile', 'arg': 'out', 'hlp': 'file to be overwritten'}'''

        .. seealso::

           Page :ref:`Core Utilities <core_utilities>`

    """
    help_kind = "min arg hlp".split()
    return dict(zip(help_kind, help_format_string.split(",")))


class CONST:
    """Constant container for the module, defines defaul and simirarity threshold.

        .. seealso::

           Page :ref:`Core Utilities <core_utilities>`

    """
    options = dict(
        h=helper(",,explain the usage"),
        f=helper(",,force graph"),
        p=helper(",,prune stop words"),
        i=helper("ifile,ipt,input file"),
        o=helper("ofile,out,output_file"),
    )
    """Options used to configure the main cli option parser."""
    SIMILARITY_THRESHOLD = 0.5
    """defines the proportion of the word radix considered for matched similarity."""


def similars(word_key: str, word: str) -> str:
    """Match two words that must have the initial portion equal up to threshold index.

        :param word_key: str: the word to be matched.
        :param word: str: a sample that attemps match.
        :return: str: The return word will be the sample if match occurs or *None*.

        .. seealso::

           Page :ref:`Core Utilities <core_utilities>`

    """
    from itertools import takewhile
    similarity = sum(1 for _ in takewhile(lambda letter: letter[0] == letter[1], zip(word_key, word)))
    return word if (similarity / len(word_key) > CONST.SIMILARITY_THRESHOLD) and (word_key != word) else None
    pass


def make_format(key_word: str, eq_list: list) -> str:
    """Create sequence of whitespace separated words beginning with key word followed by a list.

            :param key_word: str: the word leading the gtring.
            :param eq_list: list: lis of words that follow.
            :return: str: keyword followed by a list of words in eq_list.

    """
    format_string = (len(eq_list) + 1) * "{} " + "\n"
    return format_string.format(key_word, *eq_list)


def no_stops(text: str, stop_words: list) -> list:
    """Create list of words whitespace separate removing stop words and puntuation.

            :param text: str: a text to be cleaned from stop words.
            :param stop_words: list: lis of stop words to be removed from text.
            :return:list: lower case words list from given text excluding stop word and punctuation.

    """
    text = [tag.lower().strip(".!?;-+(),@#$%&*{}ªº") for tag in text.split() if tag.isalpha()]
    return [word for word in text if (word not in stop_words) and len(word) > 2]


def main(module_name: str, option_string: dict = None, argv=None, help_no_opt: bool = True) -> dict:
    """Programmable handler for cli parsing of optios and arguments.

            :param module_name: str: name of tho module as to be called from command line.
            :param option_string: dict: configuration of help lines as given in helper.
            :param argv: sys.argv: comamnd line arguments receive from system.
            :param help_no_opt: bool: show help if no arguments given when **True**.
            :return: dict: dict of parameters extracted from cli parsing.

    """
    def print_help(status=0, *_, **__):
        print(f'{module_name} [OPTS:] [ARGS]')
        up, do = "\t--{min} {arg}\t: {hlp}", "\t-{min}{opt} {arg}\t\t: {hlp}"
        [print((up if option_help["min"] else do).format(opt=option, **option_help))
         for option, option_help in option_string.items()]
        sys.exit(status)

    def argum_handler(slot, argument=True):
        arguments[slot] = argument or True

    import sys
    import getopt
    argv = argv or sys.argv[1:]
    option_string = option_string or CONST.options
    double_minus_options = [f"{args['min']}=" for args in option_string.values() if args["min"]]
    options = "".join(f"{op}:" if arg["min"] else op for op, arg in option_string.items())
    # print(f"double_minus_options {double_minus_options} options {options}")
    arguments = {}  # {opt.strip("-"): False for opt in option_string.keys()}
    handle_key = [opt.strip("=") for opt in list(option_string.keys())+double_minus_options]
    handlers = {opt: argum_handler for opt in handle_key}
    handlers["-h"] = print_help
    handlers["h"] = print_help
    try:
        opts, args = getopt.getopt(argv, f"{options}", double_minus_options)
        # opts, args = getopt.getopt(argv, f"{options}", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print_help(2)
        sys.exit(2)
    [handlers[opt.strip("-")](opt.strip("-"), arg)for opt, arg in opts] if opts else help_no_opt and print_help(2)
    # if False:
    #     if opt == '-h':
    #         print_help()
    #         sys.exit()
    #     elif opt[1] in options:
    #         arguments["option"] = opt
    #     elif opt in ("-i", "--ifile"):
    #         arguments["inputfile"] = arg
    #     elif opt in ("-o", "--ofile"):
    #         arguments["outputfile"] = arg
    # arguments.pop("h") if  "h" in arguments else None
    return arguments


if __name__ == "__main__":
    def tester(i=None, o=None, ifile=None, ofile=None, **kwargs):
        i, o = i or ifile, o or ofile
        task = dict(f=lambda: print(f"got f o={o} i={i}"), p=lambda: print(f"got p o={o} i={i}"))
        print("kwargs", kwargs, i, o)
        [task[op]() for op, arg in kwargs.items() if arg]
    tester(**main("utilities.py"))
    # print(main("utilities.py"))
