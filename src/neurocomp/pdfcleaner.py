#! /usr/bin/env python
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa Neurocomp
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Download pdfs from url, convert to text and clean.

This module downloads a collection of files from SBIE conference.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python3.8 pdfcleaner.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

Todo:
    * For module TODOs
    * You have to also use ``sphinx.ext.todo`` extension

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""


import re
import subprocess
from time import sleep
from bs4 import BeautifulSoup
from urllib.request import urlopen, urlretrieve
from json import dump, load
import sys
sys.path.insert(0, 'conf')


class CONST:
    site_url = "https://br-ie.org/pub/index.php/sbie"
    conf = "conf/{}"
    data = "data/{}"
    texto = "text/{}"
    file_link = conf.format("file_link.json")
    Patterns = [("""VIII Congresso Brasileiro de Informática na Educação (CBIE 2019)
Anais do XXX Simpósio Brasileiro de Informática na Educação (SBIE 2019)

""", ""), ("", "")]


class TextPreProcess:
    def __init__(self, link=None):
        self.text_link = link or self.scrap_from_url()

    def scrap_from_url(self, url=CONST.site_url, conf_file=CONST.file_link):
        html_page = urlopen(url)
        self.text_link = []
        soup = BeautifulSoup(html_page, features="html5lib")
        self.text_link = [
            link.get('href').replace("view", "download")
            for link in soup.findAll('a', attrs={'href': re.compile("^https://")})
            if "/6" in link.get('href')[-6:]]
        # link.get('href').replace()
        print(f"self.text_link {self.text_link}")
        with open(conf_file, "w") as confile:
            dump(self.text_link, confile)
        return self.text_link

    def download_files(self):
        for link in self.text_link[:3]:
            file_name = CONST.texto.format(f"texto{link[-4:]}.pdf")
            print(file_name)
            sleep(1)
            urlretrieve(link, file_name)

    def convert_pdf_to_txt(self):
        for link in self.text_link[:3]:
            file_name = CONST.texto.format(f"texto{link[-4:]}.pdf")
            out_filename = file_name.replace(".pdf", ".txt")
            subprocess.run(["pwd"])
            subprocess.run(["pdftotext", file_name, out_filename])

    def clean_txt_files(self):
        regular = r"\nFigura|Tabela .*\n\n|\[.* \d+\]|\(\d+\)|\n\n\d+\n\n|\nX\n|(\d+\.*)+,*\d+| [+-=%]"
        regfig = re.compile(regular, re.IGNORECASE)
        for link in self.text_link[1:3]:
            file_name = CONST.texto.format(f"texto{link[-4:]}.txt")
            file_out = CONST.texto.format(f"ctexto{link[-4:]}.txt")
            with open(file_name, "r") as text_file:
                text = text_file.read()
                # text = regex.sub("", text)
                text = regfig.sub("", text)
                # [print(line) for line in text.split("\n")[100:300]]
                for i, j in CONST.Patterns:
                    text = text.replace(i, j)
                # [print(line) for line in text.split("\n")[100:300]]
                with open(file_out, "w") as text_out:
                    text_out.write(text)


def main():
    import os.path
    link = None
    if os.path.isfile(CONST.file_link):
        print(f"os.path.isfile {CONST.file_link}")
        with open(CONST.file_link, "r") as confile:
            link = load(confile)
    text_pro = TextPreProcess(link=link)
    current = 1
    print(f"text processed: ", CONST.texto.format(f"ctexto{text_pro.text_link[current][-4:]}.txt"))
    # text_pro.scrap_from_url()
    # text_pro.download_files()
    # text_pro.convert_pdf_to_txt()
    text_pro.clean_txt_files()
    # return
    from indexer import GraphText
    file_name = CONST.texto.format(f"ctexto{text_pro.text_link[current][-4:]}.txt")
    file_out = CONST.data.format(f"ctexto{text_pro.text_link[current][-4:]}.json")
    gtext = GraphText(textfile=file_name)
    gtext.initiate_3dforce(file_out)
    gtext.initiate_graph(file_out[:-4]+"gv")


if __name__ == "__main__":
    main()
