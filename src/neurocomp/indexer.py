#! /usr/bin/env python3
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa Neurocomp
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Index the words in a text file forming a graph, pruning stop words and similarities

The GraphText class loads a text file and clean stop words for semantic inferences.
The words are parsed through a word similarity calculator to generate a tag list of
semantic relevance for the text contents.

Example CLI:
    Test the cli parser::

        $ python3.8 indexer.py -i <inputfile> -o <outputfile>

Example Module Call:
    call from another module::

        from utilities import similars  # calculate similarities of two given words.
        from utilities import make_format  # generate a dynamic format string.
        from utilities import no_stops  # strips a text from stop words.
        from utilities import main  # programmable cli arguments parser.



Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
        class CONST: Contains the constants used in de module.
        * CONST.help_kind (str): keys to the options dictionary.
        * CONST.options (dict): example config dictionary for CLI parse.
        * CONST.SIMILARITY_THRESHOLD (float):


        There are no other classes in this module.

Todo:
    * TODO: Generate a class for CLI parsing.

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""
from collections import Counter
from graphviz import Graph
from json import dump, load
from utilities import similars, make_format, helper, main


class File:
    conf = "conf/{}"
    data = "data/{}"
    texto_original = conf.format("textomscindex.txt")
    stop_words = conf.format("stopwords.txt")
    non_stop = conf.format("nonstopnn.txt")
    similar_words = conf.format("similar_word_list_bad.txt")
    mscivgraph = data.format("mscivgraph.json")
    word_cloud = data.format("nonstopcloud.txt")
    relation_file = conf.format("word_relations.json")
    edge_file = data.format("links.txt")
    search_range = 7
    options = dict(
        s=helper(",inou,word similarity file [meaning alias*]"),
        e=helper(",out,edge labeling file [s -> t : l]"),
        g=helper(",out,graph file to 3D forge viewer"),
        r=helper(",in,word relation dictionary json {s: t: l:}"),
        w=helper(",inou,stop words list not relevant to meaning"),
        p=helper(",,prune stop words: remove duplicates <-p -w stop_word.txt>"),
        d=helper(",,create similarity dictionary <-p -s stop_word.txt>"),
        f=helper(",,generate 3D graph <-s -g graph.json -r relation.json -e edges.txt>")
    )


class GraphText:
    def __init__(self, text=None, textfile=File.texto_original, similarities=None, non_stop=None):

        self.text = text.split()[1:] if text else self.load(textfile)
        self.taglist = {}
        self.remove_stop_words()
        self.create_taglist_lowercase_from_file(non_stop) if non_stop else self.create_taglist_lowercase()
        self.similarities = {}
        self.create_text_from_given_taglist(similarities) if similarities else None

    @staticmethod
    def load(textfile):
        with open(textfile, "r") as tags:
            return tags.read().split()[1:]

    def remove_stop_words(self, stop_file=File.stop_words):
        with open(stop_file, "r") as stop_tags:
            stop_words = stop_tags.read().split()
            print(f"stop_words{stop_words}")
            self.text = [tag.lower().strip(".!?;-+(),@#$%&*{}ªº") for tag in self.text]
            self.text = [word for word in self.text if (word not in stop_words) and len(word) > 2]

    def create_taglist_lowercase(self):
        taglist = [tag for tag in self.text if tag[:-1] not in self.text]
        self.taglist = Counter(taglist)

    def create_text_from_given_taglist(self, similarities=File.similar_words):
        with open(similarities, "r") as similars_list:
            taglist = [
                record.split() for record in similars_list.read().split("\n")
                if (len(record) > 3 and (len(record.split()) > 1))]
            taglist = {word: similar for word, *similar in taglist}
            taglist = {simile: word for word, similar in taglist.items() for simile in similar}
            self.text = [tag if tag not in taglist else taglist[tag] for tag in self.text]

    def create_taglist_lowercase_from_file(self, non_stop_file=File.non_stop):
        with open(non_stop_file, "r") as nsfile:
            taglist = [record.split()[0] for record in nsfile.read()[1:].split("\n") if len(record) > 3]
            # taglist = [record for record in nsfile.read()[1:].split("\n") if len(record) > 2]
            self.taglist = {word: 0 for word in taglist}
            print(f"self.taglist: {self.taglist}")
            self.text = [tag for tag in self.text if tag in self.taglist]

    def create_taglist_lowercase_numbered(self, non_stop_file=File.non_stop):
        with open(non_stop_file, "r") as nsfile:
            taglist = [record.split() for record in nsfile.read()[1:].split("\n") if len(record) > 3]
            self.taglist = {word: int(count) for word, count in taglist}
            print(f"self.taglist: {self.taglist}")
            self.text = [tag for tag in self.text if tag in self.taglist]

    def _create_equivalence_dict(self, equivalence_file=File.similar_words):
        with open(equivalence_file, "w") as eq_file:
            self.similarities = similarities = {
                word_key: [similars(word_key, sample_word)
                           for sample_word in self.taglist if similars(word_key, sample_word)]
                for word_key in self.taglist}
            [eq_file.write(make_format(key_word, eq_list)) for key_word, eq_list in similarities.items()]

    def create_equivalence_dict(self, equivalence_file=File.similar_words):
        with open(equivalence_file, "w") as eq_file:
            taglist = self.taglist.keys()
            self.similarities = similarities = {word_key: [] for word_key in taglist}
            for word_key in taglist:
                for sample_word in taglist:
                    similarity = similars(word_key, sample_word)
                    if similarity:
                        similarities[word_key].append(similarity)
            similarities = {key: value for key, value in similarities.items() if value}
            [eq_file.write(make_format(key_word, eq_list)) for key_word, eq_list in similarities.items()]

    def initiate_graph(self, filename=f'data/msciv.gv', dt=7):
        filename = filename.replace('.', f"{dt}.")
        taglist = self.text
        taggraph = Counter()
        # dt = 7
        dot = Graph(comment='Mestrado Indústria 4.0', format="png", engine="dot", strict=True)
        [taggraph.update({(a, b): dt - i for a, b in zip(taglist, taglist[i:]) if a != b}) for i in range(1, dt + 1)]
        # tagback = {(b, a): count for (a, b), count in taggraph.items() if (b, a) in taggraph}
        # taggraph.update({(a, b): count for (b, a), count in tagback.items()})
        # [taggraph.pop(key) for key in tagback if key in taggraph]
        # taggraph = {a: b for a, b in taggraph.items() if b > dt*1.75}
        taggraph = {a: b for a, b in taggraph.items() if b > dt}
        print(taggraph)
        edgeweight = set(i // 5 / 2 for i in taggraph.values())
        tagcnt = Counter(a for a, _ in taggraph.keys())
        tagcnt.update(a for _, a in taggraph.keys())
        tagcntset = sorted(list(set(tagcnt.values())), reverse=True)
        tagdict = {n: f"n{i}" for i, n in enumerate(tagcnt.keys())}
        color = "red pink orange green blue cyan purple gray".split()
        colordict = {name: color[min(tagcntset.index(value), len(color) - 1)] for name, value in tagcnt.items()}
        # fontdict = {name: font[min(tagcntset.index(value), len(color)-1)] for name, value in tagcnt.items()}
        # font = list(range(48, 2, -2))
        # fl = len(font)
        nodeweight = list(set(i // 8 for i in tagcnt.values()))[::-1]
        nodeweight = {size: col for size, col in zip(nodeweight, color)}
        nodeweight[0] = "gray"
        nodesize = {name: (count // 8 + 1) for name, count in tagcnt.items()}
        nodefontsize = {name: (count // 2 + 12) * 2.0 for name, count in tagcnt.items()}
        nodefontsize[0] = 4.0
        edgelenghtmax = elx = max(set(taggraph.values())) + 1
        [dot.node(
            i, n, color=colordict[n], penwidth=f'{2.0 + nodefontsize[n] / 2}',
            height=f"{nodesize[n] / 2}", width=f"{nodesize[n]}", fontsize=str(nodefontsize[n]),
        ) for n, i in tagdict.items()]
        # [dot.node(
        #     i, n, color=nodeweight[tagcnt[n]//8], penwidth=f'{2.0+ nfs[n]/2}',
        #     height=f"{ns[n]/2}", width=f"{ns[n]}", fontsize=str(nfs[n]),
        # ) for n, i in tagdict.items()]
        [dot.edge(tagdict[a], tagdict[b], penwidth=str(s), len=str((elx - s) / 10)) for (a, b), s in taggraph.items()]
        print(edgeweight, len(taggraph), tagdict, taggraph)
        print(edgelenghtmax, nodefontsize, nodeweight, len(tagcnt), tagcnt)
        dot.render(filename=filename, view=True)

    def initiate_3dforce(self, mscivgraph_file=File.mscivgraph, relation_file=None, link_file=None, dt=7):

        def incorporate_relation_labels():
            with open(relation_file, "r") as file_relation:
                return {
                    tuple("{source} {target}".format(**record).split()): "{label}".format(**record)
                    for record in load(file_relation)}

        taglist = self.text
        taggraph = Counter()
        # dt = 7
        relations = incorporate_relation_labels() if relation_file else {}
        # print("relations", relations)
        [taggraph.update({(a, b): dt - i for a, b in zip(taglist, taglist[i:]) if a != b}) for i in range(1, dt + 1)]
        taggraph = {a: (b, relations[a]) if a in relations else (b, "---")
                    for a, b in taggraph.items() if b > dt}
        # taggraph = {a: b for a, b in taggraph.items() if b > dt}
        [print(f"{source} -> {target} : {label}") for (source, target), (_, label) in taggraph.items()]
        tagcnt = Counter(a for a, _ in taggraph.keys())
        tagcnt.update(a for _, a in taggraph.keys())
        tagcntset = sorted(list(set(tagcnt.values())), reverse=True)
        tagdict = {n: f"n{i}" for i, n in enumerate(tagcnt.keys())}
        color = "red pink orange green blue cyan purple gray".split()
        colordict = {name: color[min(tagcntset.index(value), len(color) - 1)] for name, value in tagcnt.items()}
        nodeweight = list(set(i // 8 for i in tagcnt.values()))[::-1]
        nodeweight = {size: col for size, col in zip(nodeweight, color)}
        nodeweight[0] = "gray"
        nodesize = {name: (count // 8 + 1) for name, count in tagcnt.items()}
        mscivgraph = dict(
            nodes=[dict(id=nome, group=colordict[nome], val=nodesize[nome] * 4) for nome, _ in tagdict.items()],
            links=[{"source": a, "target": b, "width": s // 4, "label": l}
                   for (a, b), (s, l) in taggraph.items() if (b, a) not in taggraph],
        )
        with open(mscivgraph_file.replace(".", f"{dt}."), "w") as mscivgr:
            dump(mscivgraph, mscivgr)
        if link_file:
            with open(link_file, "w") as lfile:
                [lfile.write(f"{source} -> {target} : {label}\n") for (source, target), (_, label) in taggraph.items()]


def generate_cloud(file=File.non_stop, out=File.word_cloud):
    with open(file, "r") as nsfile:
        in_tags = [record.split() for record in nsfile.read()[1:].split("\n") if len(record) > 3]
        print(in_tags)
        # return
        with open(out, "w") as eq_file:
            [[eq_file.write(f"{key_word} ") for _ in range(int(count))] for key_word, count, *_ in in_tags]


def clean():
    with open("nonstop.txt", "r") as nsfile:
        in_tags = [record.split() for record in nsfile.read()[1:].split("\n") if len(record) > 3]
        print(in_tags)
        # return
        with open("nonstopcloud.txt", "w") as eq_file:
            [[eq_file.write(f"{key_word} ") for _ in range(int(count))] for key_word, count, *_ in in_tags]


class Mediator:
    # F, E, P, *_ = range(9)
    #
    # cli_mapper = dict(e=E, f=F, p=P)

    def __init__(self, t=None, s=None, r=None, e=None, g=None, w=None, **kwargs):
        self.arguments = dict(
            similarities=s or File.similar_words,
            relation_file=r or File.relation_file,
            link_file=e or File.edge_file,
            mscivgraph=g or File.mscivgraph,
            stop_file=w or File.stop_words
        )
        runner = dict(
            e=self.create_equivalence_dictionary,
            f=self.create_3d_force_graphic_json_input,
            p=self.prune_stop_words_list,
        )
        runner[t] if t else [runner[op](**self.arguments) for op, arg in kwargs.items() if arg]
        # runner = {
        #     self.E: self.create_equivalence_dictionary,
        #     self.F: self.create_3d_force_graphic_json_input,
        #     self.P: self.prune_stop_words_list
        # }
        # runner[task]()

    @staticmethod
    def create_3d_force_graphic_json_input():
        graf_text = GraphText(similarities=File.similar_words)
        graf_text.initiate_3dforce(dt=File.search_range, relation_file=File.relation_file, link_file=File.edge_file)

    @staticmethod
    def create_equivalence_dictionary():
        graf_text = GraphText()
        graf_text.create_equivalence_dict()

    @staticmethod
    def prune_stop_words_list(stop_file=File.stop_words):
        with open(stop_file, "r") as stop_tags:
            stop_words = sorted(set(stop_tags.read().split()))
            print(f"stop_words{stop_words}")
        with open(stop_file, "w") as stop_tags_write:
            stop_tags_write.write("\n".join(stop_words))


if __name__ == "__main__":
    Mediator(**main("indexer.py", option_string=File.options))
