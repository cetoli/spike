from json import dump
from utilities import similars, no_stops
SIMILARITY_THRESHOLD = 0.5


class CONST:
    num_page = 9
    stop_clipper = 2
    node_size_theshold = 4
    node = "{id}"
    value = "val"
    nodes = "nodes"
    edges = "links"
    source = "source"
    target = "target"
    edge = f"{{{source}}} {{{target}}}"
    conf = "conf/{}"
    data = "data/{}"
    texto = "text/{}"
    file_link = conf.format("mscivgraph5.json")
    stop_words = conf.format("stopwords.txt")
    link_defs = conf.format("word_definitions_base.json")
    similar_words = conf.format("similar_word_definitions.json")
    relations_file = conf.format("word_relations.json")
    kw_base = conf.format("textomscindex.txt")


class TagRelator:
    def __init__(self, link=None, edge_file=CONST.file_link, kw_base=None):
        self.better_nodes = self.text = []
        self.selected_edges = self.select_from_better_edges(edge_file)
        self.link_defs = self.search_kw_base(kw_base) if kw_base else (link or self.scrap_from_url())
        self.similar_dict = sd = self.create_equivalence_dict()  # if link else {}
        self.link_defs = {
            node: [sd[tag] if tag in sd else tag for tag in defs]
            for node, defs in self.link_defs.items()}
        self.best_relations = self.find_best_relations()

    def scrap_from_url(self):

        from google import google
        # from time import sleep
        num_page = 6
        with open(CONST.link_defs + "n", "w") as defs_file, open(CONST.stop_words, "r") as stop_tags:
            stop_words = stop_tags.read().split()
            # link_defs = {}
            # for node in self.better_nodes:
            #     sleep(10)
            #     link_defs[node] = no_stops(" ".join(r.description[:-4] for r in google.search(node, num_page)))
            link_defs = {
                node: no_stops(
                    " ".join(r.description[:-4]for r in google.search(node, num_page)), stop_words)
                for node in self.better_nodes}
            dump(link_defs, defs_file)
        return link_defs

    def select_from_better_edges(self, edge_file):
        with open(edge_file, "r") as edges:
            from json import load
            network = load(edges)
            edges, nodes = network[CONST.edges], network[CONST.nodes]
            self.better_nodes = [
                CONST.node.format(**node)
                for node in nodes if node[CONST.value] >= CONST.node_size_theshold]
            the_nodes = " ".join(self.better_nodes)
            edges = ["{source} {target}".format(**edge).split() for edge in edges
                     if (edge[CONST.source] in the_nodes) and (edge[CONST.target] in the_nodes)]
            return edges

    def create_equivalence_dict(self):
        taglist = sorted(list(set(value for values in self.link_defs.values() for value in values)), reverse=True)
        print("taglist", taglist)
        similarities = {
            word_key: similarity_list for word_key in taglist
            if (similarity_list := list(set(
                similarity for sample_word in taglist
                if (similarity := similars(word_key, sample_word))))) if (len(word_key) > 3)
        }
        entries = sorted(list(similarities.keys()), reverse=True)
        singular_similes = {}
        all_similies = ""
        for entry in entries:
            all_similies += " ".join(sig for sigies in singular_similes.values() for sig in sigies)
            singular_similes.update({entry: similarities[entry]}) if entry not in all_similies else None
        print("singular_similes", singular_similes)
        return {similar: original
                for original, similarity_list in singular_similes.items()
                for similar in similarity_list}

    def find_best_relations(self):
        def match_ocurrences(source, target):
            from collections import Counter
            source_stopper = " ".join(self.similar_dict[source]) if source in self.similar_dict else ""
            target_stopper = " ".join(self.similar_dict[target]) if target in self.similar_dict else ""
            stopper = stop + f"{source} {target}" + source_stopper + target_stopper + f"{source}s {target}s"
            match_count = Counter(
                source_def for source_def in self.link_defs[source] if source_def not in stopper
                for target_def in self.link_defs[target] if target_def not in stopper
                if source_def == target_def)
            counts = [(count, key) for key, count in match_count.items()]
            return sorted(counts, reverse=True)[:6] if counts else None  # "N O N E"

        stop = "profi consumo comum consumo desse prevê poderão contém nestes trabalhos"
        stop += "quais perto admar sisu minas parou veja todos sinop gerên brasileiros dias"
        stop += "instituída através conheça horas aprenderam através informe translati"
        stop += "notar últimas relato segunda acompanhe notar últimas seleção porto capaz entra"
        stop += "confira estará secretário campus mundo vagas reais marx influencing acesso discute"
        stop += "individuo alunos line pesquisados contato pares penna inteira aprendizaje indo"
        stop += "nchez games final maría shiv capra   "
        node_defs = list(self.link_defs.keys())
        return {(source, target): match_ocurrences(source, target)
                for source in node_defs for target in node_defs if source != target}

    def save_best_relations(self, relations_file=CONST.relations_file):
        with open(relations_file, "w") as relation_file:
            self.best_relations
            relations = [dict(source=source, target=target, label=label[0][1])
                         for (source, target), label in self.best_relations.items() if label]
            dump(relations, relation_file)

    def search_kw_base(self, kw_base, stop_file=CONST.stop_words, ln_defs=CONST.link_defs):
        def search(node_, num_page, text):
            text_len = len(text)
            return [" ".join(text[max(0, index-num_page): min(text_len, index+num_page)])
                    for index, word in enumerate(text) if word == node_]
        with open(kw_base, "r") as kw_file, open(ln_defs, "w") as defs_file, open(stop_file, "r") as stop_tags:
            self.text = kw_file.read().split()[1:]
            stop_words = stop_tags.read().split()
            print(f"stop_words{stop_words}")
            self.text = [tag.lower().strip(".!?;-+(),@#$%&*{}ªº") for tag in self.text]
            self.text = [word for word in self.text if (word not in stop_words) and len(word) > CONST.stop_clipper]
            print("self.text", search('cognição', CONST.num_page, self.text)[:20])
            link_defs = {node: no_stops(" ".join(r for r in search(node, CONST.num_page, self.text)), stop_words)
                         for node in self.better_nodes}
            dump(link_defs, defs_file)
        return link_defs


def main():
    # with open(CONST.link_defs, "r") as word_defs:
    #     from json import load
    #     links = load(word_defs)
    tagrel = TagRelator(kw_base=CONST.kw_base)  # link=links)  # link={0: []})
    print("selected edges", tagrel.selected_edges)
    print("selected nodes", tagrel.better_nodes)
    print("selected nodes defs", tagrel.link_defs)
    print("similar defs", tagrel.similar_dict)
    [print(f"best relations: {relation} -> {label}")
     for relation, label in tagrel.best_relations.items() if label]
    tagrel.save_best_relations()


if __name__ == "__main__":
    main()
