'''
import http.cookiejar
import urllib.request
import urllib.parse
from base64 import b64decode as dtk
import ssl
ssl._create_default_https_context = ssl._create_unverified_context


def tk():
    return dtk(b'bGFiYXNlNGN0MXY=').decode("UTF8")


class ActivPipe:
    def __init__(self, url="https://activufrj.nce.ufrj.br"):
        self.url = url
        # cj = http.cookiejar.CookieJar()
        self.opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor())
        top_level_url = "https://activufrj.nce.ufrj.br/login"

        password_mgr = urllib.request.HTTPBasicAuthHandler()
        password_mgr.add_password(None, top_level_url, "carlo", tk())

        handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
        self.opener.add_handler(handler)
        self.opener.addheaders = [('User-agent', 'Mozilla/5.0')]
        urllib.request.install_opener(self.opener)
        request = urllib.request.Request(url)
        resp = urllib.request.urlopen(request)
        self.cookie = [
            cook for key, cook in resp.getheaders() if key == 'Set-Cookie'][0].split("=")[1].split(";")[0]
        urllib.request.install_opener(self.opener)
        payload = {
            '_xsrf': self.cookie,
            'user': 'carlo',
            'passwd': tk()
        }
        data = urllib.parse.urlencode(payload)
        binary_data = data.encode('UTF-8')
        request = urllib.request.Request(top_level_url, binary_data)
        urllib.request.urlopen(request)

    def painter(self, url):
        url = self.url + url
        url = "https://activufrj.nce.ufrj.br/bookmarks/BibliotecaIES/_relatorio_"
        req = urllib.request.Request(url)
        resp = urllib.request.urlopen(req)
        print("resp =  request.Request", resp.getheaders())
        pg = str(resp.read())
        while pg:
            print(pg[:100])
            pg = pg[100:]
'''

LABASE_LNK = [dict(icon="flask", link="http://labase.superpython.net")]
LABASE = "http://labase.superpython.net"
LBL = dict(flask="labase", tint="lattes", github="github", gitlab="gitlab", facebook="facebook", book="livrozilla")
LOREMPIX = "http://lorempixel.com/400/200/business/{}"
DUMMY = "https://i.imgur.com/QtjhrpM.jpg"
LOREM = "Bacon ipsum dolor amet ball tip brisket salami doner swine tenderloin." \
        "  Sausage leberkas bacon pork belly boudin.  Short ribs meatloaf prosciutto tail sausage meatball." \
        "  Cow pork loin kielbasa beef fatback ham jowl shankle porchetta ground round jerky kevin." \
        "  Ground round ball tip pancetta, picanha jerky ham boudin ham hock kevin."
ID_DATA = [
    dict(
        name="Carlo E. T. Oliveira",
        role="Pesquisador",
        bio="Possui doutorado em Computação - University Of London (1991)."
            " Atualmente é um pesquisador da Universidade Federal do Rio de Janeiro."
            " Tem experiência na área de Ciência da Computação, com ênfase em Informática Educacional,"
            " Sistemas de informação e Engenharia de Software atuando principalmente nos seguintes temas:"
            " neuropedagogia, neurociência computacional, orientação a objetos, uml, sistemas distribuídos,"
            " arquitetura de software e sistemas peer-to-peer. ",
        home="http://activufrj.nce.ufrj.br/profile/carlo",
        links=[
            dict(icon="flask", link=LABASE),
            dict(icon="tint", link="http://lattes.cnpq.br/9627675808739540"),
            dict(icon="github", link="https://github.com/cetoli"),
            dict(icon="gitlab", link="https://gitlab.com/cetoli"),
            dict(icon="facebook", link="https://www.facebook.com/carlo.oliveira"),
        ],
        photo="http://activufrj.nce.ufrj.br/photo/carlo",
        back=LOREMPIX
    ),
    dict(
        name="Isabel Hortência G. P. B.",
        role="Mestranda",
        bio="Mestranda em Informática pela UFRJ, possui graduação em Desenho Industrial"
             " e em Licenciatura em Computação pela UFJF. Atualmente é Coordenador do Curso de Tecnologia "
             "em Sistemas da Computação pelo CEDERJ/UAB- UFF Universidade Federal Fluminense"
             " onde também atua como tutor. Atua como Docente de Informatica no SENAC em Cursos"
             " Técnicos e profissionalizantes. Foi instrutor de informática da"
             " Fundação de Apoio à Escola Técnica do Estado do Rio de Janeiro FAETEC."
             " Tem experiência na área de Educação, com ênfase em Educação, atuando principalmente"
             " no seguinte tema: ensino, Web 2.0, Educação a Distância e Informática na Educação. ",
        home="http://activufrj.nce.ufrj.br/profile/IsabelBarros",
        links=[
            dict(icon="flask", link=LABASE),
            dict(icon="tint", link="http://lattes.cnpq.br/2327365626384469"),
            dict(icon="facebook", link="https://www.facebook.com/isabel.barros.9047"),
        ],
        photo="http://activufrj.nce.ufrj.br/photo/IsabelBarros",
        back=LOREMPIX
    ),
    dict(
        name="TATIANA DE SOUSA RIBEIRO",
        role="Bibliotecária",
        home="http://activufrj.nce.ufrj.br/profile/tatiana.ribeiro",
        links=[
            dict(icon="flask", link=LABASE),
            dict(icon="book", link="http://livrozilla.com/doc/941726/ribeiro--tatiana-de-sousa"),
        ],
        photo="http://activufrj.nce.ufrj.br/photo/tatiana.ribeiro",
        back="http://lorempixel.com/400/200/business"
    )
]
SESSIONS = [
    dict(
        session="Biblioteca",
        icon="home",
        href="#"
    ),
    dict(
        session="Notícias",
        icon="bullhorn",
        href="#"
    ),
    dict(
        session="Ajuda",
        icon="question-circle",
        href="#"
    ),
    dict(
        session="Cadastro",
        icon="file-signature ",
        href="#"
    ),
    dict(
        session="Quem Somos",
        icon="info-circle",
        href="#"
    )
]

CARD = """
                <div class="column is-one-third">
                    <div class="card large">
                        <div class="card-image">
                            <figure class="image">
                                <img src="{back}" alt="{name}">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-left">
                                    <figure class="image is-96x96">
                                        <img src="{photo}" alt="Image">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <p class="title is-4 no-padding">{name}</p>
                                    <p><span class="title is-6"><a href="{home}">@homepage</a></span></p>
                                    <p class="subtitle is-6">{role}</p>
                                </div>
                            </div>
                            <div class="content">
                                {bio}
                            </div>
                            <div class="content">
                                {icons}
                            </div>
                        </div>
                    </div>
                </div>

            """


BRY = object()


class ActivReader:
    def reader(self, url="https://activufrj.nce.ufrj.br/rest/wiki/BibliotecaIES/_relatorios_"):
        def display(msg, *_):
            print("display(msg, *_)", msg)

        def on_complete(request):
            if request.status == 200 or request.status == 0:
                self.parse(request.text)
                print("on_complete", request.text)
            else:
                display("error " + request.status + request.text)

        # print(SAVE, jsrc)
        req = BRY.AJAX.ajax()
        req.bind('complete', on_complete)
        req.set_timeout('20000', lambda *_: display("NOT SAVED: TIMEOUT"))
        req.open('GET', url, True)
        req.set_header('content-type', 'application/json')  # x-www-form-urlencoded')
        print("reader", url, req)
        req.send()

    def parse(self, data):
        import json, re
        data = data.replace("\r\n", "")
        data = data.replace("\n", "")
        activ_date = json.loads(data)
        conteudo =activ_date["result"]["wikidata"]["conteudo"]
        conteudo =re.sub("<[^>]*>", "", conteudo)
        conteudo = conteudo.replace("'", '"')
        conteudo =re.sub("([,\[][{ ]*) ([^ ]*):", r'\1 "\2":', conteudo)
        conteudo =re.sub('"year": ([0-9]*)', r'"year": "\1"', conteudo)
        for l in conteudo.split(","):
            print(l+',')
        #return
        ficha = json.loads(conteudo)
        print(ficha)
        for l in ficha:
            print(l)





class Session(object):

    def paint(self, main_menu, id_cards, data=SESSIONS, id_data=ID_DATA):
        """
            <span class="navbar-item">
                <a class="button is-white is-outlined" href="#">
                    <span class="icon is-large">
                        <i class="fa fa-home"></i>
                    </span>
                    <span>Biblioteca</span>
                </a>
            </span>

        :param main_menu: Menu to be painted
        :param data: Data to be painted in
        :return: DOM item generated
        """
        html = BRY.HTML

        def do_item(session, icon, href):
            menu_item = html.SPAN(Class="navbar-item")
            a = html.A(Class="button is-white is-outlined", href=href)
            s = html.SPAN(Class="icon is-large")
            i = html.I(Class="fa fa-{}".format(icon))
            t = html.SPAN(session)
            menu_item <= a
            a <= s
            s <= i
            a <= t
            print("def do_item(session, icon, href):", session, icon, href, a.Class, s.Class, i.Class)
            return menu_item

        def do_card(name, role="Pesquisador", bio=LOREM, home=LABASE, links=LABASE_LNK, photo=DUMMY, back=LOREMPIX):
            """

            :param name: Nome Completo
            :param role: Papel profissional (Pesquisador, Mestrado, Egresso)
            :param home: Link do Activ
            :param links: Dicionário com outros links {LATTES: "", FACE: ""}
            :return:
            """
            back = back.format(role) if "{}" in back else back
            icon = '<a href="{link}" title={title}><i class="fa fa-{icon} fa-2x" aria-hidden="true"></i></a>'
            # icons = "\n".join(icon.format(title=ref["icon"], **ref) for ref in links)
            icons = " ".join(icon.format(title=LBL[ref["icon"]], **ref) for ref in links)
            card = dict(name=name, role=role, bio=bio, home=home, icons=icons, photo=photo, back=back)
            #print("icons = ", CARD, card.keys())
            return CARD.format(**card)

        navbar = html.DIV(Class="navbar-end")
        for item in data:
            navbar <= do_item(**item)
        #print("def paint(self, main_menu, data=SESSIONS):", data)
        main_menu.html = ""
        main_menu <= navbar
        id_cards.html = "\n".join(do_card(**ids) for ids in id_data)
        # print(id_data)
        # print("\n".join(do_card(**ids) for ids in id_data))
        return main_menu


def main(brython):
    global BRY
    BRY = brython
    Session().paint(BRY.main_menu, BRY.id_cards)
    ActivReader().reader()

'''
if __name__ == '__main__':
    from browser import document, html, ajax


    class Browser:
        DOC, HTML, AJAX = document, html, ajax
        main_menu = document["nav-menu"]
        id_cards = document["people"]


    main(Browser)
'''
# ACTIV_DATA = "<html><body><p>[<br/>\r\n{ title: 'RELATÓRIO TÉCNICO CIENTÍFICO CONTEÚDO-PROCESSO: A TRANSMOGLIFAÇÃO METAFÓRICA DOS ESQUEMAS COGNITIVOS',<br/>\r\n  author:<br/>\r\n   [ 'Erica de Jesus Soares Scheffel',<br/>\r\n     'Isabel Hortencia Garnica Perez Barros',<br/>\r\n     'Marilia Campos Galvão',<br/>\r\n     'Marina Micas Rivollini',<br/>\r\n     'Victor Antonio Azevedo Costa Santos' ],<br/>\r\n  year: 2018,<br/>\r\n  pages: '1-56',<br/>\r\n  url: 'https://mega.nz/#!TKwnTKpC!l4K-A25zXKYzdakTj2j7357I6anK1vUwXFL8Cdojs1U',<br/>\r\n  key: 'NCE2018_1' },<br/>\r\n{ title: 'RELATÓRIO TÉCNICO CIENTÍFICO UM ESTUDO SOBRE TEMPLATES DE MODELOS NEUROCOMPUTACIONAIS',<br/>\r\n  author:<br/>\r\n   [ 'Bruna Fiuza do Espirito Santo Silva Ferreira',<br/>\r\n     'Cibele Ribeiro da Cunha Oliveira',<br/>\r\n     'Daniel Santos Chaves',<br/>\r\n     'Erica de Jesus Soares Scheffel',<br/>\r\n     'Isabel Hortência Garnica Perez Barros',<br/>\r\n     'Victor Antonio Azevedo Costa Santos' ],<br/>\r\n  year: 2017,<br/>\r\n  pages: '1-31',<br/>\r\n  url: 'https://mega.nz/#!zOwBFA7I!mYC4XYNNzaMHrAktgrJJWbbqBa9yoT4o-TNnuNvPPHo',<br/>\r\n  key: 'NCE2018_1' },<br/>\r\n{ title: 'RELATÓRIO TÉCNICO CIENTÍFICO A MÁQUINA DA METACOGNIÇÃO',<br/>\r\n  author:<br/>\r\n   [ 'Christiano Britto Monteiro' ],<br/>\r\n  year: 2010,<br/>\r\n  pages: '1-18',<br/>\r\n  url: 'https://mega.nz/#!TTwllaiI!NZNjyrG7_lIpt6nqLhjZKwLMOf9vkkueWLL1MwHUO2s',<br/>\r\n  key: 'NCE2018_1' },<br/>\r\n{ title: 'RELATÓRIO TÉCNICO CIENTÍFICO A REVOLUÇÃO COGNITIVA: Um estudo sobre a teoria de Franco Lo Presti Seminério',<br/>\r\n  author:<br/>\r\n   [ 'Carla Verônica Marques',<br/>\r\n     'Carlo Emanoel Tolla de Oliveira',<br/>\r\n     'Claudia Lage Rebello da Motta'],<br/>\r\n  year: 2009,<br/>\r\n  pages: '1-82',<br/>\r\n  url: 'https://mega.nz/#!zD4Fwagb!CYdBL26xnAdezNqBPsnXY2ui27zmjthuUtsYWDeorko',<br/>\r\n  key: 'NCE2018_1' },<br/>\r\n]</p></body></html>"
#ACTIV_DATA = "<html><body><p>[<br/>\r\n{ title: 'RELATÓRIO TÉCNICO CIENTÍFICO CONTEÚDO-PROCESSO: A TRANSMOGLIFAÇÃO METAFÓRICA DOS ESQUEMAS COGNITIVOS',<br/>\r\n  author:<br/>\r\n   [ 'Erica de Jesus Soares Scheffel',<br/>\r\n     'Isabel Hortencia Garnica Perez Barros',<br/>\r\n     'Marilia Campos Galvão',<br/>\r\n     'Marina Micas Rivollini',<br/>\r\n     'Victor Antonio Azevedo Costa Santos' ],<br/>\r\n  year: 2018,<br/>\r\n  pages: '1-56',<br/>\r\n  url: 'https://mega.nz/#!TKwnTKpC!l4K-A25zXKYzdakTj2j7357I6anK1vUwXFL8Cdojs1U',<br/>\r\n  key: 'NCE2018_1' },<br/>\r\n{ title: 'RELATÓRIO TÉCNICO CIENTÍFICO UM ESTUDO SOBRE TEMPLATES DE MODELOS NEUROCOMPUTACIONAIS',<br/>\r\n  author:<br/>\r\n   [ 'Bruna Fiuza do Espirito Santo Silva Ferreira',<br/>\r\n     'Cibele Ribeiro da Cunha Oliveira',<br/>\r\n     'Daniel Santos Chaves',<br/>\r\n     'Erica de Jesus Soares Scheffel',<br/>\r\n     'Isabel Hortência Garnica Perez Barros',<br/>\r\n     'Victor Antonio Azevedo Costa Santos' ],<br/>\r\n  year: 2017,<br/>\r\n  pages: '1-31',<br/>\r\n  url: 'https://mega.nz/#!zOwBFA7I!mYC4XYNNzaMHrAktgrJJWbbqBa9yoT4o-TNnuNvPPHo',<br/>\r\n  key: 'NCE2018_1' },<br/>\r\n{ title: 'RELATÓRIO TÉCNICO CIENTÍFICO A MÁQUINA DA METACOGNIÇÃO',<br/>\r\n  author:<br/>\r\n   [ 'Christiano Britto Monteiro' ],<br/>\r\n  year: 2010,<br/>\r\n  pages: '1-18',<br/>\r\n  url: 'https://mega.nz/#!TTwllaiI!NZNjyrG7_lIpt6nqLhjZKwLMOf9vkkueWLL1MwHUO2s',<br/>\r\n  key: 'NCE2018_1' },<br/>\r\n{ title: 'RELATÓRIO TÉCNICO CIENTÍFICO A REVOLUÇÃO COGNITIVA: Um estudo sobre a teoria de Franco Lo Presti Seminério',<br/>\r\n  author:<br/>\r\n   [ 'Carla Verônica Marques',<br/>\r\n     'Carlo Emanoel Tolla de Oliveira',<br/>\r\n     'Claudia Lage Rebello da Motta'],<br/>\r\n  year: 2009,<br/>\r\n  pages: '1-82',<br/>\r\n  url: 'https://mega.nz/#!zD4Fwagb!CYdBL26xnAdezNqBPsnXY2ui27zmjthuUtsYWDeorko',<br/>\r\n  key: 'NCE2018_1' },<br/>\r\n]</p></body></html>"

#ACTIV_DATA = """{"status": 0, "result": {"wikidata": {"data_cri": "2018-07-08 19:53:52.887444", "comentar": true, "tags": [], "data_alt": "2018-07-10 12:38:26.370731", "ler": true, "alterar": true, "apagar": true, "nomepag_id": "_relatorios_", "is_template": "N", "alterado_por": "carlo", "pag": "BibliotecaIES/_relatorios_", "owner": "carlo", "conteudo": "<html><body><p>[<br/>\r\n{ title: 'RELATÓRIO TÉCNICO CIENTÍFICO CONTEÚDO-PROCESSO: A TRANSMOGLIFAÇÃO METAFÓRICA DOS ESQUEMAS COGNITIVOS',<br/>\r\n  author:<br/>\r\n   [ 'Erica de Jesus Soares Scheffel',<br/>\r\n     'Isabel Hortencia Garnica Perez Barros',<br/>\r\n     'Marilia Campos Galvão',<br/>\r\n     'Marina Micas Rivollini',<br/>\r\n     'Victor Antonio Azevedo Costa Santos' ],<br/>\r\n  year: 2018,<br/>\r\n  pages: '1-56',<br/>\r\n  url: 'https://mega.nz/#!TKwnTKpC!l4K-A25zXKYzdakTj2j7357I6anK1vUwXFL8Cdojs1U',<br/>\r\n  key: 'NCE2018_1' }]</p></body></html>", "data_alt_fmt": "10/07/2018 \u00e0s 12:38", "registry_id": "BibliotecaIES", "comentarios": [], "nomepag": "_relatorios_", "data_cri_fmt": "08/07/2018 \u00e0s 19:53"}, "pagina": "_relatorios_", "path": "/<a href='/wiki/BibliotecaIES'>BibliotecaIES<\/a>/<a href='/wiki/BibliotecaIES?folder=51f8eecbdac640f88cb3e10c485ef497'>infra<\/a>/"}}"""
ACTIV_DATA = """{"status": 0, "result": {"wikidata": {"data_cri": "2018-07-08 19:53:52.887444", "comentar": true, "tags": [], "data_alt": "2018-07-10 12:38:26.370731", "ler": true, "alterar": true, "apagar": true, "nomepag_id": "_relatorios_", "is_template": "N", "alterado_por": "carlo", "pag": "BibliotecaIES/_relatorios_", "owner": "carlo", "conteudo": "<html><body><p>[<br/>\r\n{ title: 'RELATÓRIO TÉCNICO CIENTÍFICO UM ESTUDO SOBRE TEMPLATES DE MODELOS NEUROCOMPUTACIONAIS',<br/>\r\n  author:<br/>\r\n   [ 'Bruna Fiuza do Espirito Santo Silva Ferreira',<br/>\r\n     'Cibele Ribeiro da Cunha Oliveira',<br/>\r\n     'Daniel Santos Chaves',<br/>\r\n     'Erica de Jesus Soares Scheffel',<br/>\r\n     'Isabel Hortência Garnica Perez Barros',<br/>\r\n     'Victor Antonio Azevedo Costa Santos' ],<br/>\r\n  year: 2017,<br/>\r\n  pages: '1-31',<br/>\r\n  url: 'https://mega.nz/#!zOwBFA7I!mYC4XYNNzaMHrAktgrJJWbbqBa9yoT4o-TNnuNvPPHo',<br/>\r\n  key: 'NCE2018_1' },{ title: 'RELATÓRIO TÉCNICO CIENTÍFICO CONTEÚDO-PROCESSO: A TRANSMOGLIFAÇÃO METAFÓRICA DOS ESQUEMAS COGNITIVOS',<br/>\r\n  author:<br/>\r\n   [ 'Erica de Jesus Soares Scheffel',<br/>\r\n     'Isabel Hortencia Garnica Perez Barros',<br/>\r\n     'Marilia Campos Galvão',<br/>\r\n     'Marina Micas Rivollini',<br/>\r\n     'Victor Antonio Azevedo Costa Santos' ],<br/>\r\n  year: 2018,<br/>\r\n  pages: '1-56',<br/>\r\n  url: 'https://mega.nz/#!TKwnTKpC!l4K-A25zXKYzdakTj2j7357I6anK1vUwXFL8Cdojs1U',<br/>\r\n  key: 'NCE2018_1' },{ title: 'RELATÓRIO TÉCNICO CIENTÍFICO A MÁQUINA DA METACOGNIÇÃO',<br/>\r\n  author:<br/>\r\n   [ 'Christiano Britto Monteiro' ],<br/>\r\n  year: 2010,<br/>\r\n  pages: '1-18',<br/>\r\n  url: 'https://mega.nz/#!TTwllaiI!NZNjyrG7_lIpt6nqLhjZKwLMOf9vkkueWLL1MwHUO2s',<br/>\r\n  key: 'NCE2018_1' },{ title: 'RELATÓRIO TÉCNICO CIENTÍFICO A REVOLUÇÃO COGNITIVA: Um estudo sobre a teoria de Franco Lo Presti Seminério',<br/>\r\n  author:<br/>\r\n   [ 'Carla Verônica Marques',<br/>\r\n     'Carlo Emanoel Tolla de Oliveira',<br/>\r\n     'Claudia Lage Rebello da Motta'],<br/>\r\n  year: 2009,<br/>\r\n  pages: '1-82',<br/>\r\n  url: 'https://mega.nz/#!zD4Fwagb!CYdBL26xnAdezNqBPsnXY2ui27zmjthuUtsYWDeorko',<br/>\r\n  key: 'NCE2018_1' }]</p></body></html>", "data_alt_fmt": "10/07/2018 \u00e0s 12:38", "registry_id": "BibliotecaIES", "comentarios": [], "nomepag": "_relatorios_", "data_cri_fmt": "08/07/2018 \u00e0s 19:53"}, "pagina": "_relatorios_", "path": "/<a href='/wiki/BibliotecaIES'>BibliotecaIES<\/a>/<a href='/wiki/BibliotecaIES?folder=51f8eecbdac640f88cb3e10c485ef497'>infra<\/a>/"}}"""

if __name__ == '__main__':
    ActivReader().parse(ACTIV_DATA)

CARD = """
                <div class="column is-one-third">
                    <div class="card large">
                        <div class="card-image">
                            <figure class="image">
                                <img src="{back}" alt="{name}">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-left">
                                    <figure class="image is-96x96">
                                        <img src="{photo}" alt="Image">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <p class="title is-4 no-padding">{name}</p>
                                    <p><span class="title is-6"><a href="{home}">@homepage</a></span></p>
                                    <p class="subtitle is-6">{role}</p>
                                </div>
                            </div>
                            <div class="content">
                                {bio}
                            </div>
                            <div class="content">
                                {icons}
                            </div>
                        </div>
                    </div>
                </div>

            """
DIVISORIA = """
    <section class="hero is-info is-medium is-bold">
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">{title}</h1>
                <a id="{section}" name="{section}">
            </div>
        </div>
    </section>
"""
ARTICLE = """
                        <div class="tile is-parent">
                            <a href="#{title}">
                                <article class="tile is-child">
                                    <p class="title">
                                        <span class="icon is-size-1">
                                                <i class="fa fa-{icon}"></i>
                                        </span>
                                    </p>
                                    <p class="subtitle">{title}</p>
                                </article>
                            </a>
                        </div>

"""

