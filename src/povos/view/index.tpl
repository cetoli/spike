<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Povos Indígenas</title>
    <link rel="shortcut icon" href="../images/fav_icon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <!-- Bulma Version 0.7.1-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css" />
    <link rel="stylesheet" type="text/css" href="../css/landing.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

<script>
    function close(){
        var NAME = document.getElementById("the_modal");
        NAME.classList.toggle('is-active');}
</script>
    <script  type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {

            // Get all "navbar-burger" elements
            var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

            // Check if there are any navbar burgers
            if ($navbarBurgers.length > 0) {

            // Add a click event on each of them
            $navbarBurgers.forEach(function ($el) {
              $el.addEventListener('click', function () {

                // Get the target from the "data-target" attribute
                var target = $el.dataset.target;
                var $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

              });
            });
            }

        });
    </script>    <!-- fonts -->
</head>

<body>
    <div id="the_modal" class="modal is-active" onclick="close()">
        <div class="modal-background">
        </div>
            <div class="modal-content">
                <img src="/_static/indio01.jpg" onclick="close()"/>
            </div>
            <button class="modal-close is-large" aria-label="close" onclick='document.getElementById("the_modal").classList.toggle("is-active");'></button>

    </div>
    <section class="hero is-info is-fullheight">
        <div class="hero-head">
            <nav class="navbar">
                <div class="container">
                    <div class="navbar-brand">
                        <a class="navbar-item" href="../">
                            <img src="/_static/povoslogo.png" alt="Logo">
                        </a>
                        <span class="navbar-burger burger" data-target="navbarMenu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </div>
                    <div id="navbarMenu" class="navbar-menu">
                        <div class="navbar-end">
                            <span class="navbar-item">
                                <a class="button is-white is-outlined" href="#aldeias">
                                    <span class="icon">
                                        <i class="fas fa-sliders-h"></i>
                                    </span>
                                    <span>Povos</span>
                                </a>
                            </span>
                            <span class="navbar-item">
                                <a class="button is-white is-outlined" href="#noticias">
                                    <span class="icon">
                                        <i class="far fa-edit"></i>
                                    </span>
                                    <span>Notícias</span>
                                </a>
                            </span>
                            <span class="navbar-item">
                                <a class="button is-white is-outlined" href="#noticias">
                                    <span class="icon">
                                        <i class="far fa-eye"></i>
                                    </span>
                                    <span>Cultura</span>
                                </a>
                            </span>
                            <span class="navbar-item">
                                <a class="button is-white is-outlined" href="#aldeias">
                                    <span class="icon">
                                        <i class="fa fa-book"></i>
                                    </span>
                                    <span>Aldeias</span>
                                </a>
                            </span>
                            <span class="navbar-item">
                                <a class="button is-white is-outlined" href="#aldeias">
                                    <span class="icon">
                                        <i class="fas fa-flag-checkered"></i>
                                    </span>
                                    <span>Nós</span>
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </nav>
            </div>

            <div class="hero-body">
                <div class="container has-text-centered">
                    <div class="column is-6 is-offset-3">
                        <h1 class="title">
                            <br/>
                            Povos Indígenas
                        </h1>
                        <h2 class="subtitle">
                            Resistênca das Nações Indígenas na preservação de sua cultura, identidade e existência.
                        </h2>

                    </div>
                </div>
            </div>

    </section>

        <div class="section">
            <!-- Developers -->
            <div class="hero-body">
                <div class="container has-text-centered">
                    <h1 class="title">Aldeias</h1>
                    <a id="aldeias" name="aldeias"></a>
                </div>
            </div>
            <div class="row columns">
                <div class="column is-one-third">
                    <div class="card large">
                        <div class="card-image">
                            <figure class="image">
                                <img src="https://i.pinimg.com/originals/0a/0e/ce/0a0ece783a781bd00b1eda27aef9aaae.jpg" alt="Aldeia Kalapalo">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-left">
                                    <figure class="image is-96x96" style="width: 96px; height: 96px; overflow: hidden">
                                        <img src="https://i.imgur.com/s8Ewtts.jpg" width="96px" alt="Image">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <p class="title is-4 no-padding">Aldeia Kalapalo</p>
                                    <p><span class="title is-6"><a href="https://www.facebook.com/samanthakalapalo/">@samanthakalapalo</a></span></p>
                                    <p class="subtitle is-6">Samantha Kalapalo</p>
                                </div>
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum consequatur numquam aliquam tenetur ad amet inventore hic beatae, quas accusantium perferendis sapiente explicabo, corporis totam! Labore reprehenderit beatae magnam animi!
                                <div class="background-icon"><span class="icon-twitter"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-one-third">
                    <div class="card large">
                        <div class="card-image">
                            <figure class="image">
                                <img src="http://gerais.info/wp-content/uploads/2017/03/Xakriab%C3%A1-300x200.jpg" alt="Aldeia Xacriabá">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-left">
                                    <figure class="image is-96x96" style="width: 96px; height: 96px; overflow: hidden">
                                        <img src="https://i.imgur.com/Cx6V3e8.jpg" alt="Celia Xakriabá">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <p class="title is-4 no-padding">Aldeia Xakriabá</p>
                                    <p><span class="title is-6"><a href="https://www.facebook.com/C%C3%A9lia-Xakriab%C3%A1-1199748993470103/">@celiaxakriaba</a></span></p>
                                    <p class="subtitle is-6">Lider Indígena Célia Xakriabá</p>
                                </div>
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum consequatur numquam aliquam tenetur ad amet inventore hic beatae, quas accusantium perferendis sapiente explicabo, corporis totam! Labore reprehenderit beatae magnam animi!
                                <div class="background-icon"><span class="icon-facebook"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-one-third">
                    <div class="card large">
                        <div class="card-image">
                            <figure class="image">
                                <img src="https://i.imgur.com/sSuxIBB.jpg" alt="Aldeia Xavante">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-left">
                                    <figure class="image is-96x96" style="width: 96px; height: 96px; overflow: hidden">
                                        <img src="https://i.imgur.com/ZIt9SAi.jpg" height="96p" alt="Lúcio Xavante">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <p class="title is-4 no-padding">Aldeia Xavante</p>
                                    <p><span class="title is-6"><a href="https://www.facebook.com/Luciowaaneterowaa/">@Luciowaaneterowaa</a></span></p>
                                    <p class="subtitle is-6">Lúcio Xavante</p>
                                </div>
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum consequatur numquam aliquam tenetur ad amet inventore hic beatae, quas accusantium perferendis sapiente explicabo, corporis totam! Labore reprehenderit beatae magnam animi!
                                <div class="background-icon"><span class="icon-barcode"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Developers -->
            <div class="hero-body">
                <div class="container has-text-centered">
                    <h1 class="title">Notícias</h1>
                    <a id="noticias" name="noticias"></a>
                </div>
            </div>

            <!-- Staff -->
            <div id="people" class="row columns is-multiline"></div>
            <!-- End Staff -->
        </div>
            <section class="hero is-info is-medium is-bold">
    </section>

    <script async type="text/javascript" src="../js/bulma.js"></script>
</body>

</html>