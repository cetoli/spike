def equity():
    b = [b[4] for b in zip(m[i:] for i in range(9)) if ''.join(b[j] for j in
    [0,4,8]).islower() and (b[1:4]+b[5:-1]).isupper()] 
def channel():
    from zipfile import ZipFile
    wd = "/home/carlo/Documentos/dev/spike/src/challenge/channel.zip"
    archive = ZipFile(wd, 'r')
    imgdata = archive.read('readme.txt').split(b"\n")
    [print(l) for l in imgdata]
    print(len(archive.namelist()))
    nlist= set(archive.namelist())
    nothing = "90052"
    done = ["90052.txt"]
    bann = ""
    for i in range(910):
        nnothing = f'{nothing}.txt'
        done.append(nnothing)
        nofile = archive.read(nnothing)
        nothing = nofile.decode().split()[-1]
        bann += archive.getinfo(nnothing).comment.decode()
        print(i, nothing, archive.getinfo(nnothing).comment)
        if not nothing.isdigit():
            #bb = archive.getinfo(nofile).comment
            break
    sdone = set(done)
    print(nlist-sdone)
    #[print(l) for l in imgdata]
    print(archive.comment)
    banner = bann.split("\n")
    [print(bl) for bl in banner]
    
def oxygen():
    from PIL import Image
    from collections import Counter
    wd = "/home/carlo/Documentos/dev/spike/src/challenge/oxygen.png"
    im = Image.open(wd, "r")
    pix_val = list(im.getdata())
    print(im.text)
    pvf = pix_val_flat = [r for r, g, b, a in pix_val if r==g==b]
    #pix_val_flat = pix_val
    his = Counter(pix_val_flat)
    print([(chr(px), ct) for px, ct in his.items() if ct >10])
    ms = (''.join([chr(pvf[l]) for l in range(2, len(pvf), 6)]))
    msa = [l.split(']')[0] for l in ms.split("[")]
    [print(l) for l in msa]
    msf = [105,  110, 116, 101, 103, 114, 105,  116, 121]
    print(''.join([chr(l) for l in msf]))

def integrity():
    un= b'BZh91AY&SYA\xaf\x82\r\x00\x00\x01\x01\x80\x02\xc0\x02\x00 \x00!\x9ah3M\x07<]\xc9\x14\xe1BA\x06\xbe\x084'
    pw= b'BZh91AY&SY\x94$|\x0e\x00\x00\x00\x81\x00\x03$ \x00!\x9ah3M\x13<]\xc9\x14\xe1BBP\x91\xf08'
    from bz2 import decompress as bzd
    from bz2 import compress as bzc
    print (bzc(b'abcd'))
    print (bzd(un))
    print (bzd(pw))

def good():
    first =[
    146,399,163,403,170,393,169,391,166,386,170,381,170,371,170,355,169,346,167,335,170,329,170,320,170,
    310,171,301,173,290,178,289,182,287,188,286,190,286,192,291,194,296,195,305,194,307,191,312,190,316,
    190,321,192,331,193,338,196,341,197,346,199,352,198,360,197,366,197,373,196,380,197,383,196,387,192,
    389,191,392,190,396,189,400,194,401,201,402,208,403,213,402,216,401,219,397,219,393,216,390,215,385,
    215,379,213,373,213,365,212,360,210,353,210,347,212,338,213,329,214,319,215,311,215,306,216,296,218,
    290,221,283,225,282,233,284,238,287,243,290,250,291,255,294,261,293,265,291,271,291,273,289,278,287,
    279,285,281,280,284,278,284,276,287,277,289,283,291,286,294,291,296,295,299,300,301,304,304,320,305,
    327,306,332,307,341,306,349,303,354,301,364,301,371,297,375,292,384,291,386,302,393,324,391,333,387,
    328,375,329,367,329,353,330,341,331,328,336,319,338,310,341,304,341,285,341,278,343,269,344,262,346,
    259,346,251,349,259,349,264,349,273,349,280,349,288,349,295,349,298,354,293,356,286,354,279,352,268,
    352,257,351,249,350,234,351,211,352,197,354,185,353,171,351,154,348,147,342,137,339,132,330,122,327,
    120,314,116,304,117,293,118,284,118,281,122,275,128,265,129,257,131,244,133,239,134,228,136,221,137,
    214,138,209,135,201,132,192,130,184,131,175,129,170,131,159,134,157,134,160,130,170,125,176,114,176,
    102,173,103,172,108,171,111,163,115,156,116,149,117,142,116,136,115,129,115,124,115,120,115,115,117,
    113,120,109,122,102,122,100,121,95,121,89,115,87,110,82,109,84,118,89,123,93,129,100,130,108,132,110,
    133,110,136,107,138,105,140,95,138,86,141,79,149,77,155,81,162,90,165,97,167,99,171,109,171,107,161,
    111,156,113,170,115,185,118,208,117,223,121,239,128,251,133,259,136,266,139,276,143,290,148,310,151,
    332,155,348,156,353,153,366,149,379,147,394,146,399]

    second = [
    156,141,165,135,169,131,176,130,187,134,191,140,191,146,186,150,179,155,175,157,168,157,163,157,159,
    157,158,164,159,175,159,181,157,191,154,197,153,205,153,210,152,212,147,215,146,218,143,220,132,220,
    125,217,119,209,116,196,115,185,114,172,114,167,112,161,109,165,107,170,99,171,97,167,89,164,81,162,
    77,155,81,148,87,140,96,138,105,141,110,136,111,126,113,129,118,117,128,114,137,115,146,114,155,115,
    158,121,157,128,156,134,157,136,156,136] 
    #print(''.join(chr(l) for l in second))   
    from turtle import goto as gt
    sec = [second[n:n+2] for n in range(0, len(second), 2)]
    fir = [first[n:n+2] for n in range(0, len(first), 2)]
    [gt(x, y) for x, y in fir]
    [gt(x, y) for x, y in sec]
    
def sequence():
    seqs = ['1', '11']
    seq = list('11')
    for i in range(30):
        nseq = []
        simbol = letter = seq.pop()
        while seq:
            count = 1
            while seq:
                old, letter = letter, seq.pop()
                if old == letter:
                    count += 1
                    #nseq = [str(count)] + [letter] + nseq
                else:
                    nseq = [str(count)] + [old]  + nseq
                    if not seq:
                        #nseq = [str(count)] + [letter] + nseq
                        nseq = [str(1)] + [letter] + nseq
                    simbol = old
                    break
            else:
                nseq = [str(count)] + [letter] + nseq
                
            #nseq = [str(count)] + [simbol] + nseq
        seq = nseq
        seqs.append(''.join(seq))
    print(seqs[:10])
    print(len(seqs[30]))
    import re
    
    curr = '1'
    for each in range(10):
        print(curr)
        match = re.findall("(\d)(\\1*)", curr)
        curr = ''.join([str(len(k+y))+k for k,y in match])
    
    print( len(curr))

    
def _sequence():
    seqs = ['1', '11']
    seq = list('11')
    for i in range(6):
        nseq = ''
        skip = False
        for  a, b in zip(seq, seq[1:]):
            if skip:
                skip = False
                continue
            nnseq = f"1{a}1{b}" if a!=b  else f"2{a}"
            nseq += nnseq
            skip = a!=b
        print(list(zip(seq, seq[1:])), nseq)
            
        seq = ''.join(nseq)
        seqs.append(seq)
    #print(seqs)
    
def oddeven():
    from PIL import Image
    from collections import Counter
    wd = "/home/carlo/Documentos/dev/spike/src/challenge/cave.jpg"
    im = Image.open(wd, "r")
    print(im.size)
    w, h = im.size
    im2 = Image.new(im.mode, (w//2, h))
    #im2.putdata(list_of_pixels)

    pix_val = list(im.getdata())
    print(pix_val[:30])
    all_pix = len(pix_val)
    pix_odd = [pix_val[(i//2)*2] for i in range(all_pix)]
    pix_eve = [pix_val[(i//2)*2+1] for i in range(all_pix)]
    # pix_eve = [pix_val[i] for i in range(0,all_pix,2)]
    print(pix_eve[:30])
    print(pix_odd[:300])
    im2 = Image.new(im.mode, im.size)
    im2.putdata(pix_eve)
    #im2.show()
    im3 = Image.new(im.mode, im.size)
    im3.putdata(pix_odd)
    im3.show()
    im2.save('eve.jpg')
    im3.save('odd.bmp')

    return
    print(im.text)
    pvf = pix_val_flat = [r for r, g, b, a in pix_val if r==g==b]
    #pix_val_flat = pix_val
    his = Counter(pix_val_flat)
    print([(chr(px), ct) for px, ct in his.items() if ct >10])
    ms = (''.join([chr(pvf[l]) for l in range(2, len(pvf), 6)]))
    msa = [l.split(']')[0] for l in ms.split("[")]
    [print(l) for l in msa]
    msf = [105,  110, 116, 101, 103, 114, 105,  116, 121]
    print(''.join([chr(l) for l in msf]))


if __name__ == '__main__':
    oddeven()